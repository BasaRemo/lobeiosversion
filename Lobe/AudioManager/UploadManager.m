//
//  UploadManager.m
//  Lobe
//
//  Created by Professional on 2016-02-17.
//  Copyright Ã‚Â© 2016 Ntambwa. All rights reserved.
//

#import "UploadManager.h"
#import "Lobe-Swift.h"
#include <pthread.h>
#import "SDAVAssetExportSession.h"
#include <sys/time.h>
//#import "lame.h"
#define TICK   NSDate *startTime = [NSDate date]
#define TOCK   NSLog(@"LAME TIME: %f", -[startTime timeIntervalSinceNow])

#pragma mark static object shared between C and Objective-C
static OSStatus result;

//Arraylist  containing the chunks info
static NSMutableArray *chunksInfoList;

//Arraylist each chunk byte data
static NSMutableArray *chunksDataList;

static AVAssetReader *assetReader;
static AVAssetReaderTrackOutput *assetOutput;
static AVAssetExportSession *exporter;
static NSString *songName;
static NSString *exportM4aFilePath;
static NSNumber *songDuration;
NSString *uploadedFilesDirectory;
NSString *currentChunkPath;

pthread_mutex_t avasset_export_mutex;
dispatch_semaphore_t avasset_semaphore;
//To know is a song is currently uploading
static bool isUploading;

@interface UploadManager ()
@end

@implementation UploadManager
// Private instance variables
{
    
}

#pragma mark Initalisation
//returns the Singleton object
+ (UploadManager *)sharedInstance {
    static UploadManager *_sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[self alloc] init];
    });
    
    return _sharedInstance;
}

-(id)init
{
    if (self = [super init]) {
    }
    return self;
}

/*
 The initialize method is the class-level equivalent of init. It gives you a chance to set up the class before anyone uses it
 */
+ (void)initialize {
    if (self == [UploadManager class]) {
        // Makes sure this isn't executed more than once
        chunksInfoList = [[NSMutableArray alloc] init];
        chunksDataList = [[NSMutableArray alloc] init];
        avasset_semaphore = dispatch_semaphore_create(0);
        @synchronized (self) {
            isUploading = false;
        }
        
    }
}

+(void) resetArrayList {
    if (self == [UploadManager class]) {
        
        [chunksInfoList removeAllObjects];
        [chunksDataList removeAllObjects];
        
    }
}
//Check if upload is ongoing
-(BOOL) isUploading{
    return isUploading;
}
#pragma mark Chunks creation & CallBacks
-(int)createAllLocalFileChunks:(NSString*)persistentID nbSecondsPerChunk:(UInt32)nbSecondsPerChunk {
    
    @autoreleasepool {
        
        [FileManagerHelper resetUploadDirectory];
        [UploadManager resetArrayList];
        
        MPMediaItem *song;
        MPMediaPropertyPredicate *predicate = [MPMediaPropertyPredicate predicateWithValue: persistentID forProperty:MPMediaItemPropertyPersistentID];
        MPMediaQuery *songQuery = [[MPMediaQuery alloc] init];
        [songQuery addFilterPredicate: predicate];
        
        if (songQuery.items.count > 0)
        {
            //song exists
            //        NSLog(@"Found Song");
            song = [songQuery.items objectAtIndex:0];
            NSString * name = [song valueForKey:MPMediaItemPropertyTitle];
            songName = [name stringByReplacingOccurrencesOfString:@" " withString:@""];
            songDuration = [song valueForProperty:MPMediaItemPropertyPlaybackDuration];
            NSURL *url = [song valueForProperty: MPMediaItemPropertyAssetURL];
            
            [self dataFromMediaItem:song mNbSecondsPerChunk:nbSecondsPerChunk];
            //                [self testCall:song mNbSecondsPerChunk:nbSecondsPerChunk];
            
        }else{
            [self invokeChunkCreationCallbackFailed:0];
            //        NSLog(@"Error Can't Find Song");
        }
        return 0;
    }
}

-(NSData*) chunkDataAtIndex:(int) chunkIndex {
    //TODO: - App crashes here when it can't find the index
    //    return [NSData dataWithData:[chunksDataList objectAtIndex:chunkIndex]];
    return [chunksDataList objectAtIndex:chunkIndex];
}

-(void) getFileType:(MPMediaItem *)mediaItem{
    
    @autoreleasepool {
        
        NSString * name = [mediaItem valueForKey:MPMediaItemPropertyTitle];
        songName = [name stringByReplacingOccurrencesOfString:@" " withString:@""];
        songDuration = [mediaItem valueForProperty:MPMediaItemPropertyPlaybackDuration];
        
        NSURL *url = [mediaItem valueForProperty: MPMediaItemPropertyAssetURL];
        AVURLAsset *songAsset = [[AVURLAsset alloc] initWithURL:url options:nil];
        NSString *fileType = [[[[songAsset.URL absoluteString] componentsSeparatedByString:@"?"] objectAtIndex:0] pathExtension];
    }
    
}
-(void) testCall:(MPMediaItem *)mediaItem  mNbSecondsPerChunk:(int)nbSecondsPerChunk{
    
    //    TICK;
//    NSLog(@"Media Item To Data");
    NSString * name = [mediaItem valueForKey:MPMediaItemPropertyTitle];
    songName = [name stringByReplacingOccurrencesOfString:@" " withString:@""];
    songDuration = [mediaItem valueForProperty:MPMediaItemPropertyPlaybackDuration];
    
    NSURL *url = [mediaItem valueForProperty: MPMediaItemPropertyAssetURL];
    NSError *assetError;
    AVURLAsset *songAsset = [[AVURLAsset alloc] initWithURL:url options:nil];
    //    AVAssetTrack*   srcTrack = [[songAsset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0];
    
    //    NSURL *dstURL = [NSURL fileURLWithPath: [url absoluteString]];
    //    AVMutableComposition*   newAudioAsset = [AVMutableComposition composition];
    //    AVMutableCompositionTrack*  dstCompositionTrack;
    //    dstCompositionTrack = [newAudioAsset addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
    //    AVAsset*    srcAsset = [AVURLAsset URLAssetWithURL:dstURL options:nil];
    //    AVAssetTrack*   srcTrack = [[srcAsset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0];
    
    
    //        AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset: songAsset presetName:AVAssetExportPresetPassthrough];
    SDAVAssetExportSession *exporter = [SDAVAssetExportSession.alloc initWithAsset:songAsset];
    //    NSString *fileType = [[[[songAsset.URL absoluteString] componentsSeparatedByString:@"?"] objectAtIndex:0] pathExtension];
    exporter.outputFileType = AVFileTypeCoreAudioFormat;
    exporter.audioSettings = @
    {
    AVFormatIDKey: @(kAudioFileMP3Type),
    AVNumberOfChannelsKey: @2,
    AVSampleRateKey: @22050,
    AVEncoderBitRateKey: @16
    };
    
    //=====================================================================================================
    //MARK: - Get the estimated size of the track & Fill the chunkInfo Array
    //=====================================================================================================
    //        CMTime half = CMTimeMultiplyByFloat64(exporter.asset.duration, 1);
    //        exporter.timeRange = CMTimeRangeMake(kCMTimeZero, half);
    //        long long trackEstimatedSize = exporter.estimatedOutputFileLength;
    //        [self fillChunkInfo:12345676 mNbSecondsPerChunk:nbSecondsPerChunk];
    
    long long songLenght = songAsset.duration.value/songAsset.duration.timescale;
    long long trackEstimatedSize = 5547968;
    [self fillChunkInfo:trackEstimatedSize mNbSecondsPerChunk:nbSecondsPerChunk];
    
    exporter.shouldOptimizeForNetworkUse = YES;
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    uploadedFilesDirectory = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    
    NSString * fileName = [NSString stringWithFormat:@"%@.mp3",songName];
    CMTime startTime = CMTimeMake(30,1);
    CMTime stopTime = CMTimeMake(60,1);
    CMTimeRange exportTimeRange = CMTimeRangeFromTimeToTime(startTime, stopTime);
    
    NSString *exportFile = [uploadedFilesDirectory stringByAppendingPathComponent:fileName];
    NSURL *exportURL = [NSURL fileURLWithPath:exportFile];
    exporter.outputURL = exportURL;
    exporter.timeRange = exportTimeRange;
    
    
    //=====================================================================================================
    //MARK: - Start exporting
    //=====================================================================================================
    [exporter exportAsynchronouslyWithCompletionHandler:
     ^{
         NSError *error = nil;
         int exportStatus = exporter.status;
         if (exportStatus == AVAssetExportSessionStatusCompleted) {
//             NSLog (@"AVAssetExportSessionStatusCompleted");
             
             NSString * mp3DataPath = [[exportFile stringByDeletingLastPathComponent] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.mp3",songName]];
             
             NSError *renameError = nil;
             [[NSFileManager defaultManager] moveItemAtPath:exportFile toPath:mp3DataPath error:&renameError];
             
             if (renameError) {
                 NSLog (@"renameError=%@",renameError.localizedDescription);
             }else {
//                 NSLog (@" Success renaming file to .mp3 newPath=%@",mp3DataPath);
                 
             }
             
         }
         else if (exportStatus == AVAssetExportSessionStatusCancelled)
         {
//             NSLog(@"Video export cancelled");
         }
         else
         {
//             NSLog(@"AVAssetExportSessionStatusNotCompleted with error: %@ (%ld)", error.localizedDescription, (long)error.code);
         }
     }];
    
    
}

-(void)exportChunkIntoMp3WithTimeRange :(CMTimeRange) exportTimeRange chunkNumber:(UInt64) chunkNumber{
    
    
}

-(void) fillChunkInfo:(UInt64) trackEstimatedSize mNbSecondsPerChunk:(int)nbSecondsPerChunk{
    
    @autoreleasepool {
        
        //=====================================================================================================
        //MARK: - Fill the chunkInfo array & send it to Javascript
        //=====================================================================================================
        UInt64 numberOfChunks = songDuration.longLongValue/30;
        UInt64 lastChunkLenght = songDuration.longLongValue%30;
        UInt64 fileByteSize = trackEstimatedSize;
        UInt64 numByteIn30Secs = (fileByteSize/songDuration.longLongValue)*nbSecondsPerChunk;
        //    NSLog(@"song byte size: %llu",fileByteSize);
        //    NSLog(@"numByteIn30Secs: %llu",numByteIn30Secs);
        //    NSLog(@"song duration in se: %lld",songDuration.longLongValue);
        //    NSLog(@"numberOfChunks: %u",(unsigned int)numberOfChunks);
        //    NSLog(@"lastChunkLenght in sec: %u",(unsigned int)lastChunkLenght);
        //
        UInt64 mBufferCurrentPosition = 0;
        int chunkNumber = 0;
        [chunksInfoList removeAllObjects];
        while (1) {
            chunkNumber++;
            UInt64 numRemainingBytes = fileByteSize - mBufferCurrentPosition;
            if (numRemainingBytes >= 2*numByteIn30Secs) {
                
                //Write the chunk data into a file in the app cache directory
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *documentsDirectory = [paths objectAtIndex:0];
                uploadedFilesDirectory = [NSString stringWithFormat:@"%@/%@", documentsDirectory, @"uploadFolder"];
                //            NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@%i.mp3",songName,chunkNumber]];
                NSString *dataPath = [uploadedFilesDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.mp3",[self cleanString:songName]]];
                
                //Create the chunkInfo for the current chunk and add it to chunksInfoList & add Chunk data to chunksDataList
                ChunkInfo *mChunkInfo = [[ChunkInfo alloc] init];
                mChunkInfo.mNbOfBytes = numByteIn30Secs;
                mChunkInfo.mEstimatedLengthInSeconds = 30;
                mChunkInfo.mFirstFrameTimePositionMillis = mBufferCurrentPosition*(songDuration.longLongValue*1000)/fileByteSize;
                mChunkInfo.mTempStoragePath = dataPath;
                mBufferCurrentPosition += numByteIn30Secs;
                [chunksInfoList addObject:mChunkInfo];
                
                continue;
            }else{
                
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *documentsDirectory = [paths objectAtIndex:0];
                NSString *dataPath = [uploadedFilesDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.mp3",[self cleanString:songName]]];
                
                //Create the chunkInfo for the current chunk and add it to chunksInfoList & add Chunk data to chunksDataList
                ChunkInfo *mChunkInfo = [[ChunkInfo alloc] init];
                mChunkInfo.mNbOfBytes = numRemainingBytes;
                mChunkInfo.mEstimatedLengthInSeconds = numRemainingBytes*songDuration.longLongValue/fileByteSize;
                mChunkInfo.mFirstFrameTimePositionMillis = mBufferCurrentPosition*(songDuration.longLongValue*1000)/fileByteSize;
                mChunkInfo.mTempStoragePath = dataPath;
                
                [chunksInfoList addObject:mChunkInfo];
                
                break;
            }
        }
        
        //Invoke Javascript with the chunk info
        [self invokeChunkCreationCallbackSucceded:chunksInfoList];
    }
}

//- (void)cafToMp3:(NSString*)cafFileName chunkNumber:(int) chunkNumber
//{
//    TICK;
//    NSArray *dirPaths;
//    NSString *docsDir;
//    
//    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    docsDir = [dirPaths objectAtIndex:0];
//    NSString *_mp3FilePath = [docsDir stringByAppendingPathComponent:[NSString stringWithFormat:@"decoded%@%d.mp3",[self cleanString:songName],chunkNumber]];
//    
//    @try {
//        int read, write;
//        FILE *pcm = fopen([cafFileName cStringUsingEncoding:1], "rb");
//        FILE *mp3 = fopen([_mp3FilePath cStringUsingEncoding:1], "wb");
//        const int PCM_SIZE = 8192;
//        const int MP3_SIZE = 8192;
//        short int pcm_buffer[PCM_SIZE*2];
//        unsigned char mp3_buffer[MP3_SIZE];
//        
//        lame_t lame = lame_init();
//        lame_set_in_samplerate(lame, 44100);
//        lame_set_VBR(lame, vbr_default);
//        lame_init_params(lame);
//        
//        do {
//            read = fread(pcm_buffer, 2*sizeof(short int), PCM_SIZE, pcm);
//            if (read == 0)
//                write = lame_encode_flush(lame, mp3_buffer, MP3_SIZE);
//            else
//                write = lame_encode_buffer_interleaved(lame, pcm_buffer, read, mp3_buffer, MP3_SIZE);
//            
//            fwrite(mp3_buffer, write, 1, mp3);
//            
//        } while (read != 0);
//        
//        lame_close(lame);
//        fclose(mp3);
//        fclose(pcm);
//    }
//    @catch (NSException *exception) {
////        NSLog(@"%@",[exception description]);
//    }
//    @finally {
//        //Detrming the size of mp3 file
//        NSFileManager *fileManger = [NSFileManager defaultManager];
//        NSData *data = [fileManger contentsAtPath:_mp3FilePath];
//        NSString* str = [NSString stringWithFormat:@"%lu K",[data length]/1024];
////        NSLog(@"size of mp3=%@",str);
//        TOCK;
//        
//    }
//}


/*
 Get data from mpMediaItem and give it to the asset reader to get byte data
 */
-(void)dataFromMediaItem :(MPMediaItem *)curItem  mNbSecondsPerChunk:(int)nbSecondsPerChunk
{
    @autoreleasepool {
        
        //    TICK;
        //    NSLog(@"Media Item To Data");
        NSNumber *songDuration= [curItem valueForProperty:MPMediaItemPropertyPlaybackDuration];
        NSURL *url = [curItem valueForProperty: MPMediaItemPropertyAssetURL];
        NSError *assetError;
        AVURLAsset *avasset = [[AVURLAsset alloc] initWithURL:url options:nil];
        
        //=====================================================================================================
        //MARK: - This part gets you the description of the track, with its format
        //=====================================================================================================
        AVAssetTrack*track = [avasset.tracks objectAtIndex:0];
        CMFormatDescriptionRef formDesc = (__bridge CMFormatDescriptionRef)[[track formatDescriptions] objectAtIndex:0];
        const AudioStreamBasicDescription* asbdPointer = CMAudioFormatDescriptionGetStreamBasicDescription(formDesc);
        //because this is a pointer and not a struct we need to move the data into a struct so we can use it
        AudioStreamBasicDescription asbd = {0};
        //asbd now contains a basic description for the track
        memcpy(&asbd, asbdPointer, sizeof(asbd));
        
        
        //=================================================================================================
        //MARK: - These settings convert the data into PCM bytes
        //        44.1 KHz, stereo, 16-bit, non-interleaved, little-endian integer PCM
        //=================================================================================================
        const uint32_t sampleRate = 44100.0; // 16k sample/sec
        const uint16_t bitDepth = 16; // 16 bit/sample/channel
        const uint16_t channels = 2; // 2 channel/sample (stereo)
        NSDictionary *settings = [NSDictionary dictionaryWithObjectsAndKeys:
                                  [NSNumber numberWithInt:kAudioFormatLinearPCM], AVFormatIDKey,
                                  [NSNumber numberWithFloat:(float)sampleRate], AVSampleRateKey,
                                  [NSNumber numberWithInt:bitDepth], AVLinearPCMBitDepthKey,
                                  [NSNumber numberWithBool:NO], AVLinearPCMIsNonInterleaved,
                                  [NSNumber numberWithBool:NO], AVLinearPCMIsFloatKey,
                                  [NSNumber numberWithBool:NO], AVLinearPCMIsBigEndianKey, nil];
        
        //=======================================================================================================
        //MARK: - Here we setup the asset reader with the asset and the desired settings
        //        Note : Having the  outputSettings at nil won't change the  input type
        //=======================================================================================================
        assetReader = [AVAssetReader assetReaderWithAsset:avasset error:&assetError];
        assetOutput = [AVAssetReaderTrackOutput assetReaderTrackOutputWithTrack:avasset.tracks[0] outputSettings:nil];
        
        if ([assetReader canAddOutput:assetOutput]) {
            //        NSLog(@"Start Reading assetReader");
            [assetReader addOutput:assetOutput];
            [assetReader startReading];
            [self SaveDataToUrlLocalPath:songDuration.doubleValue mNbSecondsPerChunk:nbSecondsPerChunk];
        }else{
            [self invokeChunkCreationCallbackFailed:0];
        }
    }
}

//=========================================================================================================
//MARK:- Here we read the data from assetReader and save it to local cache and also into chunksDataList
//    :- Right now chunksDataList is used to send data to the server. We need to make sure it mp3 data
//=========================================================================================================
-(void) SaveDataToUrlLocalPath :(double)songDuration mNbSecondsPerChunk:(int)nbSecondsPerChunk
{
    
    @autoreleasepool {
        
        //    TICK;
        //    NSLog(@"DEBUT DECODING into mp3 chunks");
        // Write the entire mediaItem to mp3
        BOOL firstTime = YES;
        NSMutableData *dataBuffer;
        CMSampleBufferRef sample;
        CMBlockBufferRef blockBufferRef = NULL;
        
        while(assetReader.status == AVAssetReaderStatusReading) { //while (1){
            sample = [assetOutput copyNextSampleBuffer];
            //        CMTime duration = CMSampleBufferGetDuration(sample);
            //        NSLog(@"Duration: %lld seconds",duration.value/duration.timescale);
            
            blockBufferRef = CMSampleBufferGetDataBuffer(sample);
            size_t length = CMBlockBufferGetDataLength(blockBufferRef);
            UInt8 buffer[length];
            CMBlockBufferCopyDataBytes(blockBufferRef, 0, length, buffer);
            //            NSLog(@"buffer size: %zu",length);
            if (firstTime) {
                firstTime = NO;
                dataBuffer = [[NSMutableData alloc] initWithBytes:buffer length:length];
            }else{
                [dataBuffer appendBytes:buffer length:length];
            }
        }
        
        UInt64 fileByteSize = dataBuffer.length;
        
        if (fileByteSize == 0 || songDuration < nbSecondsPerChunk) {
            [self invokeChunkCreationCallbackFailed:0];
        }else{
            
            UInt64 numByteIn30Secs = (fileByteSize/songDuration)*nbSecondsPerChunk;
            //        NSLog(@"song byte size: %llu",fileByteSize);
            //        NSLog(@"song duration: %f",songDuration);
            //        NSLog(@"numByteIn30Secs: %llu",numByteIn30Secs);
            
            UInt64 mBufferCurrentPosition = 0;
            NSData *chunkFileData;
            
            int chunkNumber = 0;
            //        [chunksInfoList removeAllObjects];
            
            while (1) {
                chunkNumber++;
                UInt64 numRemainingBytes = fileByteSize - mBufferCurrentPosition;
                if (numRemainingBytes >= 2*numByteIn30Secs) {
                    chunkFileData = [dataBuffer subdataWithRange:NSMakeRange(mBufferCurrentPosition, numByteIn30Secs)];
                    
                    //Write the chunk data into a file in the app cache directory
                    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                    NSString *documentsDirectory = [paths objectAtIndex:0];
                    uploadedFilesDirectory = [NSString stringWithFormat:@"%@/%@", documentsDirectory, @"uploadFolder"];
                    NSString *dataPath = [uploadedFilesDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"decoded%@%d.mp3",[self cleanString:songName],chunkNumber]];
                    
                    [chunkFileData writeToFile:dataPath atomically:YES];
                    //                [self cafToMp3:dataPath chunkNumber:chunkNumber];
                    
                    //Create the chunkInfo for the current chunk and add it to chunksInfoList & add Chunk data to chunksDataList
                    ChunkInfo *mChunkInfo = [[ChunkInfo alloc] init];
                    mChunkInfo.mNbOfBytes = numByteIn30Secs;
                    mChunkInfo.mEstimatedLengthInSeconds = 30;
                    mChunkInfo.mFirstFrameTimePositionMillis = mBufferCurrentPosition*(songDuration*1000)/fileByteSize;
                    mChunkInfo.mTempStoragePath = dataPath;
                    mBufferCurrentPosition += numByteIn30Secs;
                    [chunksInfoList addObject:mChunkInfo];
                    //                [chunksDataList addObject:chunkFileData];
                    
                    continue;
                }else{
                    chunkFileData = [dataBuffer subdataWithRange:NSMakeRange(mBufferCurrentPosition, numRemainingBytes)];
                    
                    //Write the chunk data into a file in the app cache directory
                    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                    NSString *documentsDirectory = [paths objectAtIndex:0];
                    NSString *dataPath = [uploadedFilesDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"decoded%@%d.mp3",[self cleanString:songName],chunkNumber]];
                    [chunkFileData writeToFile:dataPath atomically:YES];
                    //                [self cafToMp3:dataPath chunkNumber:chunkNumber];
                    
                    //Create the chunkInfo for the current chunk and add it to chunksInfoList & add Chunk data to chunksDataList
                    ChunkInfo *mChunkInfo = [[ChunkInfo alloc] init];
                    mChunkInfo.mNbOfBytes = numRemainingBytes;
                    mChunkInfo.mEstimatedLengthInSeconds = numRemainingBytes*songDuration/fileByteSize;
                    mChunkInfo.mFirstFrameTimePositionMillis = mBufferCurrentPosition*(songDuration*1000)/fileByteSize;
                    mChunkInfo.mTempStoragePath = dataPath;
                    
                    [chunksInfoList addObject:mChunkInfo];
                    //                [chunksDataList addObject:chunkFileData];
                    
                    break;
                }
            }
            
            //        NSLog(@" Number of CHUNKS: %lu ",(unsigned long)chunksInfoList.count);
            
            [self invokeChunkCreationCallbackSucceded:chunksInfoList];
        }
    }
}

/*
 Failure callback that notify Javascript that chunk creation failed
 */
-(void) invokeChunkCreationCallbackFailed:(int) status{
    
    NativeToWebHelper *nativeToWeb = [NativeToWebHelper sharedInstance];
    [nativeToWeb chunksLocalCreationCallback:NULL status:status];
//    NSLog(@"Chunk Creation Failed");
}

/*
 Success callback that notify Javascript that chunk creation Succeeded
 */
-(void) invokeChunkCreationCallbackSucceded:(NSMutableArray*) chunkInfoList{
    
    NativeToWebHelper *nativeToWeb = [NativeToWebHelper sharedInstance];
    [nativeToWeb chunksLocalCreationCallback:chunksInfoList status:1];
}

#pragma mark Song upload
/*
 Helper function called by webToNativeHelper
 */
-(void) startUploadingWithURL:(UInt32)chunckIndex url:(NSString*)urlName correlationID:(NSString*)correlationID chunkStartTimeServer:(int64_t) chunkStartTimeServer{
    NativeToWebHelper *nativeToWeb = [NativeToWebHelper sharedInstance];
    [self uploadAudioFile:chunckIndex fileName:urlName correlationID:correlationID chunkStartServerTime:chunkStartTimeServer];
}

//Upload mp3 chunks data from chunksDataList to the server
-(NSString*)uploadAudioFile:(UInt32)chunkIndex fileName:(NSString*) mFileName correlationID:(NSString*) mCorrelationID chunkStartServerTime:(int64_t) mChunkStartServerTime {
    
    @autoreleasepool {
        
        NSString *urlString = @"http://appymood-lobe.rhcloud.com/upload";
        NSString* theFileName = [[mFileName lastPathComponent] stringByDeletingPathExtension];
        //    NSLog(@"FILE NAME: %@",theFileName);
        
        NSString *fileNameWithExtension = [NSString stringWithFormat:@"%@.mp3",theFileName];
        //    NSLog(@"fileNameWithExtension : %@",fileNameWithExtension);
        
        NSMutableURLRequest* request= [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:urlString]];
        [request setHTTPMethod:@"POST"];
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        NSMutableData *postbody = [NSMutableData data];
        [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [postbody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%@\"\r\n", fileNameWithExtension] dataUsingEncoding:NSUTF8StringEncoding]];
        [postbody appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSData *data = [NSData dataWithContentsOfFile: [uploadedFilesDirectory stringByAppendingPathComponent:fileNameWithExtension]];
        //    NSLog(@"song data length:%lu ",(unsigned long)data.length);
        [postbody appendData:[NSData dataWithData:data]];
        
        [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [request setHTTPBody:postbody];
        
        
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        
        NSURLSessionUploadTask *uploadTask;
        uploadTask = [manager
                      uploadTaskWithStreamedRequest:request
                      progress:^(NSProgress * _Nonnull uploadProgress) {
                          @synchronized (self) {
                              isUploading = false;
                          }
                          // This is not called back on the main queue.
                          // NSLog(@"Progress: %f",uploadProgress.fractionCompleted);
                      }
                      completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                          if (error) {
                              //                          NSLog(@"Error: %@", error);
                          } else {
                              //NSLog(@"%@ %@", response, responseObject);
                              @synchronized (self) {
                                  isUploading = false;
                              }
                              NSDictionary *JSONDic=[[NSDictionary alloc] init];
                              NSError *error;
                              NSData *jsonData = [NSJSONSerialization dataWithJSONObject:responseObject
                                                                                 options:NSJSONWritingPrettyPrinted
                                                                                   error:&error];
                              NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                              //                          NSLog(@"upload jsonString response %@", jsonString);
                              
                              jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                              jsonString = [NSString stringWithFormat:@"%s",[jsonString UTF8String]];
                              
                              NativeToWebHelper *nativeToWeb = [NativeToWebHelper sharedInstance];
                              [nativeToWeb fileUploadResponseFromServer:200 resultStr:jsonString correlationID:mCorrelationID chunkStartServerTime:mChunkStartServerTime];
                          }
                      }];
        
        [uploadTask resume];
        return @"";
    }
}

-(NSString*)cleanString:(NSString *)unfilteredString{
    
    @autoreleasepool {
        
        NSCharacterSet *charactersToRemove =
        [[ NSCharacterSet alphanumericCharacterSet ] invertedSet ];
        
        NSString *trimmedReplacement =
        [ unfilteredString stringByTrimmingCharactersInSet:charactersToRemove ];
        
        NSString *condensedPhoneno = [[trimmedReplacement componentsSeparatedByCharactersInSet:
                                       [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"]
                                        invertedSet]]
                                      componentsJoinedByString:@""];
        
        condensedPhoneno = [condensedPhoneno stringByReplacingOccurrencesOfString:@"[\\!\\.:\\/\\-\\:\\(\\)]" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, [condensedPhoneno length])];
        
        //    NSCharacterSet *notAllowedChars = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"] invertedSet];
        //    NSString *resultString = [[unfilteredString componentsSeparatedByCharactersInSet:notAllowedChars] componentsJoinedByString:@""];
        //    NSString* noSpaces =
        //    [[resultString componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]
        //     componentsJoinedByString:@""];
        
        return condensedPhoneno;
    }
    
}

//Make a cleanup and remove all files which content the extension in parameter
-(void) cleanUpAudioFileWithExtension:(NSString*) fileExtension{
    
    @autoreleasepool {
        
        NSFileManager  *manager = [NSFileManager defaultManager];
        
        // the preferred way to get the apps documents directory
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory;
        documentsDirectory = [paths objectAtIndex:0];
        
        // grab all the files in the documents dir
        NSArray *allFiles = [manager contentsOfDirectoryAtPath:documentsDirectory error:nil];
        
        // filter the array for only sqlite files
        NSString* fExtension = [NSString stringWithFormat:@"self ENDSWITH '.%@'", fileExtension];
        //    NSPredicate *fltr = [NSPredicate predicateWithFormat:@"self ENDSWITH '.m4a'"];
        NSPredicate *fltr = [NSPredicate predicateWithFormat:fExtension];
        NSArray *audioFiles = [allFiles filteredArrayUsingPredicate:fltr];
        
        // use fast enumeration to iterate the array and delete the files
        for (NSString *audioFile in audioFiles)
        {
            NSError *error = nil;
            
            [manager removeItemAtPath:[documentsDirectory stringByAppendingPathComponent:audioFile] error:&error];
            NSAssert(!error, @"Assertion: Audio file deletion shall never throw an error.");
        }
    }
}

@end