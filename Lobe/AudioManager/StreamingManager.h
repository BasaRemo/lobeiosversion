//
//  PcmAudioManager.h
//  Lobe
//
//  Created by Professional on 2016-02-17.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#include <stdlib.h>
#include <math.h>

#include <AudioToolbox/AudioQueue.h>
#include <CoreAudio/CoreAudioTypes.h>
#include <CoreFoundation/CFRunLoop.h>

#import <AudioToolbox/AudioToolbox.h>

#import <MediaPlayer/MediaPlayer.h>
#import "UploadManager.h"

@interface StreamingManager : NSObject <NSStreamDelegate>
// Protected instance variables (not recommended)
{
    
}

#pragma mark Class Methods
+ (StreamingManager *)sharedInstance;
+ (void)initialize;
//Stream
-(void)downloadChuncksFromUrl:(NSString*)url localPath:(NSString*)localPath correlationID:(NSString*)correlationID musicStartTimeInUnixTime:(int64_t)musicStartTimeInUnixTime isLocalSong:(BOOL)isLocalSong mNbOfBytesPassedBeforeChunk:(int64_t)mNbOfBytesPassedBeforeChunk;

-(void)playLocalSong:(NSData*)songData correlationID:(NSString*)correlationID musicStartTimeInUnixTime:(int64_t)musicStartTimeInUnixTime;
//Sync
+(void)syncTimerTicked;
-(int64_t) getCurrentUnixTimeInMs;

//audioqueue playback
-(void) startAudioQueue;
-(void) pauseAudioQueue;
-(void) resumeAudioQueue;
//-(void) resetAudioQueue;
-(void) resetAudioQueueSetup;
-(void) stopAudioQueue:(bool) immediately;
-(void) stopSyncTimer;

@end
