//
//  UploadManager.h
//  Lobe
//
//  Created by Professional on 2016-02-17.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import "AFNetworking.h"
#import <AVFoundation/AVFoundation.h>

#define TICK   NSDate *startTime = [NSDate date]
#define TOCK   NSLog(@"Decode Duration: %f \n \n", -[startTime timeIntervalSinceNow])

@interface UploadManager : NSObject{
}
//@property (strong, nonatomic) AVAssetReader *assetReader;
//@property (strong, nonatomic) AVAssetReaderTrackOutput *assetOutput;

// Class Methods
+ (UploadManager *)sharedInstance;
// Instance Methods
-(void) startUploadingWithURL:(UInt32)chunckIndex url:(NSString*)urlName correlationID:(NSString*)correlationID chunkStartTimeServer:(int64_t) chunkStartTimeServer;
-(int)createAllLocalFileChunks:(NSString*)persistentID nbSecondsPerChunk:(UInt32)nbSecondsPerChunk;
-(void) divideDataIntoChunks:(NSData*) data songDuration:(double)songDuration mNbSecondsPerChunk:(int)nbSecondsPerChunk;
-(NSData*) chunkDataAtIndex:(int) chunkIndex;

+(void) resetArrayList;
+ (void)initialize;
//Check if upload is ongoing
-(BOOL) isUploading;
@end
