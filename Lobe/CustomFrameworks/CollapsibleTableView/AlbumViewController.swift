//
//  ArtistViewController.swift
//  Example-Swift
//
//  Created by Robert Nash on 22/09/2015.
//  Copyright © 2015 Robert Nash. All rights reserved.
//

import UIKit

class AlbumViewController: CollapsableTableViewController {

    @IBOutlet var tableView: UITableView!
    
    let menu = ModelBuilder.buildMenu()
    
    override func model() -> [CollapsableTableViewSectionModelProtocol]? {
        return menu
    }

    override func sectionHeaderNibName() -> String? {
        return "MenuSectionHeaderView"
    }
    
    override func singleOpenSelectionOnly() -> Bool {
        return true
    }
    
    override func shouldCollapse(tableSection: Int) -> Bool {
        let menuSection = self.model()?[tableSection]
        return (menuSection?.items ?? []).count < 1000
    }
    
    override func collapsableTableView() -> UITableView? {
        return tableView
    }
}

extension AlbumViewController {
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 44.0
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 90
    }
    override func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 8
    }
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = UIColor.blackColor()
        return view
    }
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("MusicCell", forIndexPath: indexPath) as! MusicCell
         cell.cellImgLeadingConstraint.constant = 50              
        return cell
    }
}
