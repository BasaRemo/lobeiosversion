//
//  FakeModelBuilder.swift
//  Example-Swift
//
//  Created by Robert Nash on 22/09/2015.
//  Copyright © 2015 Robert Nash. All rights reserved.
//

import Foundation
//import CollapsableTable

class MenuSection: CollapsableTableViewSectionModelProtocol  {
    
    var title: String
    var subtitle: String
    var isVisible: Bool
    var items: [AnyObject]
    
    init() {
        title = ""
        subtitle = ""
        isVisible = false
        items = []
    }
}

class ModelBuilder {
    
    class func buildMenu() -> [CollapsableTableViewSectionModelProtocol] {
        
        var collector = [CollapsableTableViewSectionModelProtocol]()
        
        for var i = 0; i < 15; i++ {
            
            let section = MenuSection()
            
            switch i {
            case 0:
                section.title = "Option 1"
                section.subtitle = "Till I collapse"
                section.isVisible = true
                section.items = [NSNull(), NSNull(), NSNull(),NSNull(), NSNull(), NSNull(), NSNull(), NSNull(), NSNull(),NSNull(), NSNull(), NSNull(), NSNull(), NSNull(), NSNull(),NSNull(), NSNull(), NSNull(),NSNull(), NSNull(), NSNull(), NSNull(), NSNull(), NSNull(),NSNull(), NSNull(), NSNull(), NSNull(), NSNull(), NSNull(),NSNull(), NSNull(), NSNull(),NSNull(), NSNull(), NSNull(), NSNull(), NSNull(), NSNull(),NSNull(), NSNull(), NSNull(), NSNull(), NSNull(), NSNull(),NSNull(), NSNull(), NSNull(),NSNull(), NSNull(), NSNull(), NSNull(), NSNull(), NSNull(),NSNull(), NSNull(), NSNull(), NSNull(), NSNull(), NSNull()]
            case 1:
                section.title = "Option 2"
                section.subtitle = "Till I collapse"
                section.items = [NSNull(), NSNull(), NSNull(),NSNull(), NSNull(), NSNull(), NSNull(), NSNull(), NSNull(),NSNull(), NSNull(), NSNull(), NSNull(), NSNull(), NSNull(),NSNull(), NSNull(), NSNull(),NSNull(), NSNull(), NSNull(), NSNull(), NSNull(), NSNull(),NSNull(), NSNull(), NSNull(), NSNull(), NSNull(), NSNull(),NSNull(), NSNull(), NSNull(),NSNull(), NSNull(), NSNull(), NSNull(), NSNull(), NSNull(),NSNull(), NSNull(), NSNull(), NSNull(), NSNull(), NSNull(),NSNull(), NSNull(), NSNull(),NSNull(), NSNull(), NSNull(), NSNull(), NSNull(), NSNull(),NSNull(), NSNull(), NSNull(), NSNull(), NSNull(), NSNull()]
            case 2:
                section.title = "Option 3"
                section.subtitle = "Till I collapse"
                section.items = [NSNull(), NSNull(), NSNull(),NSNull(), NSNull(), NSNull(), NSNull(), NSNull(), NSNull(),NSNull(), NSNull(), NSNull(), NSNull(), NSNull(), NSNull(),NSNull(), NSNull(), NSNull(),NSNull(), NSNull(), NSNull(), NSNull(), NSNull(), NSNull(),NSNull(), NSNull(), NSNull(), NSNull(), NSNull(), NSNull(),NSNull(), NSNull(), NSNull(),NSNull(), NSNull(), NSNull(), NSNull(), NSNull(), NSNull(),NSNull(), NSNull(), NSNull(), NSNull(), NSNull(), NSNull(),NSNull(), NSNull(), NSNull(),NSNull(), NSNull(), NSNull(), NSNull(), NSNull(), NSNull(),NSNull(), NSNull(), NSNull(), NSNull(), NSNull(), NSNull()]
            case 3:
                section.title = "Option 4"
                section.subtitle = "Till I collapse"
                section.items = [NSNull(), NSNull(), NSNull(),NSNull(), NSNull(), NSNull(), NSNull(), NSNull(), NSNull(),NSNull(), NSNull(), NSNull(), NSNull(), NSNull(), NSNull(),NSNull(), NSNull(), NSNull(),NSNull(), NSNull(), NSNull(), NSNull(), NSNull(), NSNull(),NSNull(), NSNull(), NSNull(), NSNull(), NSNull(), NSNull(),NSNull(), NSNull(), NSNull(),NSNull(), NSNull(), NSNull(), NSNull(), NSNull(), NSNull(),NSNull(), NSNull(), NSNull(), NSNull(), NSNull(), NSNull(),NSNull(), NSNull(), NSNull(),NSNull(), NSNull(), NSNull(), NSNull(), NSNull(), NSNull(),NSNull(), NSNull(), NSNull(), NSNull(), NSNull(), NSNull()]
            case 4:
                section.title = "Option 5"
                section.subtitle = "Till I collapse"
                section.items = [NSNull(), NSNull(), NSNull(), NSNull()]
            case 5:
                section.title = "Option 6"
                section.isVisible = true
                section.items = [NSNull(), NSNull(), NSNull()]
            case 6:
                section.title = "Option 7"
                section.items = [NSNull(), NSNull(), NSNull(), NSNull(), NSNull(), NSNull()]
            case 7:
                section.title = "Option 8"
                section.items = [NSNull(), NSNull(), NSNull()]
            case 8:
                section.title = "Option 9"
                section.items = [NSNull(), NSNull(), NSNull(),NSNull(), NSNull(), NSNull(), NSNull(), NSNull(), NSNull(),NSNull(), NSNull(), NSNull(), NSNull(), NSNull(), NSNull(),NSNull(), NSNull(), NSNull(),NSNull(), NSNull(), NSNull(), NSNull(), NSNull(), NSNull(),NSNull(), NSNull(), NSNull(), NSNull(), NSNull(), NSNull()]
            case 9:
                section.title = "Option 10"
                section.items = [NSNull(), NSNull(), NSNull(), NSNull()]
            case 10:
                section.title = "Option 11"
                section.isVisible = true
                section.items = [NSNull(), NSNull(), NSNull()]
            case 11:
                section.title = "Option 12"
                section.items = [NSNull(), NSNull(), NSNull(), NSNull(), NSNull(), NSNull()]
            case 12:
                section.title = "Option 13"
                section.items = [NSNull(), NSNull(), NSNull()]
            case 13:
                section.title = "Option 14"
                section.items = [NSNull(), NSNull()]
            case 14:
                section.title = "Option 15"
                section.items = [NSNull(), NSNull(), NSNull(), NSNull()]
            default:
                break
                
            }
            
            collector.append(section)
        }
        
        return collector
    }
}