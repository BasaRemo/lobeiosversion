//
//  SongItemListHelper.swift
//  Lobe
//
//  Created by Professional on 2016-03-20.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

import UIKit
import Chronos
import Dollar
import MediaPlayer
import Foundation

class SongItemListHelper: NSObject {

    static var collection: MPMediaItemCollection?
    static var mediaItems = MPMediaQuery.songsQuery().items!
    
    let albumsQuery = MPMediaQuery.albumsQuery()
    let artistQuery = MPMediaQuery.albumsQuery()
    
}

//===================================================================================
//Local List of media items
//====================== f=============================================================

extension SongItemListHelper {
    
    static func localListContains(songItem:SongItem) -> Bool{
        
        guard AUDIO_OUT_MANAGER.localObservableTrackList.count > 0 else {return false }
//        do {
//            let isContained = try AUDIO_OUT_MANAGER.localObservableTrackList.filter({ $0.trackInfo.mID == songItem.trackInfo.mID }).count > 0
//        } catch {
//            print("Something went wrong!")
//        } 
        guard AUDIO_OUT_MANAGER.localObservableTrackList.filter({ $0.trackInfo.mID == songItem.trackInfo.mID }).count > 0 else {return false}
        return true
    }
    
    static func findLocalSong(songItem:SongItem) -> [SongItem]{
        
        guard AUDIO_OUT_MANAGER.localObservableTrackList.count > 0 else {
            return []
        }
        let resultArr = AUDIO_OUT_MANAGER.localObservableTrackList.array.filter({ $0.trackInfo.mID == songItem.trackInfo.mID })
        return resultArr
    }
    
    static func getIndexOfLocalSongItem(songItem:SongItem) -> Int{
        
        guard AUDIO_OUT_MANAGER.localObservableTrackList.count > 0 else {
            return 0
        }
        let result = $.findIndex(AUDIO_OUT_MANAGER.localObservableTrackList.array) { $0.trackInfo.mID == songItem.trackInfo.mID }
        //        print("current Song Index : \(result!)")
        return result!
    }
    
    static func getIndexOfCurrentPlayingLocalTrack() -> Int {
        let currentSong = AUDIO_OUT_MANAGER.currentSong
        return getIndexOfLocalSongItem(currentSong)
    }
    
    static func loadLocalSongs(){
        
        collection = MPMediaItemCollection(items: mediaItems)
        
        print("mediaItems.count: \(self.mediaItems.count)")
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)) {
            
            for item in self.mediaItems {
                
                guard let _ = item.valueForProperty(MPMediaItemPropertyAssetURL) else{
//                    print("Song: \(item.valueForProperty(MPMediaItemPropertyTitle)) is DRM protected")
                    continue
                }
                guard let _ = item.valueForProperty(MPMediaItemPropertyIsCloudItem) else{
//                    print("Song: \(item.valueForProperty(MPMediaItemPropertyTitle)) is Cloud item")
                    continue
                }
                
                let songItem = SongItem(mMediaItem: item)
                
                if !AUDIO_OUT_MANAGER.localListContains(songItem){
                    AUDIO_OUT_MANAGER.localObservableTrackList.append(songItem)
                }
            }
//            print(" AUDIO_OUT_MANAGER.localListSongItem.count: \(AUDIO_OUT_MANAGER.localObservableTrackList.count)")
            
            guard AUDIO_OUT_MANAGER.localObservableTrackList.count > 0 else {
                return
            }
            
//            print("Init the Library")
//            AUDIO_OUT_MANAGER.player.setQueueWithItemCollection(collection!)
//            AUDIO_OUT_MANAGER.currentSong = AUDIO_OUT_MANAGER.localObservableTrackList[0]
//            AUDIO_OUT_MANAGER.player.nowPlayingItem = AUDIO_OUT_MANAGER.currentSong.music!
            AUDIO_OUT_MANAGER.initialLoading = false
        }
    }
}


//===================================================================================
//Lobelist
//===================================================================================

extension SongItemListHelper {
    
    static func lobeListContains(songItem:SongItem) -> Bool{
        
        if AUDIO_OUT_MANAGER.lobeListSongItems.count > 0 {
            
            guard AUDIO_OUT_MANAGER.lobeListSongItems.filter({ $0.trackInfo.mID == songItem.trackInfo.mID }).count > 0 else {return false}
            return true
        }
        return false
    }
    
    static func getIndexOfLobeListSongItem(songItem:SongItem) -> Int{
        
        guard AUDIO_OUT_MANAGER.lobeListSongItems.count > 0 else {
            return 0
        }
        guard let searchArray = AUDIO_OUT_MANAGER.lobeListSongItems as NSArray as? [SongItem] else {return 0}
        let result = $.findIndex(searchArray) { $0.trackInfo.mID == songItem.trackInfo.mID }
        //        print("current Song Index : \(result!)")
        return result!
    }
    
    static func findSongInLobeList(songItem:SongItem) -> [SongItem] {
        
        guard AUDIO_OUT_MANAGER.lobeListSongItems.count > 0 else {
            return []
        }
        let resultArr = AUDIO_OUT_MANAGER.lobeListSongItems.filter({ $0.trackInfo.mID == songItem.trackInfo.mID })
        return resultArr as! [SongItem]
    }
    
    static func getIndexOfCurrentPlayingLobeListTrack() -> Int {
        guard findSongInLobeList(AUDIO_OUT_MANAGER.currentSong).count > 0 else {
//            print("Current song is not selected in Lobelist")
            return 0
        }
        return getIndexOfLobeListSongItem(AUDIO_OUT_MANAGER.currentSong)
    }
    
    static func getTrackAtIndex(index:Int) -> SongItem {
        return AUDIO_OUT_MANAGER.lobeListSongItems[index] as! SongItem
    }
    
    static func isNextLobeListTrackValid() -> Bool {
        
        let currentSongIndex = self.getIndexOfCurrentPlayingLobeListTrack()
        
        guard  currentSongIndex >= 0 else {
//            print("index of current song not valid.. ")
            return false
        }
        
        let nextTrackIndex = currentSongIndex + 1
        guard AUDIO_OUT_MANAGER.lobeListSongItems.count > nextTrackIndex else {
//            print("couldnt find index of next song index .. ")
            return false
        }
        let nextTrack = AUDIO_OUT_MANAGER.lobeListSongItems[nextTrackIndex]
        
        guard !nextTrack.trackInfo.mIsYoutubeVideo else {
//            print("Next track is youtube video")
            return false
        }
        return true
    }
    
    static func removeSongInLobeList(songItemToRemove:SongItem){
        guard AUDIO_OUT_MANAGER.lobeListSongItems.count > 0 else {
            return
        }
        let index = getIndexOfLobeListSongItem(songItemToRemove)
        AUDIO_OUT_MANAGER.lobeListSongItems.removeObjectAtIndex(index)
    }
}

//===================================================================================
// Local Playlists
//===================================================================================

extension SongItemListHelper {
    
}

//===================================================================================
// Local Artists
//===================================================================================

extension SongItemListHelper {
    
    func queryLocalMediaItemByArtist(){
    }
}

//===================================================================================
// Local Albums
//===================================================================================

extension SongItemListHelper {
    
    func queryLocalMediaItemByAlbum(){
    }
}
