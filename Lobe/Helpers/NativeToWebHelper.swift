//
//  NativeToWebHelper.swift
//  Lobe
//
//  Created by Professional on 2016-01-05.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol NativeToWebHelperDelegate {
    func genericFunctionCall(functionName:String, params:String)
    func genericLoginWithFacebook()
    func changeMenuButtonIcon(showBurgerBtn showBurgerBtn:Bool)
}

protocol YoutubeDelegate {
    func javascriptFunctionCall(functionName:String, params:String)
    func nativeCallLoadVideo(trackInfo:TrackInfo)
    func nativeCallSetIsPlayingMediaContentAllowed(isAllowed:Bool)
    func playerCommApiReceivedMessage(strObj:String)
}

class NativeToWebHelper: NSObject {
    
    static let sharedInstance = NativeToWebHelper()
    var delegate:NativeToWebHelperDelegate?
    var youtube_delegate:YoutubeDelegate?
    
    private override init() {
        super.init()
    }
    
    func JavaCallPushNotifActionReceived()
    {
        let pushNotif = LobeStateHelper.sharedInstance.pushNotif!.rawString()
        let functionName:String = "JavaCallPushNotifActionReceived";
        self.delegate?.genericFunctionCall(functionName, params: "'\(replaceBadChar(pushNotif!))'")
    }
    
    func fromJavaDeviceLocationCallback(longitude:Double,latitude:Double)
    {
        let functionName:String = "fromJavaDeviceLocationCallback";
        self.delegate?.genericFunctionCall(functionName, params: "'\(longitude)','\(latitude)'")
    }
    
    // MARK: -  Nabil : Fonction a utiliser
    func fileUploadResponseFromServer(status:Int,resultStr:String, correlationID:String, chunkStartServerTime:UInt64){
        let functionName:String  = "JavaCallFileUploadResponseFromServer"
        self.delegate?.genericFunctionCall(functionName, params: "\(status), '\(resultStr)', '\(correlationID)', \(chunkStartServerTime)")
    }
    
    // MARK: -  Nabil : Fonction a utiliser
    func chunksLocalCreationCallback( chunckInfoList:[ChunkInfo], status:Int){
        
        let functionName:String = "JavaCallChunksLocalCreationCallback"
        
        if chunckInfoList.count != 0 {
            
            let chunckInfoListJSON = JsonHelper.chunkInfoListToJSONArrayData(chunckInfoList)

            for (_,subJson):(String, JSON) in chunckInfoListJSON {
                if let chunkInfoListString = subJson.rawString() {
                    self.delegate?.genericFunctionCall(functionName, params: "'\(replaceBadChar(chunkInfoListString))',\(status)")
                }else{
//                    print("chunksLocalCreationCallback: FAILURE Sending ChunkInfoList TO Social")
                }
            }
            
        }else {
            self.delegate?.genericFunctionCall(functionName, params: "null ,'\(status)'")
        }
        
    }
    
    func pushSingleSuggestionItem(track:TrackInfo){
        
        let letTrackJson = JsonHelper.trackInfoToJSON(track)
        if let string1 = letTrackJson.rawString() {
            
            let functionName:String  = "JavaCallPushSingleSuggestionItem";
            self.delegate?.genericFunctionCall(functionName, params: "'\( replaceBadChar(string1))'")
        }else{
//            print("FAILURE PUSHING SUGGESTION TO WEB")
        }
    }
    
    func removeSingleSuggestionItem(track:TrackInfo)
    {
        let functionName:String  = "JavaCallRemoveSingleSuggestionItem";
        self.delegate?.genericFunctionCall(functionName, params: "\(track)")
    }
    
    func leaveGroupSession(){
        let functionName:String = "JavaCallLeaveGroupSession"
        self.delegate?.genericFunctionCall(functionName, params: "null")
    }
    
    func createGroupSession(name:String){
        let functionName:String  = "JavaCallCreateGroupSession";
        self.delegate?.genericFunctionCall(functionName, params: "\(name)")
    }
    
    func joinGroupSession(name:String){
        let functionName:String  = "JavaCallJoinGroupSession";
        self.delegate?.genericFunctionCall(functionName, params: "\(name)")
    }
    
    func recalculateSyncAlgo(){
        let functionName:String = "JavaCallRecalculateSyncAlgo"
        self.delegate?.genericFunctionCall(functionName, params: "null")
    }
    
    // called on when in broadcasting state, (3)
    func updateLobeLists(var lobeActiveList:[TrackInfo] , lobeSuggestionList:[TrackInfo]  ){
        
        lobeActiveList.removeAll()
        for songItem:SongItem in  (AUDIO_OUT_MANAGER.lobeListSongItems as NSArray as! [SongItem])  {
            lobeActiveList.append(songItem.trackInfo)
        }
        
        let suggestionListJSON = JsonHelper.musicListToJSONObjectData(lobeSuggestionList)
        let lobeListJSON = JsonHelper.musicListToJSONArrayData(lobeActiveList)
        
        if let string1 = lobeListJSON.rawString() {
            //Do something you want
            if let string2 = suggestionListJSON.rawString() {
                //Do something you want
                let functionName:String = "JavaCallUpdateLobeLists"
                self.delegate?.genericFunctionCall(functionName, params: "'\(replaceBadChar(string1))',''")
            }
        }else{
//            print("FAILURE PUSHING LOBE LISTS TO WEB")
        }
    }
    func replaceBadChar(str:String) -> String  {
        return str.replace("\'", withString: "\\\'").replace("\"", withString: "\\\"").replace("\n", withString: "\\n").replace("\r", withString: "")
        
        //        return str.replace(/\n/g, "\\\\n").replace(/\/g, "\\\\r").replace(/ t/g, "\\\\t");
    }
    // (2)
    func stopListeningStream()
    {
        let functionName:String  = "JavaCallStopListeningStream";
        self.delegate?.genericFunctionCall(functionName,params:"null" );
    }
    
    // (2,3)
    func newUploadChunkAvailable(track:TrackInfo, start_time_stamp:UInt64 ,music_delay:Int )
    {
        let functionName:String  = "JavaCallNewUploadChunkAvailable";
        self.delegate?.genericFunctionCall(functionName, params: "\(track)")
    }
    
    //DJ (3)
    func resumeEvent()
    {
        let functionName:String  = "JavaCallResumeEvent";
        self.delegate?.genericFunctionCall(functionName, params:"null");
    }
    
    //DJ (3)
    func pauseEvent()
    {
        let functionName:String  = "JavaCallPauseEvent";
        self.delegate?.genericFunctionCall(functionName, params: "null");
    }
    
    // Debug 
    func debugPing(pingType:String, value:Int )
    {
        let functionName:String  = "JavaCallDebugPing";
        self.delegate?.genericFunctionCall(functionName,  params: "\(pingType),\(value)");
    }
    
    func viewMyProfile()
    {
        let functionName:String  = "JavaCallViewMyProfile";
        self.delegate?.genericFunctionCall(functionName, params: "null")
    }
    func setTrackToUpload(track:TrackInfo)
    {
        let letTrackJson = JsonHelper.trackInfoToJSON(track)
        if let string1 = letTrackJson.rawString() {
            
            let functionName:String  = "JavaCallSetTrackToUpload";
            self.delegate?.genericFunctionCall(functionName, params: "'\( replaceBadChar(string1))'")
        }else{
//            print("FAILURE SEND UPLOAD TO WEB")
        }
    }
    
    func triggerDirectPlayTrack(track:TrackInfo)
    {
        let letTrackJson = JsonHelper.trackInfoToJSON(track)
        if let string1 = letTrackJson.rawString() {
            
            let functionName:String  = "JavaCallTriggerDirectPlayTrack";
            self.delegate?.genericFunctionCall(functionName, params: "'\( replaceBadChar(string1))'")
        }else{
//            print("FAILURE SEND UPLOAD TO WEB")
        }
    }
    
}

//Social song management & Control Calls (for DJ and Streaming mode)
extension NativeToWebHelper {
    
    // MARK: - Social playback management
    // new
    func triggerPreviousTrackEvent()
    {
        let functionName = "JavaCallTriggerPreviousTrackEvent";
        self.delegate?.genericFunctionCall(functionName, params: "null")
    }
    
    // new
    func triggerNextTrackEvent()
    {
        let functionName = "JavaCallTriggerNextTrackEvent";
        self.delegate?.genericFunctionCall(functionName, params: "null")
    }
    
    // new
    func setIsPlayingMediaContentAllowed(isAllowed:Bool)
    {
        let functionName = "JavaCallSetIsPlayingMediaContentAllowed";
        self.delegate?.genericFunctionCall(functionName, params: "\(isAllowed)")
    }
    
    
    //MARK: - Social song voting system
    // new
    func promoteTrackInLobeList(trackPosIndex:Int, trackUid:String)
    {
        let functionName = "JavaCallPromoteTrackInLobeList";
        self.delegate?.genericFunctionCall(functionName, params: "\(trackPosIndex),'\(trackUid)'")
    }
    
    // new
    func dislikeTrackInLobeList(trackPosIndex:Int, trackUid:String)
    {
        let functionName = "JavaCallDislikeTrackInLobeList";
        self.delegate?.genericFunctionCall(functionName, params: "\(trackPosIndex),'\(trackUid)'")
    }
    
    // new
    func voteChangeCurrentTrack()
    {
        let functionName = "JavaCallVoteChangeCurrentTrack";
        self.delegate?.genericFunctionCall(functionName, params: "null")

    }
    
    // MARK: - LobeList song list management
    // new
    func deleteTrackFromLobeList(trackUid:String )
    {
        let functionName = "JavaCallDeleteTrackFromLobeList";
        self.delegate?.genericFunctionCall(functionName, params: "'\(trackUid)'")
    }
    
    func refreshMediaContentPlaybackState(){
        let functionName = "refreshMediaContentPlaybackState";
        self.delegate?.genericFunctionCall(functionName, params: "null")
    }
    
    //Mark:- Facebook Login    
    func loginWithFacebookUsingInfo(mAccessToken:String,mUserID:String,mExpirationDateInSec:Int64){
//        print("Here JavaCallLoginWithFacebookUsingInfo")
        let functionName = "JavaCallLoginWithFacebookUsingInfo";
        HybridBridge.sharedInstance.genericJavascriptCall(functionName, params: "'\(mAccessToken)','\(mUserID)','',\(mExpirationDateInSec)")
    }
    
    func loginWithSpotifyUsingInfo(accessToken accessToken:String,canonicalUsername:String, mExpirationDateInSec:Int64,encryptedRefreshToken:String){
        let functionName = "NativeCallLoginWithSpotifyUsingInfo";
        HybridBridge.sharedInstance.genericJavascriptCall(functionName, params: "'\(accessToken)','\(canonicalUsername)',\(mExpirationDateInSec), '\(encryptedRefreshToken)'")
    }
    
    func showView(viewString:String,backToViewString:String)
    {
//        print("CALLED SHOW VIEW: \(viewString)")
        let functionName:String = "JavaCallShowView";
        self.delegate?.genericFunctionCall(functionName, params: "'\(viewString)','\(backToViewString)'")
        self.manageViewChange(viewString)
    }
    
    //===============================================================
    //TODO:- Move this in showView call in NativeToWebHelper
    //===============================================================
    func manageViewChange(viewString:String){
        
//        NativeToWebHelper.sharedInstance.delegate?.changeMenuButtonIcon(showBurgerBtn: false)
//        dispatch_async(dispatch_get_main_queue()) {
//             AUDIO_OUT_MANAGER.mainVC.togglePlaybackView(isHidden: false)
//        }
        LobeStateHelper.sharedInstance.current_visible_view.value = viewString
        LobeStateHelper.sharedInstance.updateYoutubeViewState()
        switch (viewString)
        {
            case "lobeLiveView":
                LobeStateHelper.sharedInstance.menu_selected_index.value = 2
                break
            case "myProfileView":
                break;
            case "findLobersView":
                break;
            case "groupView":
                //TODO: - Show burger button
//            NativeToWebHelper.sharedInstance.delegate?.changeMenuButtonIcon(showBurgerBtn: true)
                LobeStateHelper.sharedInstance.menu_selected_index.value = 3
                dispatch_async(dispatch_get_main_queue()) {
                    AUDIO_OUT_MANAGER.mainVC.toggleWebViewContainerView(isHidden: false)
//                    AUDIO_OUT_MANAGER.mainVC.toggleLoadingView(isHidden:true)
                }
                break;
            case "chatting":
//                print("Go to chat")
//            NativeToWebHelper.sharedInstance.delegate?.changeMenuButtonIcon(showBurgerBtn: true)
                break;
            case "youtubeSearch":
                break;
            case "welcomeView":
//            NativeToWebHelper.sharedInstance.delegate?.changeMenuButtonIcon(showBurgerBtn: true)
//                print("Showing welcome view")
                dispatch_async(dispatch_get_main_queue()) {
                    AUDIO_OUT_MANAGER.mainVC.toggleWebViewContainerView(isHidden: false)
                    AUDIO_OUT_MANAGER.mainVC.toggleLoadingView(isHidden:true)
                     AUDIO_OUT_MANAGER.mainVC.togglePlaybackView(isHidden: true)
                }
                break;
            case "groupPreviewView":
                break;
            case "homePageView":
                //TODO: - Show burger button
//            NativeToWebHelper.sharedInstance.delegate?.changeMenuButtonIcon(showBurgerBtn: true)
                LobeStateHelper.sharedInstance.menu_selected_index.value = 1
                dispatch_async(dispatch_get_main_queue()) {
                    AUDIO_OUT_MANAGER.mainVC.toggleWebViewContainerView(isHidden: false)
//                    AUDIO_OUT_MANAGER.mainVC.toggleLoadingView(isHidden:true)
                }
                break;
            case "invitePeopleView":
                break;
            case "loadingView":
                dispatch_async(dispatch_get_main_queue()) {
                    AUDIO_OUT_MANAGER.mainVC.toggleWebViewContainerView(isHidden: false)
                }
                break;
            case "localMusicView": //Should never get here because of the guard in the requestNativeToShowView function
                LobeStateHelper.sharedInstance.menu_selected_index.value = 4
                dispatch_async(dispatch_get_main_queue()) {
                    AUDIO_OUT_MANAGER.mainVC.toggleWebViewContainerView(isHidden: true)
//                    AUDIO_OUT_MANAGER.mainVC.toggleLoadingView(isHidden:true)
                }
                break;
            case "songSelectionView":
                break;
            default :
                break;
        }
    }
    
    func backButtonWasPressed(){
        let functionName:String = "JavaCallBackButtonWasPressed";
        self.delegate?.genericFunctionCall(functionName, params: "")
    }
    
    func playerCommApiReceivedMessage(strObj:String){
        let functionName:String = "JavaCallPlayerCommApiReceivedMessage"
//        print("JavaCallPlayerCommApiReceivedMessage with param \(strObj)")
//        dispatch_async(dispatch_get_main_queue()) {
            self.delegate?.genericFunctionCall(functionName, params: "\(strObj)")
//        }
        
    }
    
    func joinGroup(group_id:String){
        let functionName:String = "fetchAndShowGroupPreview"
        self.delegate?.genericFunctionCall(functionName, params: "'\(group_id)'")
    }
    
    func swapLobeListItems(fromIndex fromIndex:Int, toIndex:Int){
        
        let functionName:String = "NativeCallSwapLobeListItems"
        self.delegate?.genericFunctionCall(functionName, params: "\(fromIndex),\(toIndex)")
    }
    
    func fromNativePing(pingType pingType:String, params:JSON){
        
        let functionName:String = "fromNativePing"
        self.delegate?.genericFunctionCall(functionName, params: "'\(pingType)','\(JsonHelper.replaceBadChar(params.rawString()!))'")
    }
}

extension String
{
    func replace(target: String, withString: String) -> String
    {
        return self.stringByReplacingOccurrencesOfString(target, withString: withString, options: NSStringCompareOptions.LiteralSearch, range: nil)
    }
    
    func replaceRegex(targetString: String) ->String {
        if let regex = try? NSRegularExpression(pattern: "[^a-zA-Z0-9_-]", options: .CaseInsensitive) {
            let modString = regex.stringByReplacingMatchesInString(targetString, options: .WithTransparentBounds, range: NSMakeRange(0, targetString.characters.count), withTemplate: "")
            return modString
        }
        return ""
    }
    
}
