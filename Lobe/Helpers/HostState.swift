//
//  HostState.swift
//  Lobe
//
//  Created by Professional on 2016-02-20.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

import UIKit

class HostState: SocialState {

    static let sharedInstance = HostState()
    private override init() {
        super.init()
    }
    static func getSharedInstance() -> HostState{
        return sharedInstance
    }
    
    //TODO: - create a direct play function: directPlayTrack() that is different from pause&resume (play and pause)
    //TODO: - if upload manager is currently uploading : Show toast message (you need to wait before changing music)
    //TODO: -  otherwise call JS function : NativeToWebHelper.sharedInstance.triggerDirectPlayTrack()
    override func directPlayTrack(){
        
        if AUDIO_OUT_MANAGER.musicPlayerState == .LobeList{
            
            guard !uploadManager_sharedInstance.isUploading() else {
                //            print("Ongoing Upload: you need to wait before changing music")
                return
            }
            
            guard AUDIO_OUT_MANAGER.lobeListSongItems.count > 0 else {
                //            print("No song in the playlist")
                return
            }
            
            streamingManager_sharedInstance.pauseAudioQueue()
            NativeToWebHelper.sharedInstance.triggerDirectPlayTrack(AUDIO_OUT_MANAGER.currentSong.trackInfo)
            AUDIO_OUT_MANAGER.isGroupMusicPlaying = true

        }
        
    }
    
//    override func skipToNextMusic(){
//        
//        guard !uploadManager_sharedInstance.isUploading() else {
//            print("Ongoing Upload: you need to wait before changing music")
//            return
//        }
//        
//        guard AUDIO_OUT_MANAGER.lobeListSongItems.count > 0 else {
//            print("No song in the playlist")
//            return
//        }
//        
//        print("Next song triggered")
//        //Group playing state
//        AUDIO_OUT_MANAGER.isGroupMusicPlaying = true
//        
//        //Notify real audio playing state to Social (Javascript)
//        NativeToWebHelper.sharedInstance.setIsPlayingMediaContentAllowed(true)
//        NativeToWebHelper.sharedInstance.youtube_delegate?.nativeCallSetIsPlayingMediaContentAllowed(true)
//        
//        NativeToWebHelper.sharedInstance.triggerNextTrackEvent()
////        dispatch_async(dispatch_get_main_queue()) {
////            SVProgressHUD.show()
////        }
//    }
    
    override func skipToPreviousMusic(){
        
        guard !uploadManager_sharedInstance.isUploading() else {
//            print("Ongoing Upload: you need to wait before changing music")
            return
        }
        
        guard AUDIO_OUT_MANAGER.lobeListSongItems.count > 0 else {
//            print("No song in the playlist")
            return
        }
        
        //Group playing state
//        AUDIO_OUT_MANAGER.isGroupMusicPlaying = true
        //Notify real audio playing state to Social (Javascript)
        NativeToWebHelper.sharedInstance.setIsPlayingMediaContentAllowed(true)
        NativeToWebHelper.sharedInstance.youtube_delegate?.nativeCallSetIsPlayingMediaContentAllowed(true)
        NativeToWebHelper.sharedInstance.triggerPreviousTrackEvent()
        streamingManager_sharedInstance.pauseAudioQueue()
    
    }
    
    override func skipToBeginning(){
        
    }
}
