//
//  SocialState.swift
//  Lobe
//
//  Created by Professional on 2016-02-20.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

import UIKit

class SocialState: LobeAppState {

//    static let sharedInstance = SocialState()
//    static func getSharedInstance() -> SocialState{
//        return sharedInstance
//    }
//    private override init() {
//        super.init()
//    }
    
    override func isTrackPlaying() -> Bool {
        return AUDIO_OUT_MANAGER.isGroupMusicPlaying
    }
    
    override func playTrack(){
        
        guard AUDIO_OUT_MANAGER.lobeListSongItems.count > 0 else {
//            print("No song in the playlist")
            return
        }
        
        guard !AUDIO_OUT_MANAGER.isGroupMusicPlaying else {
//            print("A song is already playing in social mode")
            return
        }
        
        startPlayAction()
    }
    
    override func resumeTrack(){
        guard AUDIO_OUT_MANAGER.lobeListSongItems.count > 0 else {
//            print("No song in the playlist")
            return
        }
        
        guard !AUDIO_OUT_MANAGER.isGroupMusicPlaying else {
//            print("A song is already playing in social mode")
            return
        }
        startPlayAction()
    }
    
    func startPlayAction(){
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)) {
            
            switch (AUDIO_OUT_MANAGER.currentSong.trackInfo.track_type) {
                
            case "youtube":
                AUDIO_OUT_MANAGER.shouldPlayYoutubeVideo.value = true
            case "spotify":
                AUDIO_OUT_MANAGER.stopStreamingAudio()
                LobeStateHelper.sharedInstance.updateYoutubeViewState()
            case "local":
                streamingManager_sharedInstance.resumeAudioQueue()
                
            default:break
                
            }
            
            AUDIO_OUT_MANAGER.isGroupMusicPlaying = true
            NativeToWebHelper.sharedInstance.setIsPlayingMediaContentAllowed(true)
            NativeToWebHelper.sharedInstance.youtube_delegate?.nativeCallSetIsPlayingMediaContentAllowed(true)
            SpotifyHelper.sharedInstance.playPause(isPlaying: true)
            
        }
    }
    
    override func skipToNextMusic(){
        
        guard !uploadManager_sharedInstance.isUploading() else {
//            print("Ongoing Upload: you need to wait before changing music")
            return
        }
        
//        guard AUDIO_OUT_MANAGER.lobeListSongItems.count > 1 else {
////            print("No song in the playlist")
//            return
//        }
        
//        print("Next song triggered")
        //Group playing state
        AUDIO_OUT_MANAGER.isGroupMusicPlaying = true
        
        //Notify real audio playing state to Social (Javascript)
        NativeToWebHelper.sharedInstance.setIsPlayingMediaContentAllowed(true)
        NativeToWebHelper.sharedInstance.youtube_delegate?.nativeCallSetIsPlayingMediaContentAllowed(true)
        NativeToWebHelper.sharedInstance.triggerNextTrackEvent()
        //        dispatch_async(dispatch_get_main_queue()) {
        //            SVProgressHUD.show()
        //        }
    }
    
    override func directPlayTrack(){
        preconditionFailure("SocialState directPlayTrack() : This method must be overridden")
    }
    override func skipToPreviousMusic(){
        preconditionFailure("SocialState directPlayTrack() : This method must be overridden")
    }
    override func skipToBeginning(){
        preconditionFailure("SocialState directPlayTrack() : This method must be overridden")
    }
    

}
