//
//  LocalState.swift
//  Lobe
//
//  Created by Professional on 2016-02-20.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

import UIKit
//import Dollar

class LocalState: LobeAppState {

    static let sharedInstance = LocalState()

    private override init() {
        super.init()
    }
    static func getSharedInstance() -> LocalState{
        return sharedInstance
    }
    
    override func isTrackPlaying() -> Bool {
        return AUDIO_OUT_MANAGER.isPlaying
    }
    
    override func playTrack(){
        guard let currentTrack = AUDIO_OUT_MANAGER.currentSong.music else {
            return
        }
        AUDIO_OUT_MANAGER.player.nowPlayingItem = currentTrack
        AUDIO_OUT_MANAGER.player.play()
        AUDIO_OUT_MANAGER.isPlaying = true
    }
    override func resumeTrack(){
        playTrack()
    }
    
    override func directPlayTrack(){
        if AUDIO_OUT_MANAGER.musicPlayerState == .Library {
//            AUDIO_OUT_MANAGER.currentLocalSongIndex = AUDIO_OUT_MANAGER.getIndexOfCurrentPlayingLocalTrack()
        }else {
            AUDIO_OUT_MANAGER.currentLobeListSongIndex = AUDIO_OUT_MANAGER.getIndexOfCurrentPlayingLobeListTrack()
        }
        
        playTrack()
    }
    override func skipToNextMusic(){
        
        guard AUDIO_OUT_MANAGER.localObservableTrackList.count > 1 else {
            print("cant't skip next because localObservableTrackList < 0 ..")
            return
        }
        
        if AUDIO_OUT_MANAGER.musicPlayerState == .Library{
            
//            if AUDIO_OUT_MANAGER.currentLocalSongIndex == AUDIO_OUT_MANAGER.localObservableTrackList.count - 1 {
//                AUDIO_OUT_MANAGER.currentLocalSongIndex = 0
//            }else{
//                AUDIO_OUT_MANAGER.currentLocalSongIndex = SongItemListHelper.getIndexOfCurrentPlayingLocalTrack()
//                
////                print("AUDIO_OUT_MANAGER.currentLocalSongIndex: \(AUDIO_OUT_MANAGER.currentLocalSongIndex)")
//                
//                AUDIO_OUT_MANAGER.currentLocalSongIndex += 1
//            }
//            
//            AUDIO_OUT_MANAGER.currentSong = AUDIO_OUT_MANAGER.localObservableTrackList[AUDIO_OUT_MANAGER.currentLocalSongIndex]
            
        }else{
            
            guard AUDIO_OUT_MANAGER.lobeListSongItems.count > 1 else {
                print("no song in Lobelists, can't skip to next")
                return
            }
            //LobeList Library
            if AUDIO_OUT_MANAGER.currentLobeListSongIndex == AUDIO_OUT_MANAGER.lobeListSongItems.count - 1 {
                AUDIO_OUT_MANAGER.currentLobeListSongIndex = 0
            }else{
                AUDIO_OUT_MANAGER.currentLobeListSongIndex = SongItemListHelper.getIndexOfCurrentPlayingLobeListTrack()
                AUDIO_OUT_MANAGER.currentLobeListSongIndex += 1
            }
            
            AUDIO_OUT_MANAGER.currentSong = AUDIO_OUT_MANAGER.lobeListSongItems[AUDIO_OUT_MANAGER.currentLobeListSongIndex] as! SongItem
        }
        playTrack()
    }
    
    override func skipToPreviousMusic(){

        guard (AUDIO_OUT_MANAGER.player.nowPlayingItem != nil)  else {
//            print("no nowPlayingItem at the moment")
            return
        }
        let currentTimeInSec = Int(AUDIO_OUT_MANAGER.player.currentPlaybackTime)*60
//        print("skipToPreviousMusic: currentTime \( Int(AUDIO_OUT_MANAGER.player.currentPlaybackTime))")
//        print("skipToPreviousMusic: currentTime \( currentTimeInSec)")
        
        guard currentTimeInSec > 3 else {
            skipToBeginning()
            return
        }

        if AUDIO_OUT_MANAGER.musicPlayerState == .Library{
            if AUDIO_OUT_MANAGER.currentLocalSongIndex > 0 {
                AUDIO_OUT_MANAGER.currentLocalSongIndex-=1
                AUDIO_OUT_MANAGER.currentSong = AUDIO_OUT_MANAGER.localObservableTrackList[AUDIO_OUT_MANAGER.currentLocalSongIndex] 
            }

        }else{
            if AUDIO_OUT_MANAGER.currentLobeListSongIndex > 0 {
                AUDIO_OUT_MANAGER.currentLobeListSongIndex-=1
                AUDIO_OUT_MANAGER.currentSong = AUDIO_OUT_MANAGER.localObservableTrackList[AUDIO_OUT_MANAGER.currentLobeListSongIndex] 
            }

        }
        playTrack()
        
    }
    override func skipToBeginning(){
        guard AUDIO_OUT_MANAGER.localObservableTrackList.count > 0 else {
            return
        }
        AUDIO_OUT_MANAGER.player.skipToBeginning()
        
    }
}
