//
//  SpotifyHelper.swift
//  Lobe
//
//  Created by Professional on 2016-07-26.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

import UIKit

class SpotifyHelper: NSObject {

    static let sharedInstance = SpotifyHelper()
    private override init() {}
    
    let kClientId = "20beb54968d44f1fbd9031b41d62160b"
    let kCallbackURL = "lobe://spotify/"
    let kTokenSwapServiceURL = "https://spotify-lobe-swap-service.herokuapp.com/swap"
    let ktokenRefreshServiceURL = "https://spotify-lobe-swap-service.herokuapp.com/refresh"
    
    var session:SPTSession?
    var player:SPTAudioStreamingController?
    let auth:SPTAuth = SPTAuth.defaultInstance()
    var spotifyAuthViewController:SPTAuthViewController = SPTAuthViewController.authenticationViewController()
}

extension SpotifyHelper {
    
    func setupAuthClient(){
        
        auth.clientID =  kClientId
        auth.redirectURL = NSURL(string: kCallbackURL)
        auth.requestedScopes = [SPTAuthStreamingScope]
        auth.sessionUserDefaultsKey = "SpotifySession"
        auth.tokenSwapURL = NSURL(string: kTokenSwapServiceURL)
        auth.tokenRefreshURL = NSURL(string: ktokenRefreshServiceURL)
        auth.requestedScopes = [SPTAuthStreamingScope,SPTAuthPlaylistReadPrivateScope,SPTAuthUserLibraryReadScope]
        
    }
    func loginWithSpotify(){
        

        self.setupAuthClient()
        
        delay(0.1) {
            UIApplication.sharedApplication().openURL(self.auth.loginURL)
            return
        }
    }
    
    func successLoginCallBack(sessionObject session:SPTSession){
        
        saveSessionInCache(session:session)
        auth.session = session
        let accessToken = session.accessToken
        let canonicalUsername = session.canonicalUsername
        let encryptedRefreshToken = session.encryptedRefreshToken
        let expirationDate = session.expirationDate
        
        print("session: \(session)")
        print("accessToken: \(accessToken)")
        print("canonicalUsername: \(canonicalUsername)")
        print("encryptedRefreshToken: \(encryptedRefreshToken)")
        print("expirationDate: \(expirationDate)")
        
        let accessTokenExpirationInSec = expirationDate.timeIntervalSince1970
        //Notify Web
        NativeToWebHelper.sharedInstance.loginWithSpotifyUsingInfo(accessToken:accessToken, canonicalUsername: canonicalUsername, mExpirationDateInSec: Int64(accessTokenExpirationInSec),encryptedRefreshToken:encryptedRefreshToken ?? "")
        SpotifyHelper.sharedInstance.session = session
//        sessionObject.
        
    }
    
    func delay(delay:Double, closure:()->()) {
        dispatch_after(
            dispatch_time(
                DISPATCH_TIME_NOW,
                Int64(delay * Double(NSEC_PER_SEC))
            ),
            dispatch_get_main_queue(), closure)
    }
    
    func retrieveSessionFromCache(){
        
        let userDefaults = NSUserDefaults.standardUserDefaults()
        guard let sessionObj:AnyObject = userDefaults.objectForKey("SpotifySession") else {return}
        let sessionDataObj = sessionObj as! NSData
        let session = NSKeyedUnarchiver.unarchiveObjectWithData(sessionDataObj) as! SPTSession
        guard session.isValid() else {
            print("Session invalid")
            self.refreshSessionToken()
            return
        }
        print("Session valid!")
        self.session = session
        self.auth.session = session
        
        NativeToWebHelper.sharedInstance.fromNativePing(pingType: "updateSpotifyAccessToken", params: ["access_token":session.accessToken])
        self.loginPlayerWithSessionToken(sessionToken: session.accessToken)
    }
    
    func saveSessionInCache(session session:SPTSession){
        
        let userDefaults = NSUserDefaults.standardUserDefaults()
        let sessionData = NSKeyedArchiver.archivedDataWithRootObject(session)
        userDefaults.setObject(sessionData, forKey: "SpotifySession")
        userDefaults.synchronize()
    }
}

extension SpotifyHelper {
    
    
    func loginPlayerWithSessionToken(sessionToken sessionToken:String!){
        
        SPTCacheData.self
        
        if player == nil {
            do{
                player = SPTAudioStreamingController.sharedInstance()
                try self.player!.startWithClientId(auth.clientID)
                player!.playbackDelegate = self
                player!.delegate = self
                
                self.player!.loginWithAccessToken(sessionToken)
            }catch{
                print("problem")
            }
            
        }
    }

    func refreshSessionToken() {
        
        let userDefaults = NSUserDefaults.standardUserDefaults()
        guard let sessionObj:AnyObject = userDefaults.objectForKey("SpotifySession") else {return}
        let sessionDataObj = sessionObj as! NSData
        let session = NSKeyedUnarchiver.unarchiveObjectWithData(sessionDataObj) as! SPTSession
        self.auth.session = session
        
        if session.isValid() {
            print("Session valid no need to refresh")
            NativeToWebHelper.sharedInstance.fromNativePing(pingType: "updateSpotifyAccessToken", params: ["access_token":session.accessToken])
            self.loginPlayerWithSessionToken(sessionToken: session.accessToken)
            
        }else{
            
            guard (auth.session != nil) else {
                print("auth has no valid session")
                return
            }
            
            guard auth.hasTokenRefreshService else {
                print("No hasTokenRefreshService")
                return
            }
            
            auth.renewSession(auth.session, callback: { (error, renewedSession) -> Void in
                guard error == nil else {
                    print("Failed renewing session token with error: \(error)")
                    return
                }
                guard renewedSession != nil else {
                    print("Failed renewing session token..session invalid")
                    return
                }
                print("success renewedSession")
                self.saveSessionInCache(session:renewedSession)
                self.auth.session = renewedSession
                NativeToWebHelper.sharedInstance.fromNativePing(pingType: "updateSpotifyAccessToken", params: ["access_token":renewedSession.accessToken])
                self.loginPlayerWithSessionToken(sessionToken: renewedSession.accessToken)
            })
            
        }
    }
    
}

//Playback
extension SpotifyHelper {
    
    func playTrack(trackInfo:TrackInfo){
        guard let player = self.player else {return}
        var trackList = [NSURL]()
        trackList.append(NSURL(string: trackInfo.track_url)!)  //ex: "spotify:track:58s6EuEYJdlb0kO7awm3Vp"
        
        print(SPTAudioStreamingController.sharedInstance().loggedIn)
        
        player.playURIs(trackList, fromIndex:0, callback:{ (error) -> Void in
            
            guard error == nil else {
                print(error)
                return
            }
            print(SPTAudioStreamingController.sharedInstance().isPlaying)
        })
    }
    
    func playPause(isPlaying isPlaying:Bool){
        
        guard let player = self.player else {return}
        player.setIsPlaying(isPlaying) { (error) -> Void in
            
        }
    }
}

extension SpotifyHelper:SPTAudioStreamingDelegate ,SPTAudioStreamingPlaybackDelegate{
    
    func audioStreamingDidLogin(audioStreaming: SPTAudioStreamingController!) {
        
        print("Player Logged In")
        
    }
    
    func audioStreaming(audioStreaming: SPTAudioStreamingController!, didEncounterError error: NSError!) {
        print("Error connecting")
    }
    
    func audioStreaming(audioStreaming: SPTAudioStreamingController!, didStartPlayingTrack trackUri: NSURL!) {
        print("audioStreaming didStartPlayingTrack")
    }
    
}