//
//  FileManagerHelper.swift
//  Lobe
//
//  Created by Professional on 2016-03-20.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

import UIKit
//import RNActivityView
import JGProgressHUD

class FileManagerHelper: NSObject {

    static func doesFileExistAtPath(path:String) -> Bool{
        let fileManager = NSFileManager.defaultManager()
        var isDir : ObjCBool = false
        return fileManager.fileExistsAtPath(path, isDirectory:&isDir)
    }
    
    static func createUploadDirectory(){
        
        let fileManager = NSFileManager.defaultManager()
        
        let dirPaths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory,
            .UserDomainMask, true)
        let docsDir = dirPaths[0]
        let newDir = docsDir + "/uploadFolder"
        var isDir : ObjCBool = false
        
        guard !fileManager.fileExistsAtPath(newDir, isDirectory:&isDir) else{
//            print("Folder Already exist")
            return
        }
        //        guard !isDir else {
        //            print("Path is not a directory")
        //            return
        //        }
        
        do {
            try fileManager.createDirectoryAtPath(newDir, withIntermediateDirectories: true, attributes: nil)
//            print("Success creating folder")
        } catch {
            print("Could not create upload folder: \(error)")
        }
    }
    
    static func deleteUploadDirectory(){
        
        let fileManager = NSFileManager.defaultManager()
        let dirPaths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory,
            .UserDomainMask, true)
        let docsDir = dirPaths[0]
        let newDir = docsDir + "/uploadFolder"
        var isDir : ObjCBool = false
        
        guard fileManager.fileExistsAtPath(newDir, isDirectory:&isDir) else{
//            print("Folder dont exist")
            return
        }
        guard isDir else {
//            print("Path is not a directory")
            return
        }
        
        do {
            try fileManager.removeItemAtPath(newDir)
//            print("Success Delete upload folder")
        } catch {
//            print("Could not delete upload folder: \(error)")
        }
    }
    
    
    static func resetUploadDirectory(){
        
        let fileManager = NSFileManager.defaultManager()
        let docsDir = NSSearchPathForDirectoriesInDomains(.DocumentDirectory,
            .UserDomainMask, true)[0]
        
        let newDir = docsDir + "/uploadFolder"
        var isDir : ObjCBool = false
        
        guard fileManager.fileExistsAtPath(newDir, isDirectory:&isDir) else{
//            print("Folder dont exist")
            return
        }
        guard isDir else {
//            print("Path is not a directory")
            return
        }
        
        deleteUploadDirectory()
        createUploadDirectory()
    }
    
    static func shouldSyncAudio() -> Bool{
        
        guard AUDIO_OUT_MANAGER.isMusicPlaying() == true else {return false }
        guard LobeStateHelper.sharedInstance.state != .Local else {return false }
        guard !AUDIO_OUT_MANAGER.currentSong.trackInfo.mIsYoutubeVideo else {return false }
        
        return true
    }
}

