//
//  LocationHelper.swift
//  OfficeHours
//
//  Created by Eliel Gordon on 12/11/15.
//  Copyright © 2015 Saltar Group. All rights reserved.
//

import UIKit
import CoreLocation

typealias LocationCallback = (address: String) -> Void

class LocationHelper: NSObject, CLLocationManagerDelegate {
    
    //Singleton
    static let sharedInstance = LocationHelper()
    
    var location_latitude: CLLocationDegrees = -1
    var location_longitude: CLLocationDegrees = -1
    
    var manager = CLLocationManager()
    
    private override init() {
        super.init()
//        print("LocationHelper init")
        self.manager.delegate = self
        self.manager.distanceFilter  = 20000                         // Must move at least 3km
        self.manager.delegate = self
        self.manager.distanceFilter  = 20000                         // Must move at least 3km
        
        self.manager.requestWhenInUseAuthorization()
        
    }
    
    func startMonitoring(){
        
        
        self.manager.desiredAccuracy = kCLLocationAccuracyBest
        //        self.manager.requestAlwaysAuthorization()
        self.manager.startUpdatingLocation()
    }
    
    func stopMonitoring(){
        self.manager.stopUpdatingLocation()
        
    }
    
    func updateLocation(){
        
        self.startMonitoring()
        
        guard let location = self.manager.location else {
            debugPrint("getDeviceLocation Failed to get location..")
            return
        }
        let latitude: CLLocationDegrees = location.coordinate.latitude
        let longitude: CLLocationDegrees = location.coordinate.longitude
        
        LocationHelper.sharedInstance.location_latitude = latitude
        LocationHelper.sharedInstance.location_longitude = longitude
        
        self.stopMonitoring()
    }
    
    /*
    * Gets the address of a coordinate location
    */
    func getUserLocationName(callback : LocationCallback) {
        let geoCoder = CLGeocoder()
        
        guard let locValue = manager.location?.coordinate else {return}
        
        let location = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
        
        geoCoder.reverseGeocodeLocation(location) {
            
            (placemarks, error) -> Void in
            
            if error != nil {
                print(error!.description)
            } else {
                let placeArray = placemarks
                
                // Place details
                var placeMark: CLPlacemark!
                placeMark = placeArray?[0]
                
                let streetNum = placeMark.subThoroughfare ?? ""
                let postCode = placeMark.postalCode ?? ""
                let state = placeMark.administrativeArea ?? ""
                //                 City
                if let city = placeMark.addressDictionary!["City"] as? String {
                    if let street = placeMark.addressDictionary!["Thoroughfare"] as? String {
                        let address = streetNum + " " + street + ", " + city + " " + state + " " + postCode
                        callback(address: address)
                    }
                }
            }
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateToLocation newLocation: CLLocation, fromLocation oldLocation: CLLocation) {
    }
    
    
    func locationManager(manager: CLLocationManager,
        didChangeAuthorizationStatus status: CLAuthorizationStatus) {
            
                        switch status {
                            case CLAuthorizationStatus.Restricted: break
                            case CLAuthorizationStatus.Denied: break
                            case CLAuthorizationStatus.NotDetermined: break
                            case CLAuthorizationStatus.Authorized:
//                                print("Got location authorisations")
                                guard let location = LocationHelper.sharedInstance.manager.location else {
                                    return
                                }
                                self.location_latitude = location.coordinate.latitude
                                self.location_longitude = location.coordinate.longitude
                            
//                            case CLAuthorizationStatus.AuthorizedAlways:break
//                            case CLAuthorizationStatus.AuthorizedWhenInUse:break
                            default: break

                        }

    }
}
