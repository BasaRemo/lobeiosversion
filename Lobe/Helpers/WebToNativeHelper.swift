//
//  WebToNativeHelper.swift
//  Lobe
//
//  Created by Professional on 2016-01-05.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

import Foundation
import SwiftyJSON
import MediaPlayer
import AVFoundation
import AudioToolbox
//import JLToast
import Parse
import Crashlytics
//import RNActivityView
import JGProgressHUD

let WebToNativeCalls = WebToNative()
let DEBUG = false

struct WebToNative {
    
    let testCall = "testCall"
    let showToastMessage = "showToastMessage"
    let setTimeDiffWithFirebase = "setTimeDiffWithFirebase"
    let toJavaCallSetAudioSourceMode = "toJavaCallSetAudioSourceMode"
    let playNotifSound = "playNotifSound"
    let setIsSignedInState = "setIsSignedInState"
    let setMyPersonalInfo = "setMyPersonalInfo"
    let setLobeList = "setLobeList"
    let setSuggestionList = "setSuggestionList"
    let backToNormal = "backToNormal"
    let doSynchoFutureEventTest = "doSynchoFutureEventTest"
    let startStreamingFromUrl = "startStreamingFromUrl"
    let resumeMusic = "resumeMusic"
    let pauseMusic = "pauseMusic"
    let toggleDrawerMenuClick = "toggleDrawerMenuClick"
    let groupUploadRequestReceived = "groupUploadRequestReceived"
    let addTrackUniqueToLobeList = "addTrackUniqueToLobeList"
    let createAllLocalFileChunks = "createAllLocalFileChunks"
    let uploadFileToServer = "uploadFileToServer"
    let logFromSocial = "logFromSocial"
    let setCurrYoutubeTrackLength = "setCurrYoutubeTrackLength"
    
}

class WebToNativeHelper: NSObject {
    
    static let sharedInstance = WebToNativeHelper()
    let appDelegateInstance = UIApplication.sharedApplication().delegate as? AppDelegate
    let toastHUD:JGProgressHUD = JGProgressHUD(style: .Dark)
//    let streamingManager_sharedInstance = StreamingManager.sharedInstance()
//    let uploadManager_sharedInstance = UploadManager.sharedInstance()
    
    private override init() {
        super.init()
    }
    
    func goToViewFromBackAction(backToViewString:String){
        
    }
    
    // TODO: -  Nabil : Fonction a utiliser
    func createAllLocalFileChunks(trackInfo:JSON, nbSecondsPerChunk:Int){
        //createAllLocalFileChunks:(NSString*)persistentID nbSecondsPerChunk:(double)nbSecondsPerChunk
        let trackInfo = TrackInfo(trackInfoJson: trackInfo)

        let localPath = trackInfo.mCurrChunkLocalPath
//        print("localPath: \(localPath)")
        let localPathArr = localPath.componentsSeparatedByString("id=")
        let persistantID = localPathArr[1] // must be 3839944631263754183 for exemple
        
//        print("SongID: \(persistantID)")
        
        uploadManager_sharedInstance.createAllLocalFileChunks(persistantID, nbSecondsPerChunk: UInt32(nbSecondsPerChunk))
        // TODO: Make your calls here. I already create the trackInfo. You can just use it as needed
    }
    
    // TODO: -  Nabil : Fonction a utiliser
    func uploadFileToServer(chunkIndex:UInt32, localFilePath:String,correlationID:String, chunkStartTimeServer:Int64){
//        print("LOCAL PATH: \(localFilePath)")
        uploadManager_sharedInstance.startUploadingWithURL(chunkIndex, url: localFilePath, correlationID: correlationID, chunkStartTimeServer:chunkStartTimeServer);
    }
    
    func toggleDrawerMenuClick(){
        AUDIO_OUT_MANAGER.showMenu()
    }
    
    //Unused for now
    func groupUploadRequestReceived(trackObjStr: JSON){
        
//        let trackInfo = TrackInfo(trackInfoJson: trackObjStr)
        //        let localPath = trackInfo.mCurrChunkLocalPath
        //        let localPathArr = localPath.componentsSeparatedByString("id=")
        //        let persistantID = localPathArr[1] // must be 3839944631263754183 for exemple
        //TODO: - NABIL This needs to be a trackInfo with and we do the upload using the local path
        //"track_chunk_local_path": "ipod-library://item/item.mp3?id=3839944631263754183"
        
//        let persistantID = trackInfo.mID
//        print("persistant ID: \(persistantID)")
//        let musicIdentifier = NSNumberFormatter().numberFromString(persistantID);
//        let query = MPMediaQuery(filterPredicates: NSSet(object:MPMediaPropertyPredicate(value: musicIdentifier, forProperty: MPMediaItemPropertyPersistentID)) as! Set<MPMediaPredicate>)
//        let itemToUpload = (query.items?.first)! as MPMediaItem
        
    }
    
    func addTrackUniqueToLobeList(trackJsonObjString:JSON){
        
        autoreleasepool {
            
            let lockQueue = dispatch_queue_create("lobeList.LockQueue", nil)
            dispatch_sync(lockQueue) {
                //            print("//=================== Add unique track to Lobelist=====================//")
                let songItem = SongItem(mTrackInfo: TrackInfo(trackInfoJson: trackJsonObjString))
                // TODO: - MAKE SURE THE ITEM IS UNIQUE BEFORE Add song item to lobeList
                if (!AUDIO_OUT_MANAGER.lobeListContains(songItem)){
                    //                print("UNIQUE TRACK")
                    AUDIO_OUT_MANAGER.lobeListSongItems.addObject(songItem)
                    AUDIO_OUT_MANAGER.lobeListObservableList.replaceRange(0...0, with: [SongItem()])
                    //                print("Lobelist count: \(AUDIO_OUT_MANAGER.lobeListSongItems.count)")
                    //                print("Lobelist observable count: \(AUDIO_OUT_MANAGER.lobeListObservableList.count)")
                    //                print("//=================== END Adding item to LobeList =====================//")
                }
            }
        }
    }
    
    
    func setLobeList(jsonArrayString:JSON){
        
        autoreleasepool {
            
            //        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            
            let lockQueue = dispatch_queue_create("lobeList.LockQueue", nil)
            dispatch_sync(lockQueue) {
                //            print("//=================== Updating LobeList in background =====================//")
                let lobeList:NSMutableArray = NSMutableArray()
                //        print("func setLobeList jsonArrayString: \(jsonArrayString)")
                for (index,subJson):(String, JSON) in jsonArrayString[0] {
                    //            print("subJson: \(subJson)")
                    let trackInfo = TrackInfo(trackInfoJson: subJson)
                    let songItem = SongItem(mTrackInfo: trackInfo)
                    lobeList.addObject(songItem)
                }
                
                AUDIO_OUT_MANAGER.lobeListSongItems.removeAllObjects()
                AUDIO_OUT_MANAGER.lobeListSongItems.addObjectsFromArray(lobeList as [AnyObject])
                AUDIO_OUT_MANAGER.lobeListObservableList.replaceRange(0...0, with: [SongItem()])
                
                //            print("lobeListCount SERVER: \(lobeList.count)")
                //            print("lobeListCount: \(AUDIO_OUT_MANAGER.lobeListSongItems.count)")
                //            print("Lobelist observable count: \(AUDIO_OUT_MANAGER.lobeListObservableList.count)")
                //            print("//=================== END Updating LobeList =====================//")
            }
            //        }
        }
    }
    
    //TODO:- Clean unused
    func setSuggestionList(jsonObjListString:JSON){
        
        var suggestList = [TrackInfo]()
        for (key,subJson):(String, JSON) in jsonObjListString[0] {
//            print("subJson id \(key): \(subJson)")
            let track = TrackInfo(trackInfoJson: subJson)
            suggestList.append(track)
        }
//        AUDIO_OUT_MANAGER.suggestionList.removeAll()
//        AUDIO_OUT_MANAGER.suggestionList = suggestList

    }
    
    func setUnreadMsgsCount(count:Int){
        //        mUnreadMsgsCount = count;
        //        LobeStateHelper.sharedInstance.notifyUserStateChange()
        
    }
    
    func testCall(testString:String)
    {
//        dispatch_async(dispatch_get_main_queue()) {
//            JLToast.makeText(testString).show()
//        }
    }
    
    
    func showToastMessage(msg:String)
    {
        dispatch_async(dispatch_get_main_queue()) {
            
//            JLToastCenter.defaultCenter().cancelAllToasts()
//            JLToastView.setDefaultValue(
//                 75,
//                forAttributeName: JLToastViewPortraitOffsetYAttributeName,
//                userInterfaceIdiom: .Phone
//            )
//            
//            JLToastView.setDefaultValue(
//                UIFont (name: "HelveticaNeue-Medium", size: 13)!,
//                forAttributeName: JLToastViewFontAttributeName,
//                userInterfaceIdiom: .Phone
//            )
//            let toast = JLToast.makeText(msg, duration: 2.0)
//            toast.show()
//            

            self.toastHUD.indicatorView = nil
            self.toastHUD.textLabel.text = msg
            guard let window = self.appDelegateInstance!.window else {return}
            self.toastHUD.interactionType = .BlockNoTouches
            self.toastHUD.showInView(window)
            self.toastHUD.dismissAfterDelay(2, animated: true)
            self.toastHUD.position = .BottomCenter
            self.toastHUD.marginInsets = UIEdgeInsets(top: 0, left: 0, bottom: 75, right: 0)
            
            //            let failurePopUp:RNActivityView = RNActivityView.showHUDAddedTo(self.appDelegateInstance!.window, animated: true)
            //            failurePopUp.mode = RNActivityViewModeText
            //            failurePopUp.
            //            failurePopUp.labelFont = UIFont(name: "Helvetica Neue", size: 11)
            //            failurePopUp.labelText = msg
            //            failurePopUp.yOffset = Float((self.appDelegateInstance!.window?.frame.height)!/2 - 100)
            //            failurePopUp.removeFromSuperViewOnHide = true
            //            failurePopUp.hide(true, afterDelay: 2.0)
            
        }
    }
    
    
    func setTimeDiffWithFirebase(jsCurrUnixTime:Int64, deltaF_JS:Int64) {
        
        let iosCurrentUnixTime:Int64 = streamingManager_sharedInstance.getCurrentUnixTimeInMs()
        let delta_JS_IOS = jsCurrUnixTime - iosCurrentUnixTime
        AUDIO_OUT_MANAGER.mDeltaTimeFirebaseIOS = deltaF_JS; //Used for sync
        
//        print("jsCurrUnixTime: \(jsCurrUnixTime)")
//        print("iosCurrentUnixTime: \(iosCurrentUnixTime)")
//        print("delta_JS_IOS: \(delta_JS_IOS)")
//        print("AUDIO_OUT_MANAGER.mDeltaTimeFirebaseIOS: \(AUDIO_OUT_MANAGER.mDeltaTimeFirebaseIOS)")
    }
    
    
    func toJavaCallSetAudioSourceMode(mode:String)
    {
//        dispatch_async(dispatch_get_main_queue()) {
            switch (mode) {
            case "broadcasting":
                LobeStateHelper.sharedInstance.state = LobeState.DJ
                break
            case "listening":
                LobeStateHelper.sharedInstance.state = LobeState.Participant
                break
            case "idle":
                self.backToNormal();
            case "none","local": break
            default:
                self.backToNormal();
                break;
            }
//        }
    }
    
    
    func playNotifSound(notifType:String)
    {
        switch (notifType) {
        case "close":
            let systemSoundID: SystemSoundID = 1114
            AudioServicesPlaySystemSound (systemSoundID)
            break;
        case "msg":
            let systemSoundID: SystemSoundID = 1003
            AudioServicesPlaySystemSound (systemSoundID)
            break;
        case "tock":
            let systemSoundID: SystemSoundID = 1104
            AudioServicesPlaySystemSound (systemSoundID)
            break;
        default:
            let systemSoundID: SystemSoundID = 1016
            AudioServicesPlaySystemSound (systemSoundID)
            
        }
    }
    
    
    func setIsSignedInState(isSignedIn: Bool)
    {
        LobeStateHelper.currentUser.isSignedIn = true
        LobeStateHelper.notifyUserStateChange()
    }
    
    
    func setMyPersonalInfo(id:String, name:String, picUrl:String)
    {
        autoreleasepool {
            
            LobeStateHelper.sharedInstance.setCurrentUser(User(mName: name, mParseId: id, mPicUrl: picUrl))
            
            //Set user for push notifications
            print("Setting up push notif installation")
            let query = PFUser.query()
            query!.getObjectInBackgroundWithId(id) {
                (user: PFObject?, error: NSError?) -> Void in
                if error == nil {
                    //                print("I query User \(user)")
                    //Subscribe user to push notifs
                    let installation = PFInstallation.currentInstallation()
                    installation["user"] = user
                    installation["user_id"] = user?.objectId
                    installation.saveInBackground()
                    
                } else {
                    print("failed setting up push installation with error: \(error)")
                }
            }
            
            if let _ = LobeStateHelper.sharedInstance.pushNotif {
                print("sending push notif to web")
                NativeToWebHelper.sharedInstance.JavaCallPushNotifActionReceived()
            }
            LobeStateHelper.notifyUserStateChange()
            
            //set user info for crash analytics
            Crashlytics.sharedInstance().setUserIdentifier("\(id)")
            Crashlytics.sharedInstance().setUserName("\(name)")
        }
        
    }
    
    
    func backToNormal(){
        LobeStateHelper.sharedInstance.state = LobeState.Local
    }
    
    
    func doSynchoFutureEventTest( time_stamp:UInt64, music_start_time_delay:UInt64){
    }
    
    
    func startStreamingFromUrl(trackObjStr:JSON, server_time_stamp:Int64, music_start_time_delay:Int64){
        
        autoreleasepool {
            
            let trackInfo = TrackInfo(trackInfoJson: trackObjStr)
            AUDIO_OUT_MANAGER.currentSong = SongItem(mTrackInfo: trackInfo)
            
            
            var musicStartTimeInUnixTIme:Int64 = server_time_stamp -  AUDIO_OUT_MANAGER.mDeltaTimeFirebaseIOS + music_start_time_delay
            let persistantID = trackInfo.mID
            let localPath = trackInfo.mCurrChunkLocalPath
            let song_uid = trackInfo.mID
            
            
            //            #if DEBUG
            //                debugPrint("server_time_stamp: \(server_time_stamp)")
            //                debugPrint("music_start_time_delay: \(music_start_time_delay)")
            //                print("mDeltaTimeFirebaseIOS: \(AUDIO_OUT_MANAGER.mDeltaTimeFirebaseIOS)")
            //
            //                print("musicStartTimeInUnixTIme: \(musicStartTimeInUnixTIme)")
            //                print("persistant ID: \(persistantID)")
            //                print("LOCAL DEVICE ID: \(unique_device_id)")
            //                print("SONG DEVICE ID: \(trackInfo.mUniqueDeviceId)")
            //            #endif
            
            //=============================================================================
            // TODO: - IMPORTANT: Ne pas utiliser un Index pour joueur la chanson courante , voir la fonction de download du streaming_manager
            //=============================================================================
            AUDIO_OUT_MANAGER.shouldPlayYoutubeVideo.value = false
            if trackInfo.mUniqueDeviceId == unique_device_id {
                //                print("MY LOCAL SONG")
                //                if (AUDIO_OUT_MANAGER.isMusicPlaying() == false){
                //                    streamingManager_sharedInstance.muteAudioQueue()
                //                }
                
                guard FileManagerHelper.doesFileExistAtPath(localPath) else {
                    
                    //                    print("User's local song BUT file didn't found on upload directory..")
                    //                    AUDIO_OUT_MANAGER.resumeStreamingAudio()
                    return
                }
                //                NativeToWebHelper.sharedInstance.youtube_delegate?.nativeCallSetIsPlayingMediaContentAllowed(false)
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                    streamingManager_sharedInstance.downloadChuncksFromUrl(trackInfo.mCurrChunkUrl, localPath:localPath , correlationID:trackInfo.genCorrolationID(), musicStartTimeInUnixTime:musicStartTimeInUnixTIme,isLocalSong:true, mNbOfBytesPassedBeforeChunk:music_start_time_delay)
                }
                
            }else{
                
                switch (trackInfo.track_type) {
        
                    case "youtube":
                        AUDIO_OUT_MANAGER.stopStreamingAudio()
                    case "spotify":
                        SpotifyHelper.sharedInstance.playTrack(trackInfo)
                    case "local":
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                            streamingManager_sharedInstance.downloadChuncksFromUrl(trackInfo.mCurrChunkUrl, localPath:localPath, correlationID:trackInfo.genCorrolationID(), musicStartTimeInUnixTime:musicStartTimeInUnixTIme,isLocalSong:false, mNbOfBytesPassedBeforeChunk:music_start_time_delay)
                        }
                    
                    default:break
        
                }
                
                LobeStateHelper.sharedInstance.updateYoutubeViewState()
            }
            
            LobeStateHelper.sharedInstance.updateYoutubeViewState()
            AUDIO_OUT_MANAGER.mainVC.updateMusicPlayingUI()
            guard AUDIO_OUT_MANAGER.isMusicPlaying() == true else {
                return
            }
            AUDIO_OUT_MANAGER.isGroupMusicPlaying = true
            AUDIO_OUT_MANAGER.mainVC.updateMusicPlayingUI()
        }
    }
    
    
    func resumeMusic(){
        //Resume stream Music
//        AUDIO_OUT_MANAGER.playMusic()
    }
    
    func pauseMusic(){
        AUDIO_OUT_MANAGER.pauseMusic()
    }
    
}
