//
//  LobeAppState.swift
//  Lobe
//
//  Created by Professional on 2016-02-20.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

import UIKit

let streamingManager_sharedInstance = StreamingManager.sharedInstance()
let uploadManager_sharedInstance = UploadManager.sharedInstance()

class LobeAppState: NSObject {

    //MARK:- audio playback
    final func pauseTrack(){
//        print("In PAUSE STATE")
        //We do the pause everywhere to be safe
        // Local Pause
        if AUDIO_OUT_MANAGER.isPlaying {
            AUDIO_OUT_MANAGER.player.pause()
            AUDIO_OUT_MANAGER.isPlaying = false
        }
        //Group playing state
        AUDIO_OUT_MANAGER.isGroupMusicPlaying = false
        //TODO:- AudioQueue Pause : trigger the pause for AudioQueue : Make sure sync doesn't stop
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            streamingManager_sharedInstance.pauseAudioQueue()
        }
        
        AUDIO_OUT_MANAGER.shouldPlayYoutubeVideo.value = false
        //Notify real audio playing state to Social (Javascript)
        NativeToWebHelper.sharedInstance.setIsPlayingMediaContentAllowed(false)
        NativeToWebHelper.sharedInstance.youtube_delegate?.nativeCallSetIsPlayingMediaContentAllowed(false)
        SpotifyHelper.sharedInstance.playPause(isPlaying: false)
        
    }

    func stopSocialAudio(){
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            social_slider_value.value = 0.0
            if AUDIO_OUT_MANAGER.lobeListSongItems.count > 0 {
                
                AUDIO_OUT_MANAGER.lobeListSongItems = NSMutableArray()
                AUDIO_OUT_MANAGER.lobeListObservableList.replaceRange(0...0, with: [SongItem()])
            }
            streamingManager_sharedInstance.pauseAudioQueue()
            streamingManager_sharedInstance.resetAudioQueueSetup()
            NativeToWebHelper.sharedInstance.youtube_delegate?.nativeCallSetIsPlayingMediaContentAllowed(false)
            NativeToWebHelper.sharedInstance.setIsPlayingMediaContentAllowed(false)
            AUDIO_OUT_MANAGER.isGroupMusicPlaying = false
            AUDIO_OUT_MANAGER.mainVC.updateMusicPlayingUI()
            SpotifyHelper.sharedInstance.playPause(isPlaying: false)
//        }
    }
    
    func stopStreamingAudio(){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            social_slider_value.value = 0.0
            streamingManager_sharedInstance.pauseAudioQueue()
        }
    }
    
    func resumeStreamingAudio(){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            streamingManager_sharedInstance.resumeAudioQueue()
        }
    }
    
    //Ovverrided functions
    func playTrack(){
        preconditionFailure("LobeAppState playTrack() : This method must be overridden")
    }
    func resumeTrack(){
        preconditionFailure("LobeAppState resumeTrack(): This method must be overridden")
    }
    func isTrackPlaying() -> Bool {
        preconditionFailure("LobeAppState isTrackPLaying() : This method must be overridden")
    }
    func directPlayTrack(){
        preconditionFailure("LobeAppState directPlayTrack() : This method must be overridden")
    }
    func skipToNextMusic(){
        preconditionFailure("LobeAppState skipToNextMusic() : This method must be overridden")
    }
    func skipToPreviousMusic(){
        preconditionFailure("LobeAppState skipToPreviousMusic() : This method must be overridden")
    }
    func skipToBeginning(){
        preconditionFailure("LobeAppState skipToBeginning() : This method must be overridden")
    }
}
