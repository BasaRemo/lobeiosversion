//
//  JsonHelper.swift
//  Lobe
//
//  Created by Professional on 2016-03-20.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

import UIKit
import SwiftyJSON
//import Chronos
//import Dollar

class JsonHelper: NSObject {

    //MARK: - JSON Encoding
    static func trackInfoToJSON(trackInfo:TrackInfo) -> JSON {
        return JSON(trackInfo.toDictionary())
    }
    
    static func musicListToJSONArrayData(musicList: [TrackInfo]) -> JSON {
        
        var dictPTracks:[JSON] = []
        var dictionary = [[String:AnyObject]]()
        
        for track in musicList{
            dictPTracks.append(JSON(track.toDictionary()))
            dictionary.append(track.toDictionary())
            //            print("Track: \(track.toDictionary())")
        }
        //        print("valid JSON: \(dictPTracks)")
        let dataDict  = NSMutableDictionary()
        dataDict.setValue(dictionary, forKey: "data")
        
        return JSON(dataDict)
    }
    
    static func musicListToJSONObjectData(musicList: [TrackInfo]) -> JSON{
        
        let trackJsonDict :NSMutableDictionary = [:]
        for track in musicList{
            trackJsonDict.setValue(track.toDictionary(), forKey: "\(track.mUnixTimeStampAtUserPush)")
        }
        
        let dataDict:NSMutableDictionary = [:]
        dataDict.setValue(trackJsonDict, forKey: "data")
        return JSON(dataDict)
    }
    
    static func chunkInfoListToJSONArrayData(musicList: [ChunkInfo]) -> JSON {
        
        //        var dictPTracks:[JSON] = []
        var dictionary = [[String:AnyObject]]()
        
        for track in musicList{
            //            dictPTracks.append(JSON(track.toDictionary()))
            dictionary.append(track.toDictionary())
            //            print("Track: \(track.toDictionary())")
        }
        
        //        print("valid JSON: \(dictPTracks)")
        let dataDict  = NSMutableDictionary()
        dataDict.setValue(dictionary, forKey: "data")
        
        return JSON(dataDict)
    }
    
    static func musicListToJSONArray(musicList: [TrackInfo]) -> [JSON] {

        var dictPTracks:[JSON] = []
        for track in musicList{
            dictPTracks.append(JSON(track.toDictionary()))
//            print("Track: \(track.toDictionary())")
        }
//        print("valid JSON: \(dictPTracks)")

        return dictPTracks
    }

    static func musicListToJSONObject(musicList: [TrackInfo]) -> JSON{

        let trackJsonDict :NSMutableDictionary = [:]
        for track in musicList{
            trackJsonDict.setValue(track.toDictionary(), forKey: "\(track.mUnixTimeStampAtUserPush)")
        }
        //        print("valid DICT: \(trackJsonDict))")
        //        print("valid JSON: \(JSON(trackJsonDict)))")
        
        return JSON(trackJsonDict)
    }
    
    static func replaceBadChar(str:String) -> String  {
        return str.replace("\'", withString: "\\\'").replace("\"", withString: "\\\"").replace("\n", withString: "\\n").replace("\r", withString: "")
        
        //        return str.replace(/\n/g, "\\\\n").replace(/\/g, "\\\\r").replace(/ t/g, "\\\\t");
    }
    
    static func replaceChar(str:String) -> String  {
        return str.replace("\'", withString: "").replace("\"", withString: "").replace("\n", withString: "").replace("\r", withString: "")
        
        //        return str.replace(/\n/g, "\\\\n").replace(/\/g, "\\\\r").replace(/ t/g, "\\\\t");
    }
}
