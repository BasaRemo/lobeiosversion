//
//  LobeStateHelper.swift
//  Lobe
//
//  Created by Professional on 2016-01-10.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

import UIKit
import SwiftyJSON
import CoreLocation
import Bond
import Parse

enum LobeState :String {
    case DJ = "Broadcast"
    case Participant = "Listening"
    case Local = "Local"
    
    private init () {
        self = .Local
    }
}
let lobeStateNotificationKey = "lobeStateChangeNotification"
let userStateNotificationKey = "userStateChangeNotification"
let pushNotificationKey = "pushNotificationKey"

class LobeStateHelper: NSObject {
    
    var userActivityTimer = NSTimer()
    
    var menu_selected_index:Observable<Int> = Observable<Int>(1)
    var isChatViewOpened:Observable<Bool> = Observable<Bool>(false)
    var current_visible_view:Observable<String> = Observable<String>("localMusicView")

    var pushNotif:JSON?
    var youtube_view_mode:YoutubeViewMode = .Large
    
    static let sharedInstance = LobeStateHelper()
    
    static var currentUser = User(){
        didSet{
//            print("USER STATE CHANGED")
            currentUser.downloadCurrentUserImage()
            notifyUserStateChange()
        }
    }
    
    private override init() {
        super.init()
    }
    
    var state = LobeState() {
        didSet{
//            print("State moved from \(oldValue) to \(state)")
            switch state {
                case .Local:
                    AUDIO_OUT_MANAGER.lobeAppState = LocalState.getSharedInstance()
                    
                    if oldValue == .Participant || oldValue == .DJ {
                        AUDIO_OUT_MANAGER.isGroupMusicPlaying = false
                        AUDIO_OUT_MANAGER.pauseMusic()
//                        AUDIO_OUT_MANAGER.mainVC.updateMusicPlayingUI()
                        notifyLobeStateChange()
                    }
                
                case .DJ:
                    AUDIO_OUT_MANAGER.lobeAppState = HostState.getSharedInstance()
                    AUDIO_OUT_MANAGER.isGroupMusicPlaying = true
                    AUDIO_OUT_MANAGER.playMusic()
//                    AUDIO_OUT_MANAGER.mainVC.updateMusicPlayingUI()
//                    AUDIO_OUT_MANAGER.pauseMusic()
                    notifyLobeStateChange()
                case .Participant:
                     AUDIO_OUT_MANAGER.lobeAppState = ParticipantState.getSharedInstance()
                     AUDIO_OUT_MANAGER.isGroupMusicPlaying = true
                     AUDIO_OUT_MANAGER.playMusic()
//                     AUDIO_OUT_MANAGER.mainVC.updateMusicPlayingUI()
//                     AUDIO_OUT_MANAGER.pauseMusic()
                    notifyLobeStateChange()
            }
            updateYoutubeViewState()
        }
    }
    
    // Lobe states
    func addLobeStateObserver(viewController: AnyObject){
        NSNotificationCenter.defaultCenter().addObserver(viewController, selector: "lobeStateChanged", name: lobeStateNotificationKey, object: nil)
    }
    func notifyLobeStateChange() {
        NSNotificationCenter.defaultCenter().postNotificationName(lobeStateNotificationKey, object: nil)
    }
    
    //User/Social State
    func addUserStateObserver(viewController: AnyObject){
        NSNotificationCenter.defaultCenter().addObserver(viewController, selector: "userStateChanged", name: userStateNotificationKey, object: nil)
    }
    static func notifyUserStateChange() {
        NSNotificationCenter.defaultCenter().postNotificationName(userStateNotificationKey, object: nil)
    }
    
    //Push notif received
    func addPushNotificatonObserver(viewController: AnyObject){
        NSNotificationCenter.defaultCenter().addObserver(viewController, selector: Selector("actOnPushNotifReceived"), name: pushNotificationKey, object: nil)
    }
    
    func notifyPushNotifReceived() {
        NSNotificationCenter.defaultCenter().postNotificationName(pushNotificationKey, object: nil)
    }
}

//View hierarchy
extension LobeStateHelper {
    
    func updateYoutubeViewState(){
        
//        guard LobeStateHelper.sharedInstance.youtube_view_mode != .FullScreen else {return}
        dispatch_async(dispatch_get_main_queue()) {
            
            if LobeStateHelper.sharedInstance.state == .Participant || LobeStateHelper.sharedInstance.state == .DJ {
                
                switch (AUDIO_OUT_MANAGER.currentSong.trackInfo.track_type) {
                    case "youtube":
                       AUDIO_OUT_MANAGER.mainVC.toggleYoutubePlayerView(isHidden: false)
                    case "spotify":
                        AUDIO_OUT_MANAGER.mainVC.toggleYoutubePlayerView(isHidden: true)
                    case "local":
                        AUDIO_OUT_MANAGER.mainVC.toggleYoutubePlayerView(isHidden: true)
                        
                    default:break
                    
                }

            }else {
                
                AUDIO_OUT_MANAGER.mainVC.toggleYoutubePlayerView(isHidden: true)
            }
        }
    }
    
    func checkChatViewState(){
        dispatch_async(dispatch_get_main_queue()) {
            
            if self.current_visible_view.value == "groupView" {
            
                if self.isChatViewOpen() {
//                    AUDIO_OUT_MANAGER.mainVC.resizeYoutubeView(isDocked: true)
                }else{
//                    AUDIO_OUT_MANAGER.mainVC.resizeYoutubeView(isDocked: false)
                }
                AUDIO_OUT_MANAGER.mainVC.toggleYoutubePlayerView(isHidden: false)
            }
            
        }
    }
    
    
    func setIsChattingPanelOpen(isOpen:Bool){
        
        isChatViewOpened.value = isOpen
    }
    
    func isChatViewOpen() -> Bool {
        
        return isChatViewOpened.value
    }
    
    func observeChatViewState(){
        
        self.isChatViewOpened.observe { isChatOpen in
            
            self.updateYoutubeViewState()
        }
        
        self.current_visible_view.observe { isChatOpen in
            
            self.updateYoutubeViewState()
        }
        
    }
}

extension LobeStateHelper {
    
    func setCurrentUser(user:User){
        LobeStateHelper.currentUser = user
        self.userActivityTimer.invalidate()
        self.userActivityTimer = NSTimer.scheduledTimerWithTimeInterval(60, target: self, selector: #selector(self.updateUserActivity), userInfo: nil, repeats: true)
    }
    
    func updateUserActivity(){
        
        let parse_id:NSString =  LobeStateHelper.currentUser.parseID
        guard parse_id.isEqualToString("unavailable") else {
            return
        }
//        print("************** Update user activity ***************")
        PFCloud.callFunctionInBackground("registerActivity",
                                         withParameters: ["user_id":LobeStateHelper.currentUser.parseID])
        { (object:AnyObject?, error:NSError?) -> Void in
            
            guard error == nil else {
//                print("error calling cloud code: \(error?.description)")
                return
            }
//            let user = object as! PFUser
            let message = object as! String
//            print("Success registerActivity")
//            print("returned call: \(message)")
            
            
        }
    }
}
