//
//  Use this file to import your target's public headers that you would like to expose to Swift.
//

//#import <stdlib.h>
//#import <math.h>
#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioQueue.h>
#import <CoreAudio/CoreAudioTypes.h>
#import <CoreFoundation/CFRunLoop.h>

#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>


//#import <Cordova/CDVViewController.h>
//#import <Cordova/CDV.h>
#import "CDVViewController.h"
#import "CDV.h"
#import "CDVPlugin.h"

#import "StreamingManager.h"
#import "UploadManager.h"

//#import "PCRapidSelectionView.h"
//#import "NativePageTransitions.h"
//#import "APPBackgroundMode.h"

#import <Spotify/Spotify.h>

//#import "YTPlayerView.h"

//#import "CustonUploadAudioPlayerVC.h"
//#import "CustomAudioPlayerVC.h"

//#import <CoreTelephony/CTCallCenter.h>
//#import <CoreTelephony/CTCall.h>
