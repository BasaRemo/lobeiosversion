//
//  AlbumCell.swift
//  Lobe
//
//  Created by Professional on 2015-11-17.
//  Copyright © 2015 Ntambwa. All rights reserved.
//

import UIKit
import Bond
import Haneke
import Foundation
import MediaPlayer
import Haneke

class AlbumCell: UITableViewCell {

    @IBOutlet var albumNameLabel: UILabel!
    @IBOutlet var songsNumberLabel : UILabel!
//    @IBOutlet var songLabel : UILabel!
    @IBOutlet var albumCoverImage: UIImageView!
    @IBOutlet var containerView: UIView!
    
    //Album
    var albumCollection:MPMediaItemCollection? {
        didSet {
            
            guard let albumItem = albumCollection else {
                return
            }
            
            guard let albumTitle = albumItem.representativeItem?.valueForProperty(MPMediaItemPropertyAlbumTitle) as? String else{
                print("Album title unavailable")
                return
            }
            guard let albumTracksNumber = albumItem.representativeItem?.valueForProperty(MPMediaItemPropertyAlbumTrackCount) as? NSNumber else{
                print("Album tracks number unavailable")
                return
            }
            
            self.albumNameLabel?.text = albumTitle
            self.songsNumberLabel?.text = "\(albumTracksNumber.integerValue)"
            
        }
    }
    
    //Artist
    var artistCollection:MPMediaItemCollection? {
        didSet {
            
            guard let artistCollection = albumCollection else {
                return
            }
            
            guard let artistName = artistCollection.representativeItem?.valueForProperty(MPMediaItemPropertyArtist) as? String else{
                print("Album title unavailable")
                return
            }

            let artistTracksNumber = artistCollection.items.count
            self.albumNameLabel?.text = artistName
            self.songsNumberLabel?.text = "\(artistTracksNumber)"
            
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let selectionView = UIView()
        selectionView.backgroundColor = SELECT_FOCUS_COLOR
        self.selectedBackgroundView = selectionView
        
//        self.containerView.layer.cornerRadius = 10.0
//        self.albumCoverImage.layer.cornerRadius = 20.0
        
//        let shadowPath:UIBezierPath  =  UIBezierPath(rect: self.containerView.bounds)
//        self.containerView.layer.masksToBounds = false;
//        self.containerView.layer.shadowColor = UIColor.blackColor().CGColor
//        self.containerView.layer.shadowOffset = CGSizeMake(0.0, 1.0);
//        self.containerView.layer.shadowOpacity = 0.08
//        self.containerView.layer.shadowRadius = 5.0
//        self.containerView.layer.shadowPath = shadowPath.CGPath;
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
