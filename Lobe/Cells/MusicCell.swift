//
//  MusicCell.swift
//  Lobe
//
//  Created by Professional on 2015-11-17.
//  Copyright © 2015 Ntambwa. All rights reserved.
//

import UIKit
import MGSwipeTableCell
 
class MusicCell: MGSwipeTableCell {

    @IBOutlet var cellImgLeadingConstraint: NSLayoutConstraint!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var artistLabel: UILabel!
    @IBOutlet var coverImage: UIImageView!
    @IBOutlet var lobeLIstButton: UIButton!
    
    var songItem = SongItem(){
        didSet{
            
            self.titleLabel?.text = songItem.trackInfo.mName ?? "Unknown"
            self.artistLabel?.text = songItem.trackInfo.mArtist ?? "Unknown"
            
            songItem.trackInfo.songAlbumArt.observe{ image in
                if let albumImage = image {
                let croppedImage = DownloadHelper.cropImage( albumImage, withWidth: Int(CGRectGetWidth(self.coverImage.bounds)) , andHeigth:Int(CGRectGetHeight(self.coverImage.bounds)))
                self.coverImage.image = croppedImage
                }
            }

            setupCell()
            
        }
    }

    //closure
    var onAddToLobeListBtnTapped : ((MusicCell) -> Void)? = nil
    var onPushTrackToLobeList : ((MusicCell) -> Void)? = nil
    
    @IBAction func addSongToLobeList(sender: UIButton) {
        
        if let onButtonTapped = self.onAddToLobeListBtnTapped {
            onButtonTapped(self)
        }
    }
    func setupCell(){
        
        //TODO: - needs to be in trackInfo
        songItem.trackInfo.mUser = LobeStateHelper.currentUser
        songItem.trackInfo.mIsYoutubeVideo = false
        songItem.trackInfo.mUserOwnerName = LobeStateHelper.currentUser.userName.value!
        songItem.trackInfo.mUserOwnerUid = LobeStateHelper.currentUser.parseID
        songItem.trackInfo.mUniqueDeviceId = unique_device_id
        
        self.titleLabel?.text = songItem.trackInfo.mName //rowItem.valueForProperty(MPMediaItemPropertyTitle) as? String
        self.artistLabel?.text = songItem.trackInfo.mArtist //rowItem.valueForProperty(MPMediaItemPropertyArtist) as? String
        
        self.lobeLIstButton.selected = AUDIO_OUT_MANAGER.lobeListContains(songItem)
        
        //configure swipe letf  buttons
        let padding = 5
        
        let button1 =  MGSwipeButton(title: "QUEUE",backgroundColor: UIColor.greenPositiveColor(),padding: padding,
            callback: {
                (sender: MGSwipeTableCell!) -> Bool in
                //Queue Song
                let lockQueue = dispatch_queue_create("lobeList.LockQueue", nil)
                dispatch_sync(lockQueue) {
                    self.handlePushTrack()
                }
                
                return true
        })
        
        let button2 =  MGSwipeButton(title: "PLAY", backgroundColor: UIColor.orangeColor(),padding: padding,
            callback: {
                (sender: MGSwipeTableCell!) -> Bool in
//                print("Insert & Play!")
                return true
        })
        let button3 = MGSwipeButton(title: "NEXT", backgroundColor: UIColor.bluePositiveColor(), callback: {
            (sender: MGSwipeTableCell!) -> Bool in
//            print("Insert and Play next!")
            return true
        })
        
        let font:UIFont = UIFont(name: "HelveticaNeue-Light", size: 11)!
        button1.titleLabel?.font = font
        button2.titleLabel?.font = font
        button3.titleLabel?.font = font
        
//        self.leftButtons = [button1,button2,button3]
//        self.leftSwipeSettings.transition = MGSwipeTransition.Border
//        self.leftExpansion.expansionColor = UIColor.greenPositiveColor()
//        self.leftExpansion.fillOnTrigger = true
//        self.leftExpansion.threshold = 1.0
//        self.leftExpansion.buttonIndex = 0
        
        if self.songItem.trackInfo.mID == AUDIO_OUT_MANAGER.currentSong.trackInfo.mID {
            self.cellImgLeadingConstraint.constant = 42
            self.titleLabel.textColor = UIColor.appThemeColorBackgroundThirdOrange()
        }else{
            self.cellImgLeadingConstraint.constant = 42
//            self.cellImgLeadingConstraint.constant = -8
            self.titleLabel.textColor = UIColor.whiteColor()
        }
        
//        if inSearchMode{
//            cell.cellImgLeadingConstraint.constant = 50
//            cell.titleLabel.textColor = UIColor.whiteColor()
//            
//        }

        let lockQueue = dispatch_queue_create("lobeList.LockQueue", nil)
        dispatch_sync(lockQueue) {
            // code
            self.onAddToLobeListBtnTapped = { [unowned self] (MusicCell) -> Void in
                self.handlePushTrack()
            }
        }
    }
    
    func handlePushTrack(){
        
        if let onPushTrack = self.onPushTrackToLobeList {
            onPushTrack(self)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let selectionView = UIView()
        selectionView.backgroundColor = SELECT_FOCUS_COLOR
        self.selectedBackgroundView = selectionView
        
//        self.backgroundColor = UIColor.clearColor()
        
//        let width = Int(CGRectGetWidth(coverImage.bounds))
//        let heigth = Int(CGRectGetHeight(coverImage.bounds))
//        let croppedImage = DownloadHelper.cropImage( UIImage(named: placeholderImageName)!, withWidth: width , andHeigth:heigth)
//        self.coverImage.image = croppedImage
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
//        self.lobeLIstButton.selected = likeList.contains(PFUser.currentUser()!)
    }
    
    

}
