//
//  SuggestionCell.swift
//  Lobe
//
//  Created by Professional on 2016-01-18.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

import UIKit
import Bond
import Haneke
import Foundation
import MediaPlayer
import Haneke
import MGSwipeTableCell

class SuggestionCell: MGSwipeTableCell {

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var artistLabel: UILabel!
    @IBOutlet var coverImage: UIImageView!
    
    @IBOutlet var acceptButton: UIButton!
    @IBOutlet var rejectButton: UIButton!

    var trackInfo:TrackInfo?
    var songItem:SongItem?{
        didSet{
            
            switch LobeStateHelper.sharedInstance.state {
                case .Local:
                    if let musicItem = songItem!.music {
                        self.titleLabel?.text = musicItem.valueForProperty(MPMediaItemPropertyTitle) as? String
                        self.artistLabel?.text = musicItem.valueForProperty(MPMediaItemPropertyArtist) as? String
                    }
                case .Participant,.DJ:
                    self.titleLabel?.text = songItem?.trackInfo.mName
                    self.artistLabel?.text = songItem?.trackInfo.mArtist
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let selectionView = UIView()
        selectionView.backgroundColor = SELECT_FOCUS_COLOR
        self.selectedBackgroundView = selectionView
         
//        //configure swipe letf  buttons
//        let padding = 5
//        
//        let button1 =  MGSwipeButton(title: "REJECT",backgroundColor: UIColor.redColor(),padding: padding,
//            callback: {
//                (sender: MGSwipeTableCell!) -> Bool in
//                
//                print("Reject")
//                return true
//        })
//        
//        let button2 =  MGSwipeButton(title: "ACCEPT", backgroundColor: UIColor.greenPositiveColor(),padding: padding,
//            callback: {
//                (sender: MGSwipeTableCell!) -> Bool in
//                print("Accept")
//                return true
//        })
//        
//        let font:UIFont = UIFont(name: "HelveticaNeue-Light", size: 11)!
//        button1.titleLabel?.font = font
//        button2.titleLabel?.font = font
//        
//        self.rightButtons = [button1,button2]
//        self.rightSwipeSettings.transition = MGSwipeTransition.Border
//        self.rightExpansion.expansionColor = UIColor.greenPositiveColor()
//        self.rightExpansion.fillOnTrigger = true
//        self.rightExpansion.threshold = 1.0
//        self.rightExpansion.buttonIndex = 1
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
