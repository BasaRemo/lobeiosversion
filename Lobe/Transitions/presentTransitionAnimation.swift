//
//  presentTransitionAnimation.swift
//  Lobe
//
//  Created by Professional on 2016-04-04.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

import UIKit

class presentTransitionAnimation: NSObject, UIViewControllerAnimatedTransitioning {

    var initialY = 0.0
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return 0.2
    }
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        
        let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)!
        let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)!
        
        let finalFrameForVC = transitionContext.finalFrameForViewController(toViewController)
        let containerView = transitionContext.containerView()
        
        var fromVCFrame = transitionContext.initialFrameForViewController(fromViewController)
        
        fromVCFrame = finalFrameForVC
        fromVCFrame.origin.y = CGFloat(Double(finalFrameForVC.size.height) - self.initialY)
        toViewController.view.frame = fromVCFrame
        containerView!.addSubview(toViewController.view)
        
//        let screenShotImage:UIImage = fromViewController.view.takeScreenShot()
        if let currentSongAlbumArt = AUDIO_OUT_MANAGER.currentTrackObservable.value.trackInfo.songAlbumArt.value  {
            toViewController.view.backgroundColor = UIColor(patternImage: currentSongAlbumArt)
        }else{
            toViewController.view.backgroundColor = UIColor(patternImage: UIImage(named: "lobe_background_2")!)
        }
    
        UIView.animateWithDuration(transitionDuration(transitionContext), animations: {
            toViewController.view.frame = finalFrameForVC
            }, completion: {
                finished in
                if transitionContext.transitionWasCancelled(){
                    transitionContext.completeTransition(false)
                }else{
                    transitionContext.completeTransition(true)
                }
                
        })
    }
}
