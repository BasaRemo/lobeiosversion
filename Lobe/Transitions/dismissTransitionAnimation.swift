//
//  dismissTransitionAnimation.swift
//  Lobe
//
//  Created by Professional on 2016-04-04.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

import UIKit

class dismissTransitionAnimation: NSObject, UIViewControllerAnimatedTransitioning {
    
    var initialY = 0.0
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return 0.2
    }
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        
        let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)!
//        let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)!
        
        var frameForFromVC = transitionContext.finalFrameForViewController(fromViewController)
//        let finalFrameForVC = transitionContext.finalFrameForViewController(toViewController)
        
        frameForFromVC.origin.y = CGFloat(Double(frameForFromVC.size.height) - self.initialY)
        
//        let containerView = transitionContext.containerView()
//        containerView!.addSubview(toViewController.view)
//        containerView!.addSubview(fromViewController.view)
        
        
        UIView.animateWithDuration(transitionDuration(transitionContext), animations: {

            fromViewController.view.frame = frameForFromVC
//            toViewController.view.alpha = 1.0
            }, completion: {
                finished in
                if transitionContext.transitionWasCancelled(){
                    transitionContext.completeTransition(false)
                }else{
                    transitionContext.completeTransition(true)
                }
        })
    }
}
