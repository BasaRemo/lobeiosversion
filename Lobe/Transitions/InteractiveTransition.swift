//
//  InteractiveTransition.swift
//  Lobe
//
//  Created by Professional on 2016-04-04.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

import UIKit

class InteractiveTransition: UIPercentDrivenInteractiveTransition {
    
    var viewController:UIViewController?
    var presentViewController:UIViewController?
    var pan:UIPanGestureRecognizer?
    var shouldComplete:Bool = false
    
    func attachToViewController(viewController:UIViewController, viewToAttach view:UIView , presentViewController:UIViewController?){
//        print("attachToViewController..")
        self.viewController = viewController;
        self.presentViewController = presentViewController;
        
        let gestureRecognizer3 = UIPanGestureRecognizer(target: self, action: #selector(InteractiveTransition.onPan(_:)))
        view.addGestureRecognizer(gestureRecognizer3)
    }
}

extension InteractiveTransition {
    
    func onPan(pan:UIPanGestureRecognizer){
        
        let translation:CGPoint =  pan.translationInView(pan.view!.superview)
        
        switch (pan.state) {
            
        case UIGestureRecognizerState.Began:
//            print("Start Panning..")
            guard let _ = self.presentViewController else {
//                print("start to dismiss")
                 self.viewController?.dismissViewControllerAnimated(true, completion: nil)
                break
            }
            self.viewController?.presentViewController(self.presentViewController!, animated: true, completion: nil)
//
        case UIGestureRecognizerState.Changed:
//            print("Panning..")
            let screenHeight:CGFloat = UIScreen.mainScreen().bounds.size.height
            let DragAmount:CGFloat = (self.presentViewController != nil) ? -screenHeight :  screenHeight
            let Threshold:CGFloat = 0.3
            var percent:CGFloat = translation.y / DragAmount;
            
//            print("percent before : \(percent)")
            percent = CGFloat(fmaxf(Float(percent), 0.0))
            percent = CGFloat(fminf(Float(percent), 1.0))
//            print("percent after : \(percent)")
            self.updateInteractiveTransition(percent)
            
            self.shouldComplete = percent > Threshold
            
        case UIGestureRecognizerState.Ended,UIGestureRecognizerState.Cancelled:
//            print("End Panning..")
            if (pan.state == UIGestureRecognizerState.Cancelled || !self.shouldComplete) {
                self.cancelInteractiveTransition()
            } else {
                self.finishInteractiveTransition()
            }
        default:
            break
        }
    }
}