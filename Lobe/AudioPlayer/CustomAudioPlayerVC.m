//
////  CustomAudioPlayerVC.m
////  Lobe
////
////  Created by Professional on 2016-01-03.
////  Copyright © 2016 Ntambwa. All rights reserved.
////
//
//#import "CustomAudioPlayerVC.h"
//
//
//
//
//#define NUM_CHANNELS 2
//#define NUM_BUFFERS 4
//#define BUFFER_SIZE 1024 //2048 //8192 //512 //65536 //32768//4194304
//#define SAMPLE_TYPE short
//#define MAX_NUMBER 32767
//#define SAMPLE_RATE 44100
//#define FORMAT_ID 778924083
//#define DEBUT 1740800
//#define SEEK 889856
//
//unsigned int count;
//
//void callback(void *custom_data, AudioQueueRef queue, AudioQueueBufferRef buffer);
//
////@interface CustomAudioPlayerVC ()
////@property (strong, nonatomic) NSInputStream *inputStreamer;
////@end
//@implementation CustomAudioPlayerVC
//
//@synthesize inputStreamer;
////**** Unused variables *****//
//AVAssetReader *assetReader;
//AVAssetReaderTrackOutput *assetOutput;
//OSStatus result;
//AVAudioPCMBuffer *readBuffer;
//AudioBuffer audioBuffer;
////Float buffer just in case
//static float *mMyAudioBuffer;
//
//
////**** Used variables *****//
//
//// Current position in the buffer where when need to start reading data
//static UInt32 mPlayBufferPosition = 0;
//
//// Positioned to the end of the buffer. For our infinite buffer, this should be the position of the last added byte in the buffer
//static UInt32 mPlayBufferEndPosition;
//
//// Bufferlist that contains the decoded PCM data
//AudioBufferList myConvertedData;
//
////Buffers used by the audioqueue playback
//AudioQueueBufferRef buffers[NUM_BUFFERS];
//
////The audioqueue playback
//AudioQueueRef queue;
//
//
//NSMutableData *pcmData;
//bool firstTime = false;
//NSMutableData *data;
//
//
//NSTimer *TimeOfActiveUser;
//int numbOfPlayingRequesedTrack;
//
////Streaming VARs
//uint8_t buffer[8192];//32768  65536 16384 8192 4096 2048
//UInt32 len;
//
////int numSamples = 327680000;
////UInt8 *outputBuffer;
//NSURL *url;
//CFHTTPMessageRef request;
//
//
//UInt32 numSamples = 0;
////double floatDataArray[882000]; // SPECIFY YOUR DATA LIMIT MINE WAS 882000 , SHOULD BE EQUAL TO OR MORE THAN DATA LIMIT
//
//-(instancetype)init{
//    self = [super init];
//    return self;
//}
//
//-(void)viewDidLoad{
//    [super viewDidLoad];
//    
//    //        [self downloardChuncks:[NSString stringWithFormat:@"ATBtillicome"] numberOfTracks:4];
//    [self downloardChuncks:[NSString stringWithFormat:@"chunk"] numberOfTracks:5];
//    
//    
//}
//
//- (void)downloardChuncks:(NSString*)chunckName  numberOfTracks:(int) numberOfTracks{
//    //    [super viewDidLoad];
//    
//    pcmData = [[NSMutableData alloc] init];
//    if(!data){
//        data = [NSMutableData data];
//    }
//    //    outputBuffer = (UInt8 *)malloc(sizeof(UInt8 *));
//    numbOfPlayingRequesedTrack = 0;
//    //numberOfTracks =5;
//    
//    //http://www.lobemusic.com/files/songshaggy.mp3
//    //http://www.lobemusic.com/files/chunk%i.mp3
//    //http://lobemusic.com/uploads/ATBtillicome2.mp3
//    float time =0;
//    for (int j=0; j< numberOfTracks; j++) {
//        //                NSString * urlString = [NSString stringWithFormat:@"http://lobemusic.com/uploads/%@%i.mp3", @"ATBtillicome", j];
//        NSString * urlString = [NSString stringWithFormat:@"http://www.lobemusic.com/files/%@%i.mp3", chunckName, j];
//        time = j * 5.0;
//        //        NSLog(@"Timer ===> %f", time);
//        //Send to server next request track in 10s
//        TimeOfActiveUser = [NSTimer scheduledTimerWithTimeInterval:time  target:self selector:@selector(nextStreamRequest:) userInfo:urlString repeats:NO];
//    }
//    
//    
//}
////Used by the timer to send next request to the server
//-(void)nextStreamRequest:(NSTimer*)theTimer{
//    //    if (numbOfPlayingRequesedTrack <= 1) {
//    //    [data resetBytesInRange:NSMakeRange(0, [pcmData length])];
//    //    }
//    
//    [self initNetworkCommunication:(NSString*)[theTimer userInfo]];
//}
//
//
//int debut()
//{
//    
//    count = 0;
//    unsigned int i;
//    
//    AudioStreamBasicDescription format;
//    //PCM
//    format.mSampleRate       = SAMPLE_RATE;
//    format.mFormatID         = kAudioFormatLinearPCM;
//    format.mFormatFlags      = kLinearPCMFormatFlagIsSignedInteger | kAudioFormatFlagIsPacked;
//    format.mBitsPerChannel   = 8 * sizeof(SAMPLE_TYPE);
//    format.mChannelsPerFrame = NUM_CHANNELS;
//    format.mBytesPerFrame    = sizeof(SAMPLE_TYPE) * NUM_CHANNELS;
//    format.mFramesPerPacket  = 1;
//    format.mBytesPerPacket   = format.mBytesPerFrame * format.mFramesPerPacket;
//    format.mReserved         = 0;
//    
//    if (numbOfPlayingRequesedTrack == 0) {
//        mPlayBufferPosition = 0; //1048576, 524288
//    }
//    
//    mPlayBufferEndPosition = (int)pcmData.length; //1073741824; // Arbitrary position for now
//    
//    NSLog(@" mPlayBufferEndPosition %u \n ",(unsigned int)mPlayBufferEndPosition);
//    
//    //    NSLog(@" numbOfOccured %u \n ",(unsigned int)numbOfPlayingRequesedTrack);
//    //    NSLog(@"pcmData.length   %lu ", pcmData.length);
//    
//    
//    
//    //Get the decoded pcm buffer
//    const char *decodedPcmByte = [pcmData bytes];
//    
//    //Create an audioqueue output
//    AudioQueueNewOutput(&format, callback, (void *)(decodedPcmByte), NULL, NULL, 0, &queue);
//    
//    // Loop throught the available buffers allocate them and enqueue Data in it so that the audio playback (queue) can start reading it
//    for (i = 0; i < NUM_BUFFERS; i++)
//    {
//        AudioQueueAllocateBuffer(queue, BUFFER_SIZE, &buffers[i]);
//        buffers[i]->mAudioDataByteSize = BUFFER_SIZE;
//        AudioQueueEnqueueBuffer(queue, buffers[i], 0, NULL);
//        //        NSLog(@" buffers[0]->mPacketDescriptionCount %u", (unsigned int)buffers[0]->mPacketDescriptionCount);
//    }
//    OSStatus errorMsg = AudioQueueSetParameter(queue, kAudioQueueParam_Volume, 1.0);
//    
//    if (errorMsg) {
//        NSLog(@"AudioQueueSetParameter returned %d when setting the volume.", errorMsg);
//    }
//    numbOfPlayingRequesedTrack++;
//    // Start the audio queue playback (queue)
//    // - NULL indicates that the player should start consuming data in the queue buffers as soon as possible
//    AudioQueueStart(queue, NULL);
//    //    CFRunLoopRun();
//    
//    return 0;
//}
//
//// Sets the float buffer -- Unused for now --
//void SetAudioBuffer(float *inAudioBuffer) {
//    mMyAudioBuffer = inAudioBuffer;
//}
//
//// Callback called by the audio queue plaback (queue) when he has an empty buffer
//void callback(void * inUserData, AudioQueueRef inAQ,AudioQueueBufferRef  inCompleteAQBuffer)
//{
//    mPlayBufferEndPosition = (int)pcmData.length;
//    char *byteData = [pcmData bytes];
//    void *mUserData = (void *)byteData;
//    
//    if (mPlayBufferPosition <= mPlayBufferEndPosition) {
//        
//        UInt32 mBufferByteSize = inCompleteAQBuffer->mAudioDataByteSize;
//        UInt32 numRemainingBytes = mPlayBufferEndPosition - mPlayBufferPosition;
//        UInt32 numBytesToCopy =  numRemainingBytes < mBufferByteSize ? numRemainingBytes : mBufferByteSize;
//        
//        
//        // Do system copy of a chunk of data and enqueue it in the empty buffer so that the audioqueue playback (queue) can read it
//        inCompleteAQBuffer->mAudioDataByteSize = numBytesToCopy;
//        //        tmp += inCompleteAQBuffer->mAudioDataByteSize;
//        //        NSLog(@" %u  ==> ", tmp);
//        memcpy(inCompleteAQBuffer->mAudioData, (const void *) (mUserData  + (mPlayBufferPosition / sizeof(Byte))) , numBytesToCopy); //Copy data with offset
//        //        memcpy(inCompleteAQBuffer->mAudioData, (const void *) (inUserData  + (mPlayBufferEndPosition / sizeof(Byte))) , numBytesToCopy); //Copy data
////        AudioQueueEnqueueBuffer(queue, inCompleteAQBuffer, 0, NULL);
//         AudioTimeStamp timeStamp;
//        AudioQueueEnqueueBufferWithParameters(queue,
//                                              inCompleteAQBuffer,
//                                              0,
//                                              NULL,
//                                              0, // inTrimFramesAtStart: The number of priming frames to skip at the start of the buffer. We have 256 frames/buffer
//                                              0, // inTrimFramesAtEnd : The number of frames to skip at the end of the buffer.
//                                              0,
//                                              NULL,
//                                              NULL, //inStartTime : The desired start time for playing the buffer
//                                              &timeStamp); //outActualStartTime : the time when the buffer will actually start playing.
//        
//        
//        NSLog(@"timeInterval == %f", timeStamp.mSampleTime / SAMPLE_RATE);
//        
////        int timeInterval = 0;
////        AudioQueueTimelineRef timeLine;
////        AudioTimeStamp timeStamp;
////        OSStatus status = AudioQueueCreateTimeline(queue, &timeLine);
////        if(status == noErr) {
////            
////            AudioQueueGetCurrentTime(queue, timeLine, &timeStamp, NULL);
////            timeInterval = timeStamp.mSampleTime / SAMPLE_RATE;
////        }
////        
////        //        timeStamp.mHostTime = 700000;
////        NSLog(@"%llu ============== > %d", timeStamp.mHostTime, timeInterval);
//        
//        
//        
//        //        if (mPlayBufferPosition == DEBUT) {
//        //            mPlayBufferPosition = SEEK;
//        //        }else{
//        mPlayBufferPosition += numBytesToCopy;
//        //        }
//        
//        //        NSLog(@" mPlayBufferPosition %u \n ",(unsigned int)mPlayBufferPosition);
//    }
//    //    else{
//    //        AudioQueueStop(queue, NULL);
//    //    }
//    
//}
//-(int) GetCurrentTime {
//    int timeInterval = 0;
//    AudioQueueTimelineRef timeLine;
//    OSStatus status = AudioQueueCreateTimeline(queue, &timeLine);
//    if(status == noErr) {
//        AudioTimeStamp timeStamp;
//        AudioQueueGetCurrentTime(queue, timeLine, &timeStamp, NULL);
//        timeInterval = timeStamp.mSampleTime / SAMPLE_RATE; // modified
//    }
//    return timeInterval;
//}
//
//
////Decode an MP3 file in a PCM Bufferlist (myConvertedData)
//- (NSData*) decodeMp3ToPCMFromAudioFile:(NSData *)data  {
//    @autoreleasepool {
//        
//        
//        //    pcmData = [[NSMutableData alloc] init];
//        
//        AudioFileID refAudioFileID;
//        ExtAudioFileRef inputFileID;
//        
//        //    void* clientData,
//        //    SInt64 position,
//        //    UInt32 requestCount,
//        //    void* buffer,
//        //    UInt32* actualCount
//        
//        OSStatus result = AudioFileOpenWithCallbacks((__bridge void * _Nonnull)(data), readProc, 0, getSizeProc, 0, kAudioFileMP3Type, &refAudioFileID);
//        if (result != noErr)  {
//            NSLog(@"problem in theAudioFileReaderWithData function: result code %i \n", result);
//            //        return pcmData;
//        }
//        
//        result = ExtAudioFileWrapAudioFileID(refAudioFileID, false, &inputFileID);
//        if (result != noErr) {
//            NSLog(@"problem in theAudioFileReaderWithData function Wraping the audio FileID: result code %i \n", result);
//            //        return pcmData;
//        }
//        
//        // Client Audio Format Description
//        AudioStreamBasicDescription clientFormat;
//        memset(&clientFormat, 0, sizeof(AudioStreamBasicDescription));
//        
//        //PCM
//        clientFormat.mSampleRate       = SAMPLE_RATE;
//        clientFormat.mFormatID         = kAudioFormatLinearPCM;
//        clientFormat.mFormatFlags      = kLinearPCMFormatFlagIsSignedInteger | kAudioFormatFlagIsPacked;
//        clientFormat.mBitsPerChannel   = 8 * sizeof(SAMPLE_TYPE);
//        clientFormat.mChannelsPerFrame = NUM_CHANNELS;
//        clientFormat.mBytesPerFrame    = sizeof(SAMPLE_TYPE) * NUM_CHANNELS;
//        clientFormat.mFramesPerPacket  = 1;
//        clientFormat.mBytesPerPacket   = clientFormat.mBytesPerFrame * clientFormat.mFramesPerPacket;
//        clientFormat.mReserved         = 0;
//        
//        int size = sizeof(clientFormat);
//        result = 0;
//        result = ExtAudioFileSetProperty(inputFileID, kExtAudioFileProperty_ClientDataFormat, size, &clientFormat);
//        if(result != noErr) {
//            NSLog(@"error on ExtAudioFileSetProperty for input File with result code %i \n", result);
//        }
//        
//        
//        //How many samples to read in at a time Default 1024 (change this value to read a larger chunk of data, you will hear the sound a little longer)
//        numSamples = [data length]*32;//> mPlayBufferEndPosition ? [data length] : mPlayBufferEndPosition; //[pcmData length];//32768;
//        NSLog(@"numSamples ==> %i", numSamples);
//        UInt32 sizePerPacket = clientFormat.mBytesPerPacket; // = sizeof(Float32) = 32bytes
//        UInt32 packetsPerBuffer = numSamples;
//        UInt32 outputBufferSize = packetsPerBuffer;// * sizePerPacket ;
//        
//        // So the lvalue of outputBuffer is the memory location where we have reserved space
//        UInt8 *outputBuffer = (UInt8 *)malloc(sizeof(UInt8 *) * outputBufferSize);
//        
//        
//        AudioBufferList convertedData ;//= *(AudioBufferList *)malloc(sizeof(AudioBufferList) + sizeof(AudioBuffer) * (1));//;= malloc(sizeof(convertedData));
//        
//        
//        convertedData.mNumberBuffers = 1;    // Set this to 1 for mono
//        convertedData.mBuffers[0].mNumberChannels = clientFormat.mChannelsPerFrame;  //also = 1
//        convertedData.mBuffers[0].mDataByteSize = outputBufferSize;
//        convertedData.mBuffers[0].mData = outputBuffer; //
//        //    myConvertedData = convertedData;
//        //
//        UInt32 frameCount = numSamples; // todo .. change this logic.... we need to read all anyways ...
//        //    while (frameCount > 0) {
//        
//        result = ExtAudioFileRead(inputFileID,&frameCount,&convertedData);
//        
//        if (result != noErr) {
//            NSLog(@"Error on ExtAudioFileRead with result code %i \n", result);
//            //            break;
//        }
//        //        NSLog(@" Decoding: Before pcm Appending (frameCount = %i )", convertedData.mBuffers[0].mDataByteSize );
//        //        [pcmData appendData:tempData];
//        // if the number of frame that was read is > 0
//        
//        if (frameCount > 0)  {
//            //        NSLog(@" Decoding: Before pcm Appending (frameCount = %u )", frameCount );
//            //        int convertedBytesCount = frameCount * clientFormat.mBytesPerFrame;
//            //        NSLog(@" Decoding: frameCount is > 0 (convertedBytesCount = %u ) (mDataByteSize = %u )", convertedBytesCount, convertedData.mBuffers[0].mDataByteSize);
//            //        for (int k=0; k < convertedData.mNumberBuffers; k++) {
//            //        [pcmData appendData:[NSData dataWithBytes:convertedData.mBuffers[0].mData length:convertedBytesCount]];
//            //        }
//            if(!firstTime ){
//                firstTime = true;
//            }
//            [pcmData initWithBytes:convertedData.mBuffers[0].mData length:convertedData.mBuffers[0].mDataByteSize];
//            
//            //        NSLog(@"Error on pcmData %@ \n", [pcmData initWithBytes:convertedData.mBuffers[0].mData length:convertedData.mBuffers[0].mDataByteSize]);
//            
//        }
//        
//        
//        //Clean up
//        free(outputBuffer);
//        ExtAudioFileDispose(inputFileID);
//        AudioFileClose(refAudioFileID);
//        
//        return pcmData;
//    }
//}
//
//#pragma mark
//#pragma mark - Callback Function
//
//static OSStatus readProc(void* clientData,
//                         SInt64 position,
//                         UInt32 requestCount,
//                         void* buffer,
//                         UInt32* actualCount)
//{
//    
//    NSData *inAudioData = (__bridge NSData *) clientData;
//    
//    size_t dataSize = inAudioData.length;
//    size_t bytesToRead = 0;
//    //    NSLog(@"position %lld === dataSize %zu ", position, dataSize);
//    if(position <= dataSize) {
//        size_t bytesAvailable = dataSize - position;
//        bytesToRead = requestCount <= bytesAvailable ? requestCount : bytesAvailable;
//        
//        [inAudioData getBytes: buffer range:NSMakeRange(position, bytesToRead)];
//    } else {
//        NSLog(@"data was not read \n");
//        bytesToRead = 0;
//    }
//    
//    if(actualCount) {
//        *actualCount = bytesToRead;
//    }
//    
//    return noErr;
//}
//
//static SInt64 getSizeProc(void* clientData) {
//    NSData *inAudioData = (__bridge NSData *) clientData;
//    size_t dataSize = inAudioData.length;
//    return dataSize;
//}
//
//// Init the InputStream
//- (void)initNetworkCommunication:(NSString *)strUrl {
//    
//    //Set the request
//    @autoreleasepool {
//        CFReadStreamRef readStream;
//        url = [NSURL URLWithString:strUrl];
//        
//        request = CFHTTPMessageCreateRequest(NULL, (CFStringRef)@"GET", (__bridge CFURLRef)(url), kCFHTTPVersion1_1);
//        
//        if (!request) NSLog(@"Error setting request");
//        
//        //Set the readStream
//        readStream = CFReadStreamCreateForHTTPRequest(kCFAllocatorDefault, request);
//        if (!readStream) NSLog(@"Error in readStream");
//        /* add persistent property */
//        CFReadStreamSetProperty(readStream, kCFStreamPropertyHTTPAttemptPersistentConnection, kCFBooleanTrue);
//        /* make the read stream unique */
//        CFReadStreamSetProperty(readStream, CFSTR("UniqueProperty"), @"UniqueProperty");
//        
//        
//        //Assign the read stream to our inputStream
//        inputStreamer = (__bridge_transfer NSInputStream *)readStream;
//        CFReadStreamClose(readStream);
//        
//        //Set delegate and schedule and start loop
//        [inputStreamer setDelegate:self];
//        [inputStreamer scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
//        [inputStreamer open];
//        
//        //        CFRelease(request);
//        //        CFRelease(readStream);
//    }
// 
//}
//
//
////Delegate that manage the inputstream events
//- (void)stream:(NSStream *)theStream handleEvent:(NSStreamEvent)streamEvent {
//    //    NSMutableData * tempData = [[NSMutableData alloc] init];
//    int count= 0;
//    switch (streamEvent) {
//            
//        case NSStreamEventOpenCompleted:
//            NSLog(@"Stream opened");
//            break;
//            
//        case NSStreamEventHasSpaceAvailable:
//        {
//            NSLog(@"Request");
//            break;
//        }
//        case NSStreamEventHasBytesAvailable:
//            //            NSLog(@"Reading ");
//            
//            if (theStream == inputStreamer) {
//                
//                len = (UInt32)[inputStreamer read:buffer maxLength:sizeof(buffer)];
//                if (len > 0) {
//                    //                        NSLog(@"%u", (unsigned int)len);
//                    //                    [tempData appendBytes:(void *)buffer length:len];
//                    //
//                    //                    NSData * _data = [NSData dataWithBytesNoCopy:buffer length:len freeWhenDone:NO];
//                    
//                    [data appendBytes:(const void *)buffer length:len];
//                    
//                    //                    NSLog(@"%lu", (unsigned long)data.length);
//                    
//                }
//                //                [self decodeMp3ToPCMFromAudioFile:data];
//                //                debut();
//            }
//            
//            break;
//            
//        case NSStreamEventErrorOccurred:
//            NSLog(@"Can not connect to the host!");
//            break;
//            
//        case NSStreamEventEndEncountered:
//        {
//            NSLog(@"Stream has end encountered");
//            if (!firstTime) {
//                [self decodeMp3ToPCMFromAudioFile:data];
//                debut();
//                //                data = [[NSMutableData alloc] init];
//                //                data = [NSMutableData data];
//            }else{
//                [self decodeMp3ToPCMFromAudioFile:data];
//                //                data = [NSMutableData data];
//            }
//            [theStream close];
//            [theStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
//            [inputStreamer close];
//            theStream = nil;
//            break;
//        }
//        default:
//            NSLog(@"Unknown event");
//    }
//}
//
//
//@end
