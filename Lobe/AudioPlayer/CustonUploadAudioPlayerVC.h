//
//  CustonUploadAudioPlayerVC.h
//  Lobe
//
//  Created by Nabil Tahri on 16-01-17.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

//#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import "MHWDirectoryWatcher/MHWDirectoryWatcher.h"
//NSObject   UIViewController

@interface CustonUploadAudioPlayerVC : NSObject
@property (nonatomic, strong) MPMediaQuery * everything;
//@property (nonatomic, strong) MPMediaItem *song;
@property (nonatomic, strong) MHWDirectoryWatcher *_dirWatcher;

@property (strong, nonatomic) AVAssetReader *assetReader;
@property (strong, nonatomic) AVAssetReaderTrackOutput *assetOutput;

@property (strong, nonatomic) NSMutableArray *chunksInfoList;
@property (strong, nonatomic) NSMutableArray *chunksDataList;

//@property (nonatomic) int spliteNb;

//mEstimatedLengthInSeconds = end;
//chunkInfo.mFirstFrameTimePositionMillis

-(instancetype)init;
-(void) startUploading:(MPMediaItem*)song nbreOfSplite:(int) nbreOfSplite;
-(void) startUploadingWithURL:(UInt32)chunckIndex url:(NSString*)urlName correlationID:(NSString*)correlationID chunkStartTimeServer:(UInt64) chunkStartTimeServer;
-(int)createAllLocalFileChunks:(NSString*)persistentID nbSecondsPerChunk:(UInt32)nbSecondsPerChunk;
-(NSData*) chunkDataAtIndex:(int) chunkIndex;
@end
