/**
 * Created by johnny on 2016-01-21.
 */
function googleApiClientReady(){
    gapi.client.setApiKey('AIzaSyCGF_bcFqgzWQT6GVk1NHu88eYC0Xrk96w');
    gapi.client.load('youtube', 'v3', function() {
        //searchByKeyWords("dogs pink floyd playing")
    });
}



function onYouTubePlayerAPIReady() {
    VideoPlayerHelper.mYoutubePlayer = new YT.Player('ytplayer', {
         playerVars: { 'autoplay': 1, 'controls': 0,
         autohide: 1, 'playsinline': 1},
         events: {
         'onStateChange': VideoPlayerHelper.onPlayerStateChange,
         'onReady': VideoPlayerHelper.onPlayerReady,
         'onError': VideoPlayerHelper.onPlayerError
         }
         });
    
}

var VideoPlayerHelper = {

    INTERVAL_UPDATE_STEP_MS: 2000,
    mYoutubePlayer: null,
    mSyncCheckerPeriodicTimer: null,
    mIsYoutubePlayerReady: false,
    mSyncedTrackInfo: null,
    mDephasageCustom: 0,

    clearTimer: function () {
        if (VideoPlayerHelper.mSyncCheckerPeriodicTimer) // remove old instance of interval timer
        {
            clearInterval(VideoPlayerHelper.mSyncCheckerPeriodicTimer);
            VideoPlayerHelper.mSyncCheckerPeriodicTimer = null;
        }
    },
    setUpSyncTimer: function () {
        VideoPlayerHelper.clearTimer();

        if (!isMeInGroupSession())
        {
            return;
        }
        // TODO: check if youtube video
        // check if youtube.. continue

        VideoPlayerHelper.mSyncCheckerPeriodicTimer = setInterval(function () {
            if (!isUserSignedIn())
            {
                console.warn("in mGroupAliveWatchDog callback, called but not a host");
                clearInterval(VideoPlayerHelper.mSyncCheckerPeriodicTimer); // stop
                return;
            }
            VideoPlayerHelper.triggerSyncIter();
        }, VideoPlayerHelper.INTERVAL_UPDATE_STEP_MS);
    },
    triggerSyncIter: function () {
        //console.log("SYNC VIDEO HERE")
        if (!isMeInGroupSession() || (!VideoPlayerHelper.mSyncCheckerPeriodicTimer)) {
            VideoPlayerHelper.clearTimer();
            VideoPlayerHelper.ensureVideoIsStopped();
        } else if (mIsPlayingMediaContentAllowed) {
            var musicStartTimeInUnixTime = VideoPlayerHelper.mSyncedTrackInfo.music_server_start_time - mTimeDiff + VideoPlayerHelper.mSyncedTrackInfo.music_start_time_delay;
            var theoraticalTimePos = getUnixTime() - musicStartTimeInUnixTime;
            var theoraticalTimePosSec = theoraticalTimePos / 1000.0;
            var practicalTimePos = VideoPlayerHelper.mYoutubePlayer.getCurrentTime();
            if (Math.abs(theoraticalTimePosSec - practicalTimePos) > 0.2) { // DO the median
                // VideoPlayerHelper.mYoutubePlayer.seekTo((theoraticalTimePos/1000.0), true);
            }
            console.log("practicalTimePos " + practicalTimePos + " theoraticalTimePosSec " + theoraticalTimePosSec  +" Math.abs(theoraticalTimePosSec - practicalTimePos) " + Math.abs(theoraticalTimePosSec - practicalTimePos) )

            //VideoPlayerHelper.doSync();
        }
        // getCurrentTime
    },
    manualSyncRequestClicked: function() {
        if (!isMeInGroupSession()) {
            VideoPlayerHelper.clearTimer();
            VideoPlayerHelper.ensureVideoIsStopped();
        } else if (mIsPlayingMediaContentAllowed){
            VideoPlayerHelper.doSync();
            //setTimeout(function(){
            //    VideoPlayerHelper.doSync();
            //    setTimeout(function(){
            //        VideoPlayerHelper.doSync();
            //    }, 300);
            //}, 300);
        }
    },
    doSync: function () {
        var musicStartTimeInUnixTime = VideoPlayerHelper.mSyncedTrackInfo.music_server_start_time - mTimeDiff + VideoPlayerHelper.mSyncedTrackInfo.music_start_time_delay + VideoPlayerHelper.mDephasageCustom;
        var theoraticalTimePos = getUnixTime() - musicStartTimeInUnixTime;

        if (!isMeInGroupSession()) {
            return;
        } else if (mIsPlayingMediaContentAllowed){
            VideoPlayerHelper.mYoutubePlayer.seekTo((theoraticalTimePos/1000.0), true);
        }
    },
    initYoutubePlayer: function ()
    {
        // Load the IFrame Player API code asynchronously.
        var tag = document.createElement('script');
        tag.src = "https://www.youtube.com/player_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

        // Replace the 'ytplayer' element with an <iframe> and
        // YouTube player after the API code downloads.

    },
    onPlayerReady: function (evt) {
        console.log("Youtube player is ready.. onPlayerReady has been called")
        if (evt && evt.target)
        {
            VideoPlayerHelper.mIsYoutubePlayerReady = true;
            VideoPlayerHelper.mYoutubePlayer.loadVideoById({
                                         'videoId': 'a8dqVuHmj5k',
                                         'startSeconds': 0,
                                         //'endSeconds': 60,
                                         'suggestedQuality': 'small'});
//            VideoPlayerHelper.ensureVideoIsPlaying();
        }

    },
    onPlayerError: function (evt) {
        console.error("onPlayerError called attempt to init youtube player 0 " + evt.data);
        //  alert("error")
    },
    newMediaRequestReceived: function (track_info) {
        if (mOperatingSystem === "other") {
            if (track_info.is_youtube_video) {
                VideoPlayerHelper.mSyncedTrackInfo = track_info;
                VideoPlayerHelper.playYoutubeVideo(track_info);
            } else {
                VideoPlayerHelper.ensureVideoIsStopped();
                VideoPlayerHelper.clearTimer();
            }
        }
    },
    playYoutubeVideo: function (track_info)
    {
        console.log("playYoutubeVideo called");
        if (VideoPlayerHelper.mIsYoutubePlayerReady === false)
        {
            console.error("playYoutubeVideo called, but VideoPlayerHelper.mIsYoutubePlayerReady is false");
            return;
        }
        try {
            if (VideoPlayerHelper.mYoutubePlayer) {
                VideoPlayerHelper.mYoutubePlayer.loadVideoById({
                    'videoId': track_info.track_chunk_url,
                    height: '390',
                    width: '640',
                    'startSeconds': 0,
                    //'endSeconds': 60,
                    'suggestedQuality': 'small'});
                VideoPlayerHelper.setUpSyncTimer();
            } else {
                console.error("playYoutubeVideo video called, but VideoPlayerHelper.mYoutubePlayer undefined");
            }
        } catch(e) {
            console.error("playYoutubeVideo function call failed with error " + e);
        }
    },
    ensureVideoIsPaused: function ()
    {
        console.log("ensureVideoIsPaused called");
        if (VideoPlayerHelper.mIsYoutubePlayerReady === false)
        {
            console.error("ensureVideoIsPaused called, but VideoPlayerHelper.mIsYoutubePlayerReady is false");
            return;
        }
        try {
            if (VideoPlayerHelper.mYoutubePlayer)
                VideoPlayerHelper.mYoutubePlayer.pauseVideo();
            else
                console.error("ensureVideoIsPaused video called but mYoutubePlayer undefined");
        } catch(e) {
            console.error("ensureVideoIsPaused function call failed with error " + e);
        }
    },
    ensureVideoIsPlaying: function ()
    {
        console.log("ensureVideoIsPlaying called");
        if (VideoPlayerHelper.mIsYoutubePlayerReady === false)
        {
            console.error("ensureVideoIsPlaying called, but VideoPlayerHelper.mIsYoutubePlayerReady is false");
            return;
        }
        try {
            if (VideoPlayerHelper.mYoutubePlayer) {
                VideoPlayerHelper.mYoutubePlayer.playVideo();
            }
            else
                console.error("ensureVideoIsPlaying video called but mYoutubePlayer undefined");
        } catch(e) {
            console.error("ensureVideoIsPlaying function call failed with error " + e);
        }
    },
    ensureVideoIsStopped: function ()
    {
        if (VideoPlayerHelper.mIsYoutubePlayerReady === false)
        {
            console.error("ensureVideoIsStopped called, but VideoPlayerHelper.mIsYoutubePlayerReady is false");
            return;
        }
        try {
            if (VideoPlayerHelper.mYoutubePlayer) {
                VideoPlayerHelper.mYoutubePlayer.stopVideo();
            }
            else
                console.error("ensureVideoIsStopped video called but mYoutubePlayer undefined");
        } catch(e) {
            console.error("ensureVideoIsStopped function call failed with error " + e);
        }
    },
    onPlayerStateChange: function (evt) {
        switch (evt.data) {
            case -1: // unstarted
                // TODO: react here, trigger next maybe
                break;
            case 0: // ended
                console.log("Youtube api onPlayerStateChange called with state 0:  playing");
               
                break;
            case 1: // playing
                console.log("Youtube api onPlayerStateChange called with state 1:  playing. Calling refreshMediaContentPlaybackState to ensure correct state");
                // this the duration of the video set it in local and
                console.log("Youtube player in play state: quality is " + VideoPlayerHelper.mYoutubePlayer.getPlaybackQuality());


//                mCurrentLoadedTrack.song_length = VideoPlayerHelper.mYoutubePlayer.getDuration() * 1000;
//                setUpTriggerNextSongTimer();
//                refreshMediaContentPlaybackState();
                break;
            case 2: // paused
                console.log("Youtube api onPlayerStateChange called with state 2:  paused. Calling refreshMediaContentPlaybackState to ensure correct state");
//                refreshMediaContentPlaybackState();
                break;
            case 3: // buffering
                break;
            case 5: // video cued
                break;

        }
    }
};

VideoPlayerHelper.initYoutubePlayer();

// documentation https://developers.google.com/youtube/js_api_reference?hl=en
