//
//  AppDelegate.swift
//  Lobe
//
//  Created by Professional on 2015-11-16.
//  Copyright © 2015 Ntambwa. All rights reserved.
//

import UIKit
import Parse
import Bolts
import SwiftyJSON
import AVFoundation
import FBSDKCoreKit
import FBSDKLoginKit
import Fabric
import Crashlytics
import MediaPlayer
import Haneke
import CoreTelephony
//import STLocationRequest
import SystemConfiguration

let THEME_COLOR:UIColor = UIColor(red: 29/255 , green: 83/255, blue: 126/255, alpha: 1.0)
let LABEL_COLOR:UIColor = UIColor.whiteColor()

let DJ_COLOR:UIColor = UIColor(red: 5/255 , green: 38/255, blue: 127/255, alpha: 0.25)
let PARTICIPANT_COLOR:UIColor = UIColor(red: 86/255 , green: 0/255, blue: 0/255, alpha: 0.25)
let CURRENT_PLAYING_COLOR:UIColor = UIColor.orangeColor()
let SELECT_FOCUS_COLOR:UIColor = UIColor(red: 255/255 , green: 255/255, blue: 255/255, alpha: 0.1)

let SEGMENT_MENU_COLOR:UIColor = UIColor(red: 0/255 , green: 0/255, blue: 0/255, alpha: 0.5)
let SEGMENT_RED_MENU_COLOR:UIColor = UIColor(red: 150/255 , green: 20/255, blue: 010/255, alpha: 0.5)
let unique_device_id = UIDevice.currentDevice().identifierForVendor!.UUIDString
let placeholderImageName = "lobe_background_3"
var startViewController: MainVC = MainVC()
let callCenter = CTCallCenter()
func backgroundThread(delay: Double = 0.0, background: (() -> Void)? = nil, completion: (() -> Void)? = nil) {
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0)) {
        if(background != nil){ background!(); }
        
        let popTime = dispatch_time(DISPATCH_TIME_NOW, Int64(delay * Double(NSEC_PER_SEC)))
        dispatch_after(popTime, dispatch_get_main_queue()) {
            if(completion != nil){ completion!(); }
        }
    }
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var backgroundUpdateTask: UIBackgroundTaskIdentifier = 0
    
    let kClientId = "20beb54968d44f1fbd9031b41d62160b"
    let kCallbackURL = "lobe://spotify/"

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
//        application.statusBarOrientation = .Portrait;
        application.setMinimumBackgroundFetchInterval(60) //UIApplicationBackgroundFetchIntervalMinimum
        self.handleCall()
        Fabric.sharedSDK().debug = true
        Fabric.with([Crashlytics.self])

        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
//        application.statusBarHidden = true
//        var url  = NSURL(string: "fb://")
//        if UIApplication.sharedApplication().canOpenURL(url!) {
//            UIApplication.sharedApplication().openURL(url!)
//        }
        SpotifyHelper.sharedInstance.setupAuthClient()
        
        UIApplication.sharedApplication().statusBarStyle = .LightContent
        LocationHelper.sharedInstance.manager.requestWhenInUseAuthorization()
        
//        SideMenuController.menuButtonImage = UIImage(named: "menu_icon")
//        SideMenuController.presentationStyle = .UnderCenterPanelLeft
//        SideMenuController.animationStyle = .FadeAnimation
        
//         LocationHelper.sharedInstance.manager.requestWhenInUseAuthorization()
//         LocationHelper.sharedInstance.manager.requestAlwaysAuthorization()
//        UINavigationBar.appearance().translucent = false
//        UINavigationBar.appearance().barTintColor = UIColor.clearColor()
        
        
        // Initialize Parse.
//        Parse.setApplicationId("DlIbcE0Xiy5upPCDpZXneHfAD76lDBiGvwcT51ee",clientKey: "5BV9uurEqcEelh6cHEkR4TOrh8wB8IHfzCxPkHkc")
        let configuration = ParseClientConfiguration {
            $0.applicationId = AppConfiguration.ParseApplicationID
            $0.clientKey = AppConfiguration.ClientKey
            $0.server = AppConfiguration.server_url
        }
        Parse.initializeWithConfiguration(configuration)
        PFUser.enableRevocableSessionInBackground()
        
//        let testObject = PFObject(className: "TestObject")
//        testObject["test"] = "docwhere"
//        testObject.saveInBackgroundWithBlock { (success: Bool, error: NSError?) -> Void in
//            if error == nil {
//                print("Object has been saved.")
//            }else{
//                print("error saving to parse: \(error?.description)")
//            }
//            
//        }
//        
//        PFCloud.callFunctionInBackground("helloLobeTrack",
//                                         withParameters: [:])
//        { (object:AnyObject?, error:NSError?) -> Void in
//            
//            guard error == nil else {
//                print("error calling cloud code")
//                return
//            }
//            let message = object as! String
//            print("returned call: \(message)")
//            
//        }
//        
//        PFCloud.callFunctionInBackground("helloregisterActivity",
//                                         withParameters: ["user_id":PFUser.currentUser()?.objectId])
//        { (object:AnyObject?, error:NSError?) -> Void in
//            
//            guard error == nil else {
//                print("error calling cloud code: \(error?.description)")
//                return
//            }
////            let message = object as! PFUser
////            print("returned call: \(message["username"])")
//            let message = object as! String
//            print("returned call: \(message)")
//            
//        }
        
        // [Optional] Track statistics around application opens.
        PFAnalytics.trackAppOpenedWithLaunchOptions(launchOptions)
        LobeStateHelper.sharedInstance.notifyPushNotifReceived()
        let settings = UIUserNotificationSettings(forTypes: [.Alert, .Sound, .Badge], categories: nil)
        UIApplication.sharedApplication().registerUserNotificationSettings(settings)
        UIApplication.sharedApplication().registerForRemoteNotifications()
        
        if let launchOptions = launchOptions as? [String : AnyObject] {
            if let notificationDictionary = launchOptions[UIApplicationLaunchOptionsRemoteNotificationKey] as? [NSObject : AnyObject] {
                self.application(application, didReceiveRemoteNotification: notificationDictionary)
            }
        }
        
        //Set background audio capabilities
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            
            do{
                let session = AVAudioSession.sharedInstance()
                try session.setCategory(AVAudioSessionCategoryPlayback)
                try session.setActive(true)

            }
            catch{
                 print("\(error)")
            }
        }
        
        UIApplication.sharedApplication().beginReceivingRemoteControlEvents()
        self.becomeFirstResponder()
        
        // Nav Bar
        UINavigationBar.appearance().translucent = true
        UINavigationBar.appearance().opaque = true
        UINavigationBar.appearance().barTintColor = UIColor.whiteColor()
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()
        
        // Status Bar
        UIApplication.sharedApplication().statusBarHidden = false
        UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.LightContent
        if #available(iOS 9.0, *) {
            UIBarButtonItem.appearanceWhenContainedInInstancesOfClasses([UINavigationBar.classForCoder()]).tintColor = UIColor.whiteColor()
        } else {
            // Fallback on earlier versions
        }
        
        if #available(iOS 9.0, *) {
            NSNotificationCenter.defaultCenter().addObserver(
                self,
                selector: #selector(self.powerStateChanged),
                name: NSProcessInfoPowerStateDidChangeNotification,
                object: nil
            )
        } else {
            // Fallback on earlier versions
        }
        
//        LocationHelper.sharedInstance.manager.requestWhenInUseAuthorization()
        //        // check if we have logged in user
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        //        let startViewController: UIViewController
        startViewController = storyboard.instantiateViewControllerWithIdentifier("MainVC") as! MainVC
        self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
        self.window?.rootViewController = startViewController;
        self.window?.makeKeyAndVisible()
        
        return true
    }

    
    func clearBadges() {
        
        let installation = PFInstallation.currentInstallation()
        installation.badge = 0
        installation.saveInBackgroundWithBlock { (success, error) -> Void in
            if success {
//                print("cleared badges")
                UIApplication.sharedApplication().applicationIconBadgeNumber = 0
            }
            else {
//                print("failed to clear badges")
            }
        }
    }
    
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        
        let installation = PFInstallation.currentInstallation()
        installation.channels = ["global"]
        installation.setDeviceTokenFromData(deviceToken)
        installation.saveInBackground()
        
//        PFPush.subscribeToChannelInBackground("") { (succeeded, error) in
//            if succeeded {
//                print("ParseStarterProject successfully subscribed to push notifications on the broadcast channel.");
//            } else {
//                print("ParseStarterProject failed to subscribe to push notifications on the broadcast channel with error = %@.", error)
//            }
//        }
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        
        PFPush.handlePush(userInfo)
        let json = JSON(userInfo)
//        print("json Object: \(json)")
        LobeStateHelper.sharedInstance.pushNotif = json
        
        dispatch_async(dispatch_get_main_queue()) {
            LobeStateHelper.sharedInstance.notifyPushNotifReceived()
        }
    }
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
        self.backgroundUpdateTask = UIApplication.sharedApplication().beginBackgroundTaskWithExpirationHandler({
            self.endBackgroundUpdateTask()
        })
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        AUDIO_OUT_MANAGER.inBackgroundMode.value = true
         LocationHelper.sharedInstance.manager.stopUpdatingLocation()
//        AUDIO_OUT_MANAGER.pauseMusic()
//        AUDIO_OUT_MANAGER.playMusic()
        
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
//        LocationHelper.sharedInstance.manager.startUpdatingLocation()
        self.endBackgroundUpdateTask()
        AUDIO_OUT_MANAGER.mainVC.updateMusicPlayingUI()
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        FBSDKAppEvents.activateApp()
        clearBadges()
        AUDIO_OUT_MANAGER.inBackgroundMode.value = false
//        LocationHelper.sharedInstance.manager.stopUpdatingLocation()
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
        print("url: \(url)")
        switch url.host! {
        case "joingroup" :
            guard let url_path = url.path else {return false }
            let group_id = url_path.stringByReplacingOccurrencesOfString("/", withString: "")
            //            print("join group: \(group_id)")
            NativeToWebHelper.sharedInstance.joinGroup(group_id)
            
            return true
            
        case "spotify" :
            
            if (SPTAuth.defaultInstance().canHandleURL(url)) {
                
                SPTAuth.defaultInstance().handleAuthCallbackWithTriggeredAuthURL(url, callback: { (error, session) -> Void in
                    if (error != nil) {
                        print("*** Auth error: \(error)")
                        return
                    }
                    SpotifyHelper.sharedInstance.successLoginCallBack(sessionObject:session)
                    
                })
                
                return true
            }
            
            return true
        default:
            break
        }
//        if url.host == "joingroup" {
//            
//            guard let url_path = url.path else {return false }
//            let group_id = url_path.stringByReplacingOccurrencesOfString("/", withString: "")
////            print("join group: \(group_id)")
//            NativeToWebHelper.sharedInstance.joinGroup(group_id)
//            
//            return true
//            
//        }
        return FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    func endBackgroundUpdateTask() {
        UIApplication.sharedApplication().endBackgroundTask(self.backgroundUpdateTask)
        self.backgroundUpdateTask = UIBackgroundTaskInvalid
    }
    
    func application(application: UIApplication, performFetchWithCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        // do background work
        LobeStateHelper.sharedInstance.updateUserActivity()
        completionHandler(.NewData)
    }
    

    func application(application: UIApplication, supportedInterfaceOrientationsForWindow window: UIWindow?) -> UIInterfaceOrientationMask {
        
        return UIInterfaceOrientationMask.AllButUpsideDown
        
//        let deviceIdiom = UIDevice.currentDevice().userInterfaceIdiom
//        // 2. check the idiom
//        switch (deviceIdiom) {
//            
//            case .Phone:
//                print("iPhone and iPod touch style UI")
//                return UIInterfaceOrientationMask.AllButUpsideDown
//            default:
//                print("Unspecified UI idiom")
//                return UIInterfaceOrientationMask.Portrait
//
//        }
        
    }
    
    func powerStateChanged(){
        
        if #available(iOS 9.0, *) {
            if NSProcessInfo.processInfo().lowPowerModeEnabled {
                // Low Power Mode is enabled. Start reducing activity to conserve energy.
                print("power state changed: lowPowerModeEnabled")
            } else {
                // Low Power Mode is not enabled.
                print("power state changed: lowPowerMode Disabled")
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    func handleCall(){
        
        callCenter.callEventHandler = {(call: CTCall) -> () in
            
            switch call.callState {
                case CTCallStateDialing:
                    print("CTCallStateDialing")
                    break
                case CTCallStateIncoming:
                    print("CTCallStateIncoming")
                    AUDIO_OUT_MANAGER.pauseMusic()
                    AUDIO_OUT_MANAGER.mainVC.updateMusicPlayingUI()
                    
                    break
                case CTCallStateConnected:
                    print("CTCallStateConnected")
                    AUDIO_OUT_MANAGER.pauseMusic()
                    AUDIO_OUT_MANAGER.mainVC.updateMusicPlayingUI()
                    break
                case CTCallStateDisconnected:
                    print("CTCallStateDisconnected")
//                    AUDIO_OUT_MANAGER.playMusic()
                    break
                default:
                    break
            }
            
        }
    }

}

extension AppDelegate {
    
    func loginWithSpotify(){
        
        let auth = SPTAuth.defaultInstance()
        
        auth.clientID = kClientId
        auth.redirectURL = NSURL(string:kCallbackURL)
//        auth.tokenRefreshURL = NSURL(string:kTokenRefreshServiceURL)
//        auth.sessionUserDefaultsKey = kSessionUserDefaultsKey
        auth.requestedScopes = [SPTAuthStreamingScope, SPTAuthPlaylistReadPrivateScope]
        
        /*
         STEP 1: Get a login URL from SPAuth and open it in Safari.
         */
        
        delay(0.1) {
            UIApplication.sharedApplication().openURL(auth.loginURL)
            return
        }
    }
    
    func delay(delay:Double, closure:()->()) {
        dispatch_after(
            dispatch_time(
                DISPATCH_TIME_NOW,
                Int64(delay * Double(NSEC_PER_SEC))
            ),
            dispatch_get_main_queue(), closure)
    }
}

// Network Connection Class
public class Reachability {
    
    class func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(sizeofValue(zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(&zeroAddress) {
            SCNetworkReachabilityCreateWithAddress(nil, UnsafePointer($0))
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
}


