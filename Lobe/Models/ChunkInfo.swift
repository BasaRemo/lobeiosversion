//
//  ChunkInfo.swift
//  Lobe
//
//  Created by Professional on 2016-01-26.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import Bond
import MediaPlayer

class ChunkInfo: NSObject {

    // track
    var mTempStoragePath:String = "-1"// the full path (in cache) where the chuck (mp3) is saved
    var mNbOfBytes:Int = -1  // the exact nb of bytes in this chunk
    var mEstimatedLengthInSeconds:Int  = -1 // length of the chunk in seconds
    var mFirstFrameTimePositionMillis:Int  = -1 // At time
    
    override init() {
    }
    
    init(tempStoragePath: String, nbOfBytes: Int, estimatedLengthInSeconds: Int, firstFrameTimePositionMillis: Int) {
        
        self.mTempStoragePath = tempStoragePath
        self.mNbOfBytes = nbOfBytes
        self.mEstimatedLengthInSeconds = estimatedLengthInSeconds
        self.mFirstFrameTimePositionMillis = firstFrameTimePositionMillis

    }
    
    func toDictionary() -> [String: AnyObject] {
        return chunkToDictionary(self)
    }
    
    func chunkToDictionary(trackInfo: ChunkInfo) -> [String: AnyObject] {
        
        let chunkDict =  [
            
            "mTempStoragePath": mTempStoragePath ?? "",//String.replaceRegex(trackInfo.mArtist!),
            "mNbOfBytes": mNbOfBytes,  //
            "mEstimatedLengthInSeconds": mEstimatedLengthInSeconds,
            "mFirstFrameTimePositionMillis": mFirstFrameTimePositionMillis
            
            
        ]
        //"item_node": trackInfo.mAlbumID!,
        //        print("trackDictJSON: \(JSON(trackDict))")
        //        print("trackDict: \(trackDict)")
        
        return chunkDict as! [String : AnyObject]
    }
    
    init(chunkInfoJson: JSON) {
        
        //Track
        self.mTempStoragePath = chunkInfoJson["mTempStoragePath"].string ?? "-1"
        self.mNbOfBytes = chunkInfoJson["mNbOfBytes"].int ?? -1
        self.mEstimatedLengthInSeconds = chunkInfoJson["mEstimatedLengthInSeconds"].int ?? -1
        self.mFirstFrameTimePositionMillis = chunkInfoJson["mFirstFrameTimePositionMillis"].int ?? -1
        
    }
}
