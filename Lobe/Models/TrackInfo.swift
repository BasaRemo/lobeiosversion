//
//  TrackInfo.swift
//  Lobe
//
//  Created by Professional on 2016-01-05.
//  Copyright © 2016 Ntambwa. All rights reserved.
//
import Foundation
import UIKit
import SwiftyJSON
import Bond
import MediaPlayer
//import RealmSwift

class TrackInfo: NSObject {

//==========================================================================================
//MARK: -  track origin device
//==========================================================================================
    var mUniqueDeviceId:String = ""
    
//==========================================================================================
//MARK: -  Audio Format
//==========================================================================================
    var mAudioNbChannels:Int = 2
    var mAudioSampleRate:Int = 44100
    var mAudioBitRate:Int = 16
//==========================================================================================
//MARK: -  track info
//==========================================================================================
    var mID:String = "" {
        
        didSet{
            trackID.value = mID
        }
    }
    var mArtist:String = ""{
        didSet{
            trackArtistName.value = mArtist
        }
    }
    var mName:String = "" {
        didSet{
            trackName.value = mName
        }
    }
    
    var mDisplayName:String?
    var mAlbumID:String?
    var mLength:Int  = -1
    var mDataPath:String = "" {
        didSet{
            if mDataPath != "" {
//                downloadImage(mDataPath)
            }
        }
    }
    var  mIsYoutubeVideo:Bool = false; // ADDED
    var mVideoThumbnailImageUrl:String = "" {
        
        didSet{
//            downloadImage(mVideoThumbnailImageUrl)
            if mVideoThumbnailImageUrl != "" {
                downloadImage(mVideoThumbnailImageUrl)
            }
        }
    }
    
//==========================================================================================
//MARK: -  track owner info
//==========================================================================================
    
    var mSelfOwner:Bool  = false
    var mUserOwnerUid:String  = ""
    var mUserOwnerName:String  = ""
    var mUserOwnerPictureUrl:String  = "-1"
    
    var mUser:User?{
        didSet{
            if let user = mUser {
                
                mSelfOwner  = true
                mUserOwnerUid  = user.parseID
                mUserOwnerName  = user.name
                mUserOwnerPictureUrl  = user.picUrl
            }
        }
    }
    
//==========================================================================================
//MARK: -  track version control
//==========================================================================================

    var mUnixTimeStampAtUserPush:String = "-1"
    
//==========================================================================================
//MARK: - track control
//==========================================================================================
    
    var mCurrChunkUrl:String = "-1" //For Stream
    var mCurrChunkLocalPath:String = "-1" // this is used for the uploader
    var mSong_full_local_data_path:String = "-1"
    
//==========================================================================================
// MARK: - Observable variables
//==========================================================================================
    var trackID: Observable<String> = Observable<String>("")
    var trackName: Observable<String> = Observable<String>("")
    var trackArtistName: Observable<String> = Observable<String>("")
    var songAlbumArt : Observable<UIImage?> = Observable<UIImage?>(nil)
    
    var albulArtImage:UIImage = UIImage(named: placeholderImageName)!
    func toDictionary() -> [String: AnyObject] {
        return trackToDictionary(self)
    }
    
//==========================================================================================
//MARK: - Social voting system variables
//==========================================================================================

    var mPromotingUserIdsJsonArray:[String] = ["-1"]
    var mDislikingUserIdsJsonArray:[String] = ["-1"]
    
//==========================================================================================
//MARK: - New variables with spotify Integration
//==========================================================================================
    
    var track_type:String = "-1"
    var thumbnail_img_url:String = "-1"
    var album_id:String = "-1"
    var artist_id:String = "-1"
    var preview_url:String = "-1"
    var track_url:String = "-1"
    
  
//==========================================================================================
//MARK: - Track Json Encoding
//==========================================================================================
    
    func trackToDictionary(trackInfo: TrackInfo) -> [String: AnyObject] {
        
        let trackDict =  [
            
            "song_uid":trackInfo.mID,
            "song_artist": trackInfo.mArtist ?? "",//String.replaceRegex(trackInfo.mArtist!),
            "song_name": trackInfo.mName,  //
            "song_length": trackInfo.mLength,
            "song_full_local_data_path": trackInfo.mDataPath ?? "-1",
            
            "user_uid": trackInfo.mUserOwnerUid ?? "",
            "user_name": trackInfo.mUserOwnerName ?? "",
            "user_image_url": trackInfo.mUserOwnerPictureUrl,
            
            "push_unix_time":  trackInfo.mUnixTimeStampAtUserPush,
            "track_chunk_url": trackInfo.mCurrChunkUrl ?? "",
            "track_chunk_local_path": trackInfo.mCurrChunkLocalPath ?? "",
            
            "is_youtube_video": trackInfo.mIsYoutubeVideo ?? false,
            "youtube_thumbnail_img_url":trackInfo.mVideoThumbnailImageUrl ?? "-1",
            
            "unique_device_id":trackInfo.mUniqueDeviceId ?? "",
            
            "audio_sample_rate":trackInfo.mAudioSampleRate ?? 2,
            "audio_nb_channels":trackInfo.mAudioNbChannels ?? 44100,
            "audio_bit_rate":trackInfo.mAudioBitRate ?? 16,
            
            "track_type":trackInfo.track_type ?? "",
            "thumbnail_img_url":trackInfo.thumbnail_img_url ?? "",
            "album_id":trackInfo.album_id ?? "",
            "artist_id":trackInfo.artist_id ?? "",
            "preview_url":trackInfo.preview_url ?? "",
            "track_url":trackInfo.track_url ?? ""
        
        ]
//        print("trackInfo.mIsYoutubeVideo : \(trackInfo.mIsYoutubeVideo)")
        
//         trackDict["is_youtube_video"] = trackInfo.mIsYoutubeVideo
//         trackDict["youtube_thumbnail_img_url"] = trackInfo.mVideoThumbnailImageUrl
        
        //"item_node": trackInfo.mAlbumID!,
//        print("trackDictJSON: \(JSON(trackDict))")
//        print("trackDict: \(trackDict)")
        
        return trackDict as! [String : AnyObject]
    }
    
//==========================================================================================
//MARK: - Track Json Encoding & Decoding Helpers
//==========================================================================================
    func getJsonFromArray(arrayToTreat: [String]) -> JSON {
        let jsonArray = JSON(arrayToTreat)
//        let jsonString = jsonArray.rawString()
//        let cleanJsonString = replaceBadChar(jsonArray)
//        print("clean promote or like list JSON: \(jsonArray)")
        return jsonArray
    }

    func replaceBadChar(str:String) -> String  {
        return str.replace("\'", withString: "\\\'").replace("\"", withString: "\\\"").replace("\n", withString: "\\n").replace("\r", withString: "")
        
        //        return str.replace(/\n/g, "\\\\n").replace(/\/g, "\\\\r").replace(/ t/g, "\\\\t");
    }
    
    
    //==========================================================================================
    //MARK: - Constructor from JSON (Decoding)
    //==========================================================================================
    init(trackInfoJson: JSON) {
        super.init()
        //Track
        self.mID = trackInfoJson["song_uid"].string ?? ""
        self.mArtist = trackInfoJson["song_artist"].string ?? ""
        self.mName = trackInfoJson["song_name"].string ?? ""
        self.mDisplayName = trackInfoJson["song_name"].string ?? ""
        self.mAlbumID = trackInfoJson["item_node"].string ?? ""
        
        self.mDataPath = trackInfoJson["song_full_local_data_path"].string ?? ""
        self.mLength = trackInfoJson["song_length"].int ?? -1
        
        //User
        self.mUserOwnerName = trackInfoJson["user_name"].string ?? ""
        self.mUserOwnerUid = trackInfoJson["user_uid"].string ?? ""
        self.mUserOwnerPictureUrl = trackInfoJson["user_image_url"].string ?? ""
        
        self.mUser = User(mName: mUserOwnerName, mParseId: mUserOwnerUid, mPicUrl: mUserOwnerPictureUrl)
        
        //Time Stamp
        self.mUnixTimeStampAtUserPush = trackInfoJson["push_unix_time"].string ?? ""
        self.mCurrChunkUrl = trackInfoJson["track_chunk_url"].string ?? "-1"
        self.mCurrChunkLocalPath = trackInfoJson["track_chunk_local_path"].string ?? "-1"
        
//        print("is_youtube_video : \(trackInfoJson["is_youtube_video"])")
//        print("mVideoThumbnailImageUrl : \(trackInfoJson["youtube_thumbnail_img_url"])")
        
        self.mIsYoutubeVideo = trackInfoJson["is_youtube_video"].bool ?? false
        self.mVideoThumbnailImageUrl = trackInfoJson["youtube_thumbnail_img_url"].string ?? ""
        
        self.mUniqueDeviceId = trackInfoJson["unique_device_id"].string ?? ""
        
        
        self.mAudioSampleRate = trackInfoJson["audio_sample_rate"].int ?? 0
        self.mAudioNbChannels = trackInfoJson["audio_nb_channels"].int ?? 0
        self.mAudioBitRate = trackInfoJson["audio_bit_rate"].int ?? 0
        
        //New variables integrating spotify
        self.track_type = trackInfoJson["track_type"].string ?? ""
        self.thumbnail_img_url = trackInfoJson["thumbnail_img_url"].string ?? ""
        self.album_id = trackInfoJson["album_id"].string ?? ""
        self.artist_id = trackInfoJson["artist_id"].string ?? ""
        self.preview_url = trackInfoJson["preview_url"].string ?? ""
        self.track_url = trackInfoJson["track_url"].string ?? ""
        
        self.downloadImage(self.thumbnail_img_url)
        
    }
    
//==========================================================================================
//MARK: - Constructor from mpMediaItem
//==========================================================================================
    convenience init(music:MPMediaItem){
        
        let trackID = music.valueForProperty(MPMediaItemPropertyPersistentID) as? NSNumber
        let trackAlbumID = music.valueForProperty(MPMediaItemPropertyAlbumPersistentID) as? NSNumber
        let trackLength = music.valueForProperty(MPMediaItemPropertyPlaybackDuration) as? NSNumber
        let itemArtwork = music.valueForProperty(MPMediaItemPropertyArtwork) as? MPMediaItemArtwork
        
        //Formating
        let tID = String(format:"%f", trackID!.intValue)
        let tAlbumID = String(format:"%f", trackAlbumID!.doubleValue)
        let tLength = Int((trackLength?.doubleValue)!*1000)
        
        let tArtist = music.valueForProperty(MPMediaItemPropertyArtist) as? String
        let tName = music.valueForProperty(MPMediaItemPropertyTitle) as? String
        let tDisplayName = music.valueForProperty(MPMediaItemPropertyTitle) as? String
        
        // Call the Init Function
        self.init(trackID: tID ?? "" , artist: tArtist ?? "" , name: tName ?? "", dataPath: "", displayName: tDisplayName ?? "", length: tLength ?? 0 , albumID: tAlbumID ?? "",user:LobeStateHelper.currentUser)
        
        //Add Album art
        if let _ = itemArtwork {
            let artWorkImage = itemArtwork!.imageWithSize(CGSize(width: 100.0, height: 100.0))
            self.songAlbumArt.value = artWorkImage ?? UIImage(named:placeholderImageName)
            self.albulArtImage = artWorkImage ?? UIImage(named:placeholderImageName)!
        }else{
            self.songAlbumArt.value = UIImage(named: placeholderImageName)
            self.albulArtImage = UIImage(named:placeholderImageName)!
        }
        
        // Add local path
        if let localPath = music.valueForProperty(MPMediaItemPropertyAssetURL) as? NSURL{
        
//            print("Loca Path \(localPath)")
//            print("music.assetURL \(music.assetURL!)")
            self.mCurrChunkLocalPath = "\(localPath)"
            
            let localPath = "\(localPath)"
            let localPathArr = localPath.componentsSeparatedByString("id=")
            let persistantID = localPathArr[1] 
            
            self.mID = persistantID
            
        }else{
            print("No local Path!!")
        }
    }
    
    //==========================================================================================
    //MARK: - Constructor
    //==========================================================================================
    
    override init() {
    }
    
    init(trackID: String, artist: String, name: String, dataPath: String, displayName: String, length: Int, albumID: String, user:User) {
        
        self.mID = trackID
        self.mArtist = artist
        self.mName = name
        self.mDataPath = dataPath
        self.mDisplayName = displayName
        self.mLength = length
        self.mAlbumID = albumID
        
        self.mUser = user
        self.mUserOwnerUid = user.parseID
        self.mUserOwnerName = user.name
    }
    
    
//==========================================================================================
//MARK: - Track helper functions
//==========================================================================================
    func genCorrolationID() -> String
    {
        return mID + mUnixTimeStampAtUserPush
    }
    
    func getmVideoThumbnailImageUrl() -> String{
        return "-1"
    }
    
    // MARK: - Downloading function
    func downloadImage(imageUrl:String){
        DownloadHelper.downloadImage(imageUrl) { (image:UIImage?, error: NSError?) -> Void in
            if error == nil {
                
                self.albulArtImage = image!
                self.songAlbumArt.value = image!

            }
        }
    }
    
//==========================================================================================
//MARK: - Social voting system functions
//==========================================================================================
    func setPromotingList(jsonArrayString:JSON){
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            var promotingList:[String] = [String]()
            //        print("func setPromotingList jsonArrayString: \(jsonArrayString)")
            for (index,subJson):(String, JSON) in jsonArrayString {
                //            print("subJson promotion: \(subJson)")
                promotingList.append(subJson.string!)
            }
            
            self.mPromotingUserIdsJsonArray.removeAll()
            self.mPromotingUserIdsJsonArray.appendContentsOf(promotingList)
        }
    }
    
    func setLikeList(jsonArrayString:JSON){
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            
            var promotingList:[String] = [String]()
            //        print("func setPromotingList jsonArrayString: \(jsonArrayString)")
            for (index,subJson):(String, JSON) in jsonArrayString {
                //            print("subJson promotion: \(subJson)")
                promotingList.append(subJson.string!)
            }
            
            self.mDislikingUserIdsJsonArray.removeAll()
            self.self.mDislikingUserIdsJsonArray.appendContentsOf(promotingList)
            
        }
    }
    
    func getNbPromotingVotes() -> Int
    {
        return mPromotingUserIdsJsonArray.count - 1 //-1 because firebase dont access empty arrays
    }
    
    func getNbDislikingVotes() -> Int
    {
        return mDislikingUserIdsJsonArray.count - 1 //-1 because firebase dont access empty arrays
    }
    
    func isMePromotingTrack() -> Bool
    {
        return  mPromotingUserIdsJsonArray.filter({ $0 == mUserOwnerUid }).count > 0
    }
    
    func isMeDislikingTrack() -> Bool
    {
        return  mDislikingUserIdsJsonArray.filter({ $0 == mUserOwnerUid }).count > 0
    }
    
}

extension String {
    
    static func random(length: Int = 20) -> String {
        
        let base = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        var randomString: String = ""
        
        for _ in 0..<length {
            let randomValue = arc4random_uniform(UInt32(base.characters.count))
            randomString += "\(base[base.startIndex.advancedBy(Int(randomValue))])"
        }
        
        return randomString
    }
}

