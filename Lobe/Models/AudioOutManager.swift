//
//  AudioOutManager.swift
//  Lobe
//
//  Created by Professional on 2015-12-12.
//  Copyright © 2015 Ntambwa. All rights reserved.
//

import Foundation
import MediaPlayer
import SwiftyJSON
import Alamofire
import WebKit
//import Chronos
//import Dollar
import Bond
import AVFoundation
import Parse

enum MusicPlayerState :String {
    case Library = "Library"
    case LobeList = "LobeList"
    
    private init () {
        self = .Library
    }
}

let AUDIO_OUT_MANAGER = AudioOutManager()

class AudioOutManager:NSObject {
    
//==========================================================================================
// MARK: - Variables
//==========================================================================================
    let player = MPMusicPlayerController.applicationMusicPlayer()
    var audioPlayer = AVAudioPlayer()
    let commandCenter = MPRemoteCommandCenter.sharedCommandCenter()
    
    var lobeAppState:LobeAppState = LocalState.getSharedInstance()
    
    var initialLoading = true
    var musicListsHaveChanged = false
    var savedCurrentLocalSong = SongItem()
    var currentPlayingSongTimer = NSTimer()
    var currentLocalSongIndex = 0
    var currentLobeListSongIndex = 0
    let socialTrackLength = 100
    var mDeltaTimeFirebaseIOS:Int64 = 0
    var mainVC:MainVC!

    var inBackgroundMode: Observable<Bool> = Observable<Bool>(false)
    
    //Observable
    var lobeListObservableList: ObservableArray<SongItem> = ObservableArray([SongItem()])
    var localObservableTrackList: ObservableArray<SongItem> = ObservableArray([])
    
    var localPlaylistArray: ObservableArray<SongItem> = ObservableArray([])
    var localAlbumArray: ObservableArray<SongItem> = ObservableArray([])
    var localArtistArray: ObservableArray<SongItem> = ObservableArray([])
    
    var socialVotingMapListChangeObserver: Observable<Bool> = Observable<Bool>(false)
    var isAudioPlaying: Observable<Bool> = Observable<Bool>(false)
    var currentTrackObservable: Observable<SongItem> = Observable<SongItem>(SongItem())
    
    var shouldPlayYoutubeVideo: Observable<Bool> = Observable<Bool>(false)
    var youtube_video_id:Observable<String> = Observable<String>("")
    
    var isPlaying = false {
        didSet{
            if !isGroupMusicPlaying && !isPlaying{
                isAudioPlaying.value = false
            }else{
                isAudioPlaying.value = true
            }
        }
    }
    
    //TODO: - Social playback variables
    var isGroupMusicPlaying = false {
        didSet{
            if !isGroupMusicPlaying && !isPlaying{
                isAudioPlaying.value = false
            }else{
                isAudioPlaying.value = true
            }
        }
    }

    var socialVotingMapList:JSON?{
        didSet{
            socialVotingMapListChangeObserver.value = !socialVotingMapListChangeObserver.value
//            AUDIO_OUT_MANAGER.lobeListObservableList.replaceRange(0...0, with: [SongItem()])
        }
    }

    var musicPlayerState = MusicPlayerState() {
        didSet{
//            print("player Owner moved from \(oldValue) to \(musicPlayerState)")
        }
    }
    
    var currentSong = SongItem() {
        didSet {
            //TODO: - In social Mode (DJ, Participant )this change trigger the view update with the new currentSong before we receive data from JS
            currentTrackObservable.value = currentSong
            LobeStateHelper.sharedInstance.updateYoutubeViewState()
        }
    }
    
    var localListSongItem:[SongItem] = [SongItem](){
        didSet {
        }
    }
    var lobeListSongItems:NSMutableArray = NSMutableArray(){
        didSet {
            musicListsHaveChanged = true
        }
    }
    
    func lobeStateChanged(){
        switch LobeStateHelper.sharedInstance.state {
        case .Local:
            self.stopSocialAudio()
//            if AUDIO_OUT_MANAGER.localObservableTrackList.count > 0{
//                let songItem =  AUDIO_OUT_MANAGER.localObservableTrackList[0]
//                self.currentSong = songItem
//            }else{
//                self.currentSong = SongItem()
//            }
            self.currentSong = SongItem()
            
        case .DJ,.Participant:
            self.player.pause()
            self.isPlaying = false
            break
        }
    }

    
    func shouldSyncAudio() -> Bool{
        
        guard AUDIO_OUT_MANAGER.isMusicPlaying() == true else {return false }
        guard LobeStateHelper.sharedInstance.state != .Local else {return false }
        
        return true
    }
    
//==========================================================================================
// MARK: - Init
//==========================================================================================
    override init() {
        super.init()
        
        SongItemListHelper.loadLocalSongs()
        FileManagerHelper.createUploadDirectory()
        player.beginGeneratingPlaybackNotifications()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.playBackStateDidChange), name: MPMusicPlayerControllerPlaybackStateDidChangeNotification, object: player)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.nowPlayingMusicChanged), name: MPMusicPlayerControllerNowPlayingItemDidChangeNotification, object: player)
        
        //Enable background and lock Screen commands
//        let commandCenter = MPRemoteCommandCenter.sharedCommandCenter()
        commandCenter.nextTrackCommand.enabled = true
        commandCenter.previousTrackCommand.enabled = true
        commandCenter.playCommand.enabled = true
        commandCenter.pauseCommand.enabled = true
        commandCenter.togglePlayPauseCommand.enabled = true
        
        commandCenter.nextTrackCommand.addTarget(self, action: #selector(self.nextTrack))
        commandCenter.previousTrackCommand.addTarget(self, action: #selector(self.previousTrack))
        commandCenter.playCommand.addTarget(self, action: #selector(self.playTrack))
        commandCenter.pauseCommand.addTarget(self, action: #selector(self.pauseTrack))
        commandCenter.togglePlayPauseCommand.addTarget(self, action: #selector(self.playPauseToggled))
        
        if #available(iOS 9.0, *) {
            NSNotificationCenter.defaultCenter().addObserver(
                self,
                selector: #selector(self.powerStateChanged),
                name: NSProcessInfoPowerStateDidChangeNotification,
                object: nil
            )
        } else {
            // Fallback on earlier versions
        }

    }
    
    func powerStateChanged(){

        if #available(iOS 9.0, *) {
            if NSProcessInfo.processInfo().lowPowerModeEnabled {
                // Low Power Mode is enabled. Start reducing activity to conserve energy.
                print("power state changed: lowPowerModeEnabled")
            } else {
                // Low Power Mode is not enabled.
                print("power state changed: lowPowerMode Disabled")
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    func nowPlayingMusicChanged(){
        
        guard let currentMediaItem = player.nowPlayingItem else {
//            print("couldnt set current song with nowPlayingItem item ")
            return
        }
        currentSong = SongItem(mMediaItem: currentMediaItem)
    }
    
    func playBackStateDidChange(){
        
    }
}

//==========================================================================================
//MARK: - Bacground Music Player Playback
//==========================================================================================
extension AudioOutManager:AVAudioPlayerDelegate{
    
    func playPauseToggled(){
//        print("playPauseToggled")
        if self.isMusicPlaying() {
            self.pauseMusic()
        }else{
            self.playMusic()
        }
    }
    func nextTrack(){
//        print("nextTrack")
//        if self.inBackgroundMode && !self.isNextLobeListTrackValid() {
//            print("In background mode & next track youtube .. can't trigger next")
//            return
//        }
        self.skipToNextMusic()
    }
    
    func previousTrack(){
//        print("previousTrack")
//        if self.inBackgroundMode && !self.isNextLobeListTrackValid() {
//            print("In background mode & next track youtube .. can't trigger previous")
//            return
//        }
        self.skipToPreviousMusic()
    }
    
    func playTrack(){
        print("inBackgroundMode playTrack")
        if self.isMusicPlaying() {
            self.pauseMusic()
        }else{
            self.playMusic()
        }
    }
    
    func pauseTrack(){
        print("inBackgroundMode pauseTrack")
        if self.isMusicPlaying() {
            self.pauseMusic()
        }else{
            self.playMusic()
        }
    }
}

//==========================================================================================
//MARK: - Music Player Playback
//==========================================================================================

extension AudioOutManager{
    
    func playMusic(){
        lobeAppState.playTrack()
    }
    func directPlayMusic(){
        lobeAppState.directPlayTrack()
    }
    func pauseMusic(){
        lobeAppState.pauseTrack()
    }
    func skipToNextMusic(){
        lobeAppState.skipToNextMusic()
    }
    func skipToPreviousMusic(){
        lobeAppState.skipToPreviousMusic()
    }
    func skipToBeginning(){
        lobeAppState.skipToBeginning()
    }
    func isMusicPlaying() -> Bool{
        return lobeAppState.isTrackPlaying()
    }
    func stopSocialAudio(){
        lobeAppState.stopSocialAudio()
    }
    func stopStreamingAudio(){
        lobeAppState.stopStreamingAudio()
    }
    func resumeStreamingAudio(){
        lobeAppState.resumeStreamingAudio()
    }
    
    func loadYoutubeVideo(trackInfo:TrackInfo){
        NativeToWebHelper.sharedInstance.youtube_delegate?.nativeCallLoadVideo(trackInfo)
    }
}

//==========================================================================================
//MARK: - Songitem list management
//==========================================================================================
extension AudioOutManager {
    
    //Local list
    func localListContains(songItem:SongItem) -> Bool{
        return SongItemListHelper.localListContains(songItem)
    }
    func findLocalSong(songItem:SongItem) -> [SongItem]{
        return SongItemListHelper.findLocalSong(songItem)
    }
    
    func getIndexOfLocalSongItem(songItem:SongItem) -> Int{
        return SongItemListHelper.getIndexOfLocalSongItem(songItem)
    }
    func getIndexOfCurrentPlayingLocalTrack() -> Int {
        return SongItemListHelper.getIndexOfCurrentPlayingLocalTrack()
    }
    
    //Lobelist
    func lobeListContains(songItem:SongItem) -> Bool{
        return SongItemListHelper.lobeListContains(songItem)
    }
    func getIndexOfLobeListSongItem(songItem:SongItem) -> Int{
        return SongItemListHelper.getIndexOfLobeListSongItem(songItem)
    }
    func findSongInLobeList(songItem:SongItem) -> [SongItem] {
        return SongItemListHelper.findSongInLobeList(songItem)
    }
    func getIndexOfCurrentPlayingLobeListTrack() -> Int {
        return SongItemListHelper.getIndexOfCurrentPlayingLobeListTrack()
    }
    func removeSongInLobelist(songItem:SongItem){
        SongItemListHelper.removeSongInLobeList(songItem)
    }
    
    func isNextLobeListTrackValid() -> Bool{
        return SongItemListHelper.isNextLobeListTrackValid()
    }

}
//==========================================================================================
// MARK: - Functions
//==========================================================================================
extension AudioOutManager{
    
    func userDidLeaveGroup(){
        
        self.stopSocialAudio()
//        if AUDIO_OUT_MANAGER.localObservableTrackList.count > 0{
//            let songItem =  AUDIO_OUT_MANAGER.localObservableTrackList[0]
//            self.currentSong = songItem
//            AUDIO_OUT_MANAGER.musicPlayerState = .Library
//        }else{
//            self.currentSong = SongItem()
//        }
        self.currentSong = SongItem()

    }
    func saveLocalMusicState(){
        
        savedCurrentLocalSong = currentSong
    }
    func retrieveLocalMusicState(){
        
        currentSong = savedCurrentLocalSong
    }
    
    func addMainViewInstance(mainview:MainVC){
        self.mainVC = mainview
    }
    
    func showMenu(){
        dispatch_async(dispatch_get_main_queue()) {
            self.mainVC.menuButtonPressed(self)
        }
    }
    
    func showLibraryView(){
        dispatch_async(dispatch_get_main_queue()) {
            self.mainVC.menuViewLocalLibraryOnly()
        }
    }
}