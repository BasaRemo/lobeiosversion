//
//  PushNotif.swift
//  Lobe
//
//  Created by Professional on 2016-02-13.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

import UIKit
import SwiftyJSON

class PushNotif: NSObject {

    var mParsePushId:String = ""
    var mMessage:String = ""
    var mSenderID:String = ""
    var mReceiverID:String = ""
    
    func toJSON() -> JSON{
        return toJSON(self)
    }
    
    init(parsePushId:String, message:String, senderID:String, receiverID:String){
        mParsePushId = parsePushId
        mMessage = message
        mSenderID = senderID
        mReceiverID  = receiverID
    }
    
    init(pushNotifJson: JSON) {
        
        mParsePushId = pushNotifJson["parsePushId"].string!
        mMessage = pushNotifJson["aps"]["alert"].string!
        mSenderID = "test"
        mReceiverID = "test"
    }
    
    func toJSON(pushNotif: PushNotif) -> JSON{
        
        let pushDict = [
            "mParsePushId": mParsePushId,
            "mMessage": mMessage,
            "mSenderID": mSenderID,
            "mReceiverID": mReceiverID
        ]
        return JSON(pushDict)
    }
    
}
