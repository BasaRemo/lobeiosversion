//
//  MainVC.swift
//  Lobe
//
//  Created by Professional on 2015-11-17.
//  Copyright © 2015 Ntambwa. All rights reserved.
//

import UIKit
//import VYPlayIndicator
import ZFDragableModalTransition
import AVFoundation
import Foundation
import MediaPlayer
import SwiftyJSON
import Alamofire
import SABlurImageView
//import ARNTransitionAnimator
//import BLKFlexibleHeightBar
//import Sheriff
import CarbonKit
//import LGSideMenuController
//import BubbleTransition
import CoreLocation
import Parse
import QuartzCore
//import DOFavoriteButton
//import JLToast
import Crashlytics
import Cartography
//import RNActivityView
import Social
import STLocationRequest
//import UIView_draggable

public enum MenuItemMode {
    case None
    case Underline(height: CGFloat, color: UIColor, horizontalPadding: CGFloat, verticalPadding: CGFloat)
    case RoundRect(radius: CGFloat, horizontalPadding: CGFloat, verticalPadding: CGFloat, selectedColor: UIColor)
}


let presentAnimationTransition = presentTransitionAnimation()
let dismissAnimationTransition = dismissTransitionAnimation()

let interactivePresentTransition = InteractiveTransition()
let interactiveDismissTransition = InteractiveTransition()

//let transition = BubbleTransition()
var search_animator:ZFModalTransitionAnimator!
//Create the delegate protocol to add a mediaItem to a lobeList
protocol MainViewDelegate {
}

var sideMenuControllerInstance:SideMenuController?

class MainVC: UIViewController {
    
    let playlistViewGroupContraint = ConstraintGroup()
    
    // MARK: Attributes
    
    @IBOutlet var playButton:UIButton!
    @IBOutlet var playIndicatorView: UIView!
    @IBOutlet var playerViewBackground: SABlurImageView!
    @IBOutlet var playerView:UIView!
    @IBOutlet var socialButton:UIButton!
    @IBOutlet var topView:UIView!
    @IBOutlet var currentSongLabel:UILabel!
    @IBOutlet var currentArtistLabel:UILabel!

    @IBOutlet var containerView: UIView!
    @IBOutlet var webViewContainerView:UIView!
    @IBOutlet var youtubePlayerContainerView:UIView!
    @IBOutlet var playlistContainerView:UIView!
    @IBOutlet var loadingView:UIView!
 
     @IBOutlet var slider:UISlider!
    
//    @IBOutlet var likeButton:DOFavoriteButton!
    @IBOutlet var nextButton:UIButton!
    @IBOutlet var youtubeExpandButton:UIButton!
    
    @IBOutlet var searchBar:UISearchBar!
    
    
    @IBOutlet var loadingStatusLabel:UILabel!
    

    
    var artWorkImageView:UIImageView?
    var timer:NSTimer!
    var viewControllers: [UIViewController]=[]
//    var pagingMenuController: PagingMenuController?
    var playlistName_ :String = "New Lobe List"
    
    var playlistVC: PlaylistVC?
    var musicVC: MusicVC?
    var webViewVC:LobeWebVC?
//    var menuVC:MenuVC?
    var menuContainerVC:MenuContainerVC?
    var searchVC:SearchVC?
    
    
    var nowPlayingIndexInLobeList: Int = 0
    var nowPlayingIndexInMusicList: Int = 0
    var isPlayingExistInLobeList: Bool = false
    
//    var delegateSplitter: BLKDelegateSplitter?
    var delegate:MainViewDelegate?
    
//    var animator : ARNTransitionAnimator!
    var animator2:ZFModalTransitionAnimator!
    
    var animatorModal:ZFModalTransitionAnimator!
    var menuAnimator:ZFModalTransitionAnimator!
    var modalVC : InitVC!

    @IBOutlet var lobeListCountLabel: UILabel!
    
    var carbonTabSwipeNavigation:CarbonTabSwipeNavigation!
    let youtubeViewGroupContraint = ConstraintGroup()
    let ytExpandButtonGroupContraint = ConstraintGroup()
    
    var isInteractiveTransitionDisabled = false
    var spotifyAuthViewController:SPTAuthViewController = SPTAuthViewController.authenticationViewController()
    
    // MARK: Start of function declaration
    override func viewDidLoad() {
        super.viewDidLoad()

        LobeStateHelper.sharedInstance.addLobeStateObserver(self)
        LobeStateHelper.sharedInstance.addUserStateObserver(self)
        LobeStateHelper.sharedInstance.addPushNotificatonObserver(self)
        AUDIO_OUT_MANAGER.addMainViewInstance(self)
        LobeStateHelper.sharedInstance.addLobeStateObserver(AUDIO_OUT_MANAGER)
        
        setupUserInterface()

        self.observeCurrentSongChange()
        
        LobeStateHelper.sharedInstance.observeChatViewState()
        
        self.becomeFirstResponder()
        UIApplication.sharedApplication().beginReceivingRemoteControlEvents()
        
        self.getLocation()

    
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
//        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MainVC.rotated), name: UIDeviceOrientationDidChangeNotification, object: nil)
        self.updateMusicPlayingUI()
        self.checkNetworkConnection()
        
    }
    
    func checkNetworkConnection(){
        
        if Reachability.isConnectedToNetwork() == true {
            print("Internet connection OK")
            
            
        } else {
            print("Internet connection FAILED")
            
            let alert = UIAlertController(title: "No Internet Connection", message: "Lobe requires a good internet connection in order to work. Please check Wifi or Cellular data connection and try again.", preferredStyle: .Alert)
            let firstAction: UIAlertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) { (alert: UIAlertAction!) -> Void in
            }
            alert.addAction(firstAction)
            self.presentViewController(alert, animated: true, completion: {
                self.loadingStatusLabel.font = UIFont(name: "HelveticaNeue-Bold", size: 16)
                self.loadingStatusLabel.text = "Oups! We could not connect to the internet. Please check your Wifi or Cellular Data connection and try opening Lobe again."
            })
            
        }
    }

    func getLocation(){
        
        LocationHelper.sharedInstance.startMonitoring()
        guard let location = LocationHelper.sharedInstance.manager.location else {
            debugPrint("getDeviceLocation Failed to get location..")
            return
        }
        
        let longitude: CLLocationDegrees = location.coordinate.longitude
        let latitude: CLLocationDegrees = location.coordinate.latitude
        LocationHelper.sharedInstance.location_latitude = latitude
        LocationHelper.sharedInstance.location_longitude = longitude
        
        print("longitude: \(longitude), latitude:\(latitude)")
        LocationHelper.sharedInstance.stopMonitoring()
    }
    
    override func shouldAutorotate() -> Bool {
        if LobeStateHelper.sharedInstance.youtube_view_mode == .FullScreen { return true }
        return false
    }
    
    override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
        //        if LobeStateHelper.sharedInstance.youtube_view_mode == .FullScreen { return UIInterfaceOrientation.LandscapeRight }
        return UIInterfaceOrientation.Portrait
    }
    
    func leaveFullScreenLandscape(){
        
        let value = UIInterfaceOrientation.Portrait.rawValue
        UIDevice.currentDevice().setValue(value, forKey: "orientation")
        self.modalVC.leaveFullScreenLandscape()
        
    }
    
    func enterFullScreenLandscape(){
        
//        let value = UIInterfaceOrientation.LandscapeRight.rawValue
//        UIDevice.currentDevice().setValue(value, forKey: "orientation")
//        LobeStateHelper.sharedInstance.youtube_view_mode = .FullScreen
        self.modalVC.enterFullScreenLandscape()
        
    }
    
    func resizeYoutubeView(viewMode viewMode:YoutubeViewMode){

        self.modalVC.resizeYoutubeView(viewMode: viewMode)
    }
    
    func enableItems(){
        nextButton.enabled = true
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }
    
//    override func viewDidAppear(animated: Bool) {
//        super.viewDidAppear(true)
////        let value = UIInterfaceOrientation.Portrait.rawValue
////        UIDevice.currentDevice().setValue(value, forKey: "orientation")
//
////        let labelTap1=UITapGestureRecognizer(target: self, action: #selector(MainVC.youtubeWebViewTapped(_:)))
////        labelTap1.numberOfTapsRequired = 1
////        labelTap1.cancelsTouchesInView = false
////        self.youtubePlayerContainerView.addGestureRecognizer(labelTap1)
//        
//    }

    //Show my lobelive view
    @IBAction func socialBtnPressed(sender: AnyObject) {
        self.toggleWebViewContainerView(isHidden: false)
    }
    
    @IBAction func optionBtnPressed(sender: AnyObject){

//        showTrackOptionPopover()
    }
    @IBAction func searchBtnPressed(sender: AnyObject){
        goToSearchView()
    }
    
}

//==========================================================================================
// MARK: - Observers
//==========================================================================================
extension MainVC {
    
    func observeCurrentSongChange(){
        //Observe current song
        AUDIO_OUT_MANAGER.currentTrackObservable.observe{ currentTrack in
            
            dispatch_async(dispatch_get_main_queue()) {
//                self.enableItems()
                self.currentSongLabel.text = "Live Playlist"//currentTrack.trackInfo.mName
                self.currentArtistLabel.text = currentTrack.trackInfo.mName //currentTrack.trackInfo.mArtist
                self.updateMusicPlayingUI()
                
                let image = UIImage(named: "lobe_background_2")
                let croppedImage = DownloadHelper.cropImage(image!, withWidth: Int(CGRectGetWidth(self.view.bounds)) , andHeigth:Int(CGRectGetHeight(self.playerView.bounds)))
                dispatch_async(dispatch_get_main_queue()) {
                    guard let imageView = self.artWorkImageView else {return}
                    imageView.image = croppedImage
                    self.playerView.backgroundColor = UIColor(patternImage: croppedImage)
                    self.topView.backgroundColor = UIColor(patternImage: croppedImage)
                }
            }
            
            currentTrack.trackInfo.songAlbumArt.observe{ image in
                if let albumImage = image {
                    
                    let croppedImage = DownloadHelper.cropImage(albumImage, withWidth: Int(CGRectGetWidth(self.view.bounds)) , andHeigth:Int(CGRectGetHeight(self.view.bounds)))
                    
                    dispatch_async(dispatch_get_main_queue()) {
//                        self.playerViewBackground.image = croppedImage
                        self.playerView.backgroundColor = UIColor(patternImage: croppedImage)
                        self.topView.backgroundColor = UIColor(patternImage: croppedImage)
                    }
                }
            }
        }
        
        social_slider_value.observe{ sliderValue in
            dispatch_async(dispatch_get_main_queue()) {
                self.slider.value = Float(sliderValue)
            }
        }
        
        //Observe lobelist
        AUDIO_OUT_MANAGER.lobeListObservableList.observe{ newList in
            dispatch_async(dispatch_get_main_queue()) {
                self.lobeListCountLabel.text = "\(AUDIO_OUT_MANAGER.lobeListSongItems.count)"
                self.updateMusicPlayingUI()
            }
        }
        
        //Observe audio playing state
        AUDIO_OUT_MANAGER.isAudioPlaying.observe{ isAudioPlaying in
            dispatch_async(dispatch_get_main_queue()) {
                self.updateMusicPlayingUI()
            }
        }
        
        AUDIO_OUT_MANAGER.inBackgroundMode.observe{ isAudioPlaying in
            dispatch_async(dispatch_get_main_queue()) {
                self.updateMusicPlayingUI()
            }
        }
    }
    
    func updateMusicPlayingUI(){
        self.playButton.selected = AUDIO_OUT_MANAGER.isMusicPlaying()
    }

    
    func toggleLoadingView(isHidden isHidden:Bool){
        self.loadingView.hidden = isHidden
    }
    
    func togglePlaybackView(isHidden isHidden:Bool){
        dispatch_async(dispatch_get_main_queue()) {
            self.webViewVC?.resizeWebView(enlarge: isHidden)
            self.playerView.hidden = isHidden
        }
        
    }
}
extension MainVC:UIGestureRecognizerDelegate {
    
//    func panGesture(sender:UIPanGestureRecognizer){
//        
//        if(sender.isLeft(self.youtubePlayerContainerView)){
//            print("Gesture went left")
//        }else{
//            print("Gesture went right")
//        }
//    }
    func panGesture(sender:UIPanGestureRecognizer){
        
        print("Gesture went right")
    }
}

extension UIPanGestureRecognizer {
    
    func isLeft(theViewYouArePassing: UIView) -> Bool {
        let velocity : CGPoint = velocityInView(theViewYouArePassing)
        if velocity.x > 0 {
            print("Gesture went right")
            return false
        } else {
            print("Gesture went left")
            return true
        }
    }
}


//==========================================================================================
//MARK: - Bacground Music Player Playback
//==========================================================================================
extension MainVC:AVAudioPlayerDelegate {
    
    override func remoteControlReceivedWithEvent(event: UIEvent?) {
        let rc = event!.subtype
        print("Received control event: \(rc.rawValue)")
        
        if (event!.type == UIEventType.RemoteControl) {
            
            switch (event!.subtype) {
                
            case .RemoteControlNextTrack:
                print("RemoteControlNextTrack")
                break;
                
            case .RemoteControlPreviousTrack:
                print("RemoteControlPreviousTrack")
                break;
                
            case .RemoteControlPlay:
                print("RemoteControlPlay")
                break;
            case .RemoteControlPause:
                print("RemoteControlPause")
                break;

            case .RemoteControlTogglePlayPause:
                print("RemoteControlTogglePlayPause")
                break;
                
            default:
                break;
            }
        }
    }
    
    override func canBecomeFirstResponder() -> Bool {
        return true
    }
}

//==========================================================================================
// MARK: - UI Change Management
//==========================================================================================
extension MainVC {
    
    func showLoading(){
        
//        self.view.showActivityViewWithLabel("Loading ..")
    }
    
    func hideLoading(){
        
//        self.view.hideActivityView()
    }
    func toggleYoutubePlayerView(isHidden isHidden:Bool){
        
        self.modalVC.toggleYoutubePlayerView(isHidden: isHidden)
        
    }
    
    func toggleWebViewContainerView(isHidden isHidden:Bool){
        
        if isHidden {
            
            UIView.animateWithDuration(0.3, delay: 0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
                self.webViewContainerView.alpha = 0
                }, completion: { finished in
                    self.webViewContainerView.hidden = true
            })
            
        }else{
            
            UIView.animateWithDuration(0.3, delay: 0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
                self.webViewContainerView.alpha = 1
                }, completion: { finished in
                    self.webViewContainerView.hidden = false
            })
        }
    }

    func setupUserInterface(){
        
        self.toggleWebViewContainerView(isHidden: false)
        
        setupMusicInitVC()
        blurImage()
//        setupCarbonKitTab()
//        setupPagingMenuController()
        
        // Change navbar appearance
        UINavigationBar.appearance().translucent = true
        UINavigationBar.appearance().barTintColor = UIColor.clearColor()
        UINavigationBar.appearance().tintColor = UIColor.clearColor()
        UINavigationBar.appearance().backgroundColor = UIColor.clearColor()
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.whiteColor()]
        self.navigationItem.title = ""
        
        self.containerView.backgroundColor = UIColor.clearColor()
        self.view.backgroundColor = UIColor.clearColor()
        definesPresentationContext = true
        
//        let px = 4 / UIScreen.mainScreen().scale
//        let frame = CGRectMake(0, 0, self.playerView.frame.size.width, px)
//        let line: UIView = UIView(frame: frame)
//        self.playerView.addSubview(line)
//        line.backgroundColor = UIColor.appThemeColorBackgroundThirdOrange()

        lobeListCountLabel.layer.cornerRadius = CGRectGetWidth(lobeListCountLabel.bounds)/2
        lobeListCountLabel.layer.masksToBounds = true
        lobeListCountLabel.clipsToBounds = true
        lobeListCountLabel.text = "\(AUDIO_OUT_MANAGER.lobeListSongItems.count)"
        
        //MARK: - Placeholder Image
        let width = Int(CGRectGetWidth(self.view.bounds))
        let heigth = Int(CGRectGetHeight(self.view.bounds))
        let croppedImage = DownloadHelper.cropImage( UIImage(named: placeholderImageName)!, withWidth: width , andHeigth:heigth)
//        self.playerViewBackground.image = croppedImage
        self.playerView.backgroundColor = UIColor(patternImage: croppedImage)
        
        setupSearchVC()
        
        constrain(self.playlistContainerView, replace: self.playlistViewGroupContraint) { playlistView in
        
            let screenSize = UIScreen.mainScreen().bounds.height
            playlistView.top  == playlistView.superview!.top + screenSize
//            playlistView.centerY  == playlistView.superview!.centerY + screenSize
//            playlistView.right == playlistView.superview!.right
            playlistView.left == playlistView.superview!.left
            playlistView.height == playlistView.superview!.height
            playlistView.width == playlistView.superview!.width
        }
        self.playlistContainerView.hidden = false
    }
    
    func blurImage() {
        
        let darkBlur = UIBlurEffect(style: UIBlurEffectStyle.Dark)
        
        //Player view blur
//        let screenSize: CGRect = UIScreen.mainScreen().bounds
        
        let blurView = UIVisualEffectView(effect: darkBlur)
        blurView.frame = self.view.bounds
        self.playerView.addSubview(blurView)
        self.playerView.sendSubviewToBack(blurView)
        
        constrain(blurView) { blurView in
            
            blurView.top  == blurView.superview!.top
            blurView.bottom  == blurView.superview!.bottom
            blurView.right == blurView.superview!.right
            blurView.left == blurView.superview!.left
        }
        
        //Top view blur
        let blurView2 = UIVisualEffectView(effect: darkBlur)
        blurView2.frame = topView.bounds
        topView.addSubview(blurView2)
        topView.sendSubviewToBack(blurView2)
        topView.backgroundColor = UIColor(patternImage: UIImage(named: placeholderImageName)!)
        
        constrain(blurView2) { blurView in
            
            blurView.top  == blurView.superview!.top
            blurView.bottom  == blurView.superview!.bottom
            blurView.right == blurView.superview!.right
            blurView.left == blurView.superview!.left
        }
        
        //Top view bottom black line
        let px = 2 / UIScreen.mainScreen().scale
        let frame = CGRectMake(0, CGRectGetHeight(self.topView.frame), self.topView.frame.size.width, px)
        let line: UIView = UIView(frame: frame)
        self.topView.addSubview(line)
        line.backgroundColor = UIColor.blackColor()
        
    }
    
//    func likeButtonSelected(sender: DOFavoriteButton) {
//        print("Ici")
//        let trackInfo = AUDIO_OUT_MANAGER.currentSong.trackInfo
//        if sender.selected {
//            // deselect
////            NativeToWebHelper.sharedInstance.dislikeTrackInLobeList(-1, trackUid: (trackInfo.mID))
////            sender.deselect()
//        } else {
//            // select with animation
//            
//            NativeToWebHelper.sharedInstance.promoteTrackInLobeList(-1, trackUid: (trackInfo.mID))
//            sender.select()
//        }
//    }
    
}

//==========================================================================================
// MARK: lobeWebVC & musicVC SEGUE
//==========================================================================================
extension MainVC {
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let vc = segue.destinationViewController as? LobeWebVC
            where segue.identifier == "webViewSegue" {
                self.webViewVC = vc
        }
        
        if let viewController = segue.destinationViewController as? MusicVC
            where segue.identifier == "musicVCSegue" {
                self.musicVC = viewController
        }
        
        if let viewController = segue.destinationViewController as? InitVC
            where segue.identifier == "playlistVCSegue" {
            self.modalVC = viewController
            self.modalVC.delegate = self
        }
    }
}

//==========================================================================================
// MARK: Events Notification Management
//==========================================================================================
extension MainVC {
    
    func lobeStateChanged(){
        
        dispatch_async(dispatch_get_main_queue()) {
            
//            self.likeButton.hidden = true
            self.nextButton.hidden = false
//            self.enableItems()
            
//            switch LobeStateHelper.sharedInstance.state {
//                case .Local:
//                    self.webViewVC?.resizeWebView(enlarge: true)
//                   self.togglePlaybackView(isHidden: true)
//                case .DJ,.Participant:
//                    self.webViewVC?.resizeWebView(enlarge: false)
//                    self.togglePlaybackView(isHidden: false)
//            }
        
            self.updateMusicPlayingUI()
        }
    }
    
    func userStateChanged(){
//        if let image = LobeStateHelper.currentUser.userImage {
//            socialButton.setImage(image, forState: UIControlState.Normal)
//        }
    }
    
    func actOnMusicChange(notification:NSNotification) {
        dispatch_async(dispatch_get_main_queue()) {
            let userInfo:Dictionary<String,Bool!> = notification.userInfo as! Dictionary<String,Bool!>
            let songChanged:Bool = userInfo["songChanged"]!
            if songChanged {
                self.updateViewOnMusicChange(songChanged)
            }
        }
    }
    func actOnLobeListChange(){
        self.updateViewOnMusicChange(false)
    }
    
    func updateViewOnMusicChange(songChanged:Bool){
        
    }
}

//==========================================================================================
// MARK: - PagingMenuControllerDelegate
//==========================================================================================
//extension MainVC:PagingMenuControllerDelegate{
//    
//    func setupPagingMenuController(){
//        
//        viewControllers = [musicVC!,playlistVC!]
//        
//        let options = PagingMenuOptions()
//        options.menuHeight = 44
//        options.font = UIFont.systemFontOfSize(12)
//        options.selectedFont = UIFont.systemFontOfSize(12)
//        options.menuDisplayMode = .SegmentedControl
//        options.backgroundColor = SEGMENT_MENU_COLOR
//        options.selectedBackgroundColor = SEGMENT_MENU_COLOR
//        options.selectedTextColor = UIColor.hexColor("D2D2D2")
//        options.animationDuration = 0.01
//        //        options.deceleratingRate = 0
//        
//        pagingMenuController = self.childViewControllers.first as? PagingMenuController
//        pagingMenuController!.delegate = self
//        pagingMenuController!.setup(viewControllers: viewControllers, options: options)
//        
//        if let imageView = pagingMenuController!.view.viewWithTag(15) as? RounderCornerImageView {
//            imageView.layer.cornerRadius = imageView.frame.width/2
//        }
//        pagingMenuController?.view.backgroundColor = UIColor.clearColor()
//        
//    }
//    
//    
//    func willMoveToMenuPage(page: Int) {
////        print("will move to \(page)")
//        //        playlistVC!.tableView.reloadData()
//        //        suggestionsVC!.tableView.reloadData()
//    }
//    
//    func didMoveToMenuPage(page: Int) {
////        print("did move to \(page)")
//        playlistVC!.tableView.reloadData()
//    }
//    
//}

//==========================================================================================
// MARK: - CarbonTabSwipeNavigation Delegate
//==========================================================================================
extension MainVC:CarbonTabSwipeNavigationDelegate{
    
    func setupCarbonKitTab(){
        
        let tabMenuItems = ["Library","Lobelist"]
        
        carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: tabMenuItems, delegate: self)
        carbonTabSwipeNavigation.insertIntoRootViewController(self.childViewControllers.first!)
        
        carbonTabSwipeNavigation.toolbar.translucent = true
        carbonTabSwipeNavigation.toolbar.backgroundColor = UIColor.clearColor()
        carbonTabSwipeNavigation.setIndicatorColor(UIColor.orangeColor())
        carbonTabSwipeNavigation.toolbarHeight.constant = 0
        
        let screenSize: CGRect = UIScreen.mainScreen().bounds
        let width = screenSize.width/2
        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(width, forSegmentAtIndex: 0)
        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(width+2, forSegmentAtIndex: 1)
        
        // Custimize segmented control
        carbonTabSwipeNavigation.setNormalColor(UIColor.lightGrayColor(), font: UIFont.systemFontOfSize(12))
        carbonTabSwipeNavigation.setSelectedColor(UIColor.hexColor("D2D2D2"), font: UIFont.systemFontOfSize(12))
        carbonTabSwipeNavigation.view.backgroundColor = UIColor.clearColor()
//        carbonTabSwipeNavigation.carbonSegmentedControl?.backgroundColor = UIColor.ligthBlackColor()
        
        //Loads all the vc
        for (var i:UInt = 0; i < 1; i++){
            carbonTabSwipeNavigation.currentTabIndex = i
            carbonTabSwipeNavigation.currentTabIndex = 0
        }
        
    }
    
    func carbonTabSwipeNavigation(carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAtIndex index: UInt) -> UIViewController {
        switch index {
        case 0:
            return musicVC!
        case 1:
            return playlistVC!
        default:return musicVC!
        }
    }
    
    func carbonTabSwipeNavigation(carbonTabSwipeNavigation: CarbonTabSwipeNavigation, willMoveAtIndex index: UInt) {
        
    }
    
    func carbonTabSwipeNavigation(carbonTabSwipeNavigation: CarbonTabSwipeNavigation, didMoveAtIndex index: UInt) {
        
    }
    
    func barPositionForCarbonTabSwipeNavigation(carbonTabSwipeNavigation: CarbonTabSwipeNavigation) -> UIBarPosition {
        return UIBarPosition.Top
    }
}

//==========================================================================================
// TODO: - OLD playlist Change Management functions need to move them to InitVC
//==========================================================================================

extension MainVC{
    
//    func mainUpdateAfterDelete(){
//        self.showUserPlaylist()
//    }
//    func showUserPlaylist(){
//        if let UserPlaylistsVC = storyboard!.instantiateViewControllerWithIdentifier("UserPlaylistsVC") as? UserPlaylistsVC {
//            presentViewController(UserPlaylistsVC, animated: true, completion: nil)
//        }
//    }
}


//==========================================================================================
// MARK: InitVCDelegate
//==========================================================================================
extension MainVC:InitVCDelegate {
    func openWebView() {
        NativeToWebHelper.sharedInstance.showView("myProfileView", backToViewString: "")
        self.toggleWebViewContainerView(isHidden: false)
        //        menuViewProfile()
    }
    
    func nextButtonTapped(){
        self.handleNextButtonTapped()
    }
    func previousButtonTapped(){
        self.handlePreviousButtonTapped()
    }
    func playPauseButtonTapped(){
        self.handlePlayOrPauseMusic()
    }
    func setupTargetView(){
//        self.animator.gestureTargetView = self.modalVC!.targetImageView
    }
    
    func setupSearchVC(){
        
        searchVC  = self.storyboard?.instantiateViewControllerWithIdentifier("SearchVC") as? SearchVC
//        menuVC = self.storyboard?.instantiateViewControllerWithIdentifier("MenuVC") as? MenuVC
        //        searchVC?.delegate = musicVC!
        
        search_animator = ZFModalTransitionAnimator(modalViewController: searchVC!)
        search_animator.dragable = true
        search_animator.bounces = false
        search_animator.behindViewAlpha = 0.5
        search_animator.behindViewScale = 1.0
        search_animator.transitionDuration = 0.7
        search_animator.direction = ZFModalTransitonDirection.Bottom
        searchVC!.transitioningDelegate = search_animator
        searchVC!.modalPresentationStyle = UIModalPresentationStyle.Custom
        
    }
    
    func goToSearchView(){
        
//        let searchVC  = self.storyboard?.instantiateViewControllerWithIdentifier("SearchVC") as! SearchVC
        presentViewController(searchVC!, animated: true, completion: { () -> Void in
            
        })
    }
    
    func goToChatView(){
        
        NativeToWebHelper.sharedInstance.showView("chatting", backToViewString: "")
        self.toggleWebViewContainerView(isHidden: false)
    }
    func goToYoutubeView(){
        NativeToWebHelper.sharedInstance.showView("youtubeSearch", backToViewString: "")
        self.toggleWebViewContainerView(isHidden: false)
    }
    func goToLocalLibraryView(){
        NativeToWebHelper.sharedInstance.showView("localMusicView", backToViewString: "")
        self.toggleWebViewContainerView(isHidden: true)
    }
}

//==========================================================================================
// MARK: - Music Playback Management
//==========================================================================================
extension MainVC {
    
    @IBAction func playOrPauseMusic(sender: UIButton) {
        
        self.handlePlayOrPauseMusic()
        
    }
    
    @IBAction func backButton(sender: AnyObject) {
        
        handlePreviousButtonTapped()
    }
    @IBAction func forwardButton(sender: UIButton) {
        
//        if LobeStateHelper.sharedInstance.state != .Local {
//            guard AUDIO_OUT_MANAGER.lobeListSongItems.count > 1 else {return}
//        }
        
        handleNextButtonTapped()
    }
    
    func handlePlayOrPauseMusic() {
        
        if AUDIO_OUT_MANAGER.isMusicPlaying() {
            AUDIO_OUT_MANAGER.pauseMusic()
        }else{
            AUDIO_OUT_MANAGER.playMusic()
        }
        
    }
    
    func handleNextButtonTapped(){
        
        AUDIO_OUT_MANAGER.skipToNextMusic()
    }
    func handlePreviousButtonTapped(){

        AUDIO_OUT_MANAGER.skipToPreviousMusic()
    }
}

//==========================================================================================
// MARK: - Menu Delegate
//==========================================================================================
extension MainVC:MenuDelegate{
    
    func menuSearch(){
        self.sideMenuController()?.toggleSidePanel()
        self.goToSearchView()
    }
    func menuGoToCurrentSong(){
    }
    func menuLeaveGroup(){
        self.sideMenuController()?.toggleSidePanel()
        NativeToWebHelper.sharedInstance.leaveGroupSession()
        AUDIO_OUT_MANAGER.userDidLeaveGroup()
//        AUDIO_OUT_MANAGER.stopSocialAudio()
        
    }
    func menuRecalculateSync(){
        NativeToWebHelper.sharedInstance.recalculateSyncAlgo()
    }
    func menuLogout(){
//        print("Menu Logout")
    }
    
    func menuViewProfile() {
//        print("User Profile")
        self.sideMenuController()?.toggleSidePanel()
        NativeToWebHelper.sharedInstance.showView("myProfileView", backToViewString: "")
        self.toggleWebViewContainerView(isHidden: false)
    }
    
    func menuViewLobeLive(){
        self.sideMenuController()?.toggleSidePanel()
        NativeToWebHelper.sharedInstance.showView("lobeLiveView", backToViewString: "")
        self.toggleWebViewContainerView(isHidden: false)
    }
    func menuViewGroup(){
        self.sideMenuController()?.toggleSidePanel()
        NativeToWebHelper.sharedInstance.showView("groupView", backToViewString: "")
        self.toggleWebViewContainerView(isHidden: false)
    }
    
    func menuViewLocalLibrary(){
        self.sideMenuController()?.toggleSidePanel()
        self.toggleWebViewContainerView(isHidden: true)
        LobeStateHelper.sharedInstance.menu_selected_index.value = 4
        NativeToWebHelper.sharedInstance.showView("localMusicView", backToViewString: "")
    }
    func menuViewLocalLibraryOnly(){
        self.toggleWebViewContainerView(isHidden: true)
//        NativeToWebHelper.sharedInstance.showView("localMusicView", backToViewString: "")
    }
    func menuViewHome() {
        self.sideMenuController()?.toggleSidePanel()
        NativeToWebHelper.sharedInstance.showView("homePageView", backToViewString: "")
        self.toggleWebViewContainerView(isHidden: false)
    }
    
    func menuViewPlaybackView(){
        self.sideMenuController()?.toggleSidePanel()
        expandPlayerView()
    }
    
    func menuViewChatView(){
        self.sideMenuController()?.toggleSidePanel()
        NativeToWebHelper.sharedInstance.showView("chatting", backToViewString: "")
        self.toggleWebViewContainerView(isHidden: false)
    }
    
    
    @IBAction func menuButtonPressed(sender: AnyObject) {
        
        // 
        guard !self.webViewContainerView.hidden else{
            self.toggleWebViewContainerView(isHidden: false)
            NativeToWebHelper.sharedInstance.backButtonWasPressed()
            return
        }
        self.sideMenuController()?.menuDelegate = self
        self.sideMenuController()?.toggleSidePanel()
    }
    
}

//==========================================================================================
// MARK: - PUSH NOTIF & MAGIC BUTTON
//==========================================================================================
extension MainVC {
    
    func actOnPushNotifReceived(){
        print("push received")
        dispatch_async(dispatch_get_main_queue()) {
            NativeToWebHelper.sharedInstance.JavaCallPushNotifActionReceived()
            self.toggleWebViewContainerView(isHidden: false)
        }
    }

}

// MARK: - Searchbar Delegate
extension MainVC: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {

    }
    func searchBarShouldBeginEditing(searchBar: UISearchBar) -> Bool {
        self.resignFirstResponder()
        self.goToSearchView()
        return false
    }
    
}

//extension MainVC {
//    
//    func showTrackOptionPopover(){
//        
//        var alert: UIAlertController!
//        var firstAction: UIAlertAction!
//        var secondAction: UIAlertAction!
//        var thirdAction: UIAlertAction!
//        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: {(alert: UIAlertAction!) in
////            print("cancel")
//        })
//        
//        alert = UIAlertController(title: "Views song", message: "", preferredStyle: .ActionSheet)
//        firstAction = UIAlertAction(title: "By Playlist", style: UIAlertActionStyle.Default) { (alert: UIAlertAction!) -> Void in
//            
//            JLToast.makeText(" This option is not yet implemented").show()
//
//        }
//        secondAction = UIAlertAction(title: "By Artist", style: .Default) { (alert: UIAlertAction!) -> Void in
//            
//            JLToast.makeText(" This option is not yet implemented").show()
//            
//        }
//        thirdAction = UIAlertAction(title: "By Album", style: .Default) { (alert: UIAlertAction!) -> Void in
//            
//            JLToast.makeText(" This option is not yet implemented").show()
//            
//        }
//        alert.addAction(firstAction)
//        alert.addAction(secondAction)
//        alert.addAction(thirdAction)
//        alert.addAction(cancelAction)
//        presentViewController(alert, animated: true, completion:nil)
//    }
//}

//==========================================================================================
// MARK: - present InitVC transition Animation
//==========================================================================================
extension MainVC {

    
    func goToPlaybackView(){
        
        presentViewController(modalVC!, animated: true, completion: { () -> Void in
            
        })
    }
    
    func setupMusicInitVC(){
//        self.definesPresentationContext = true
//        let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
//        self.modalVC = storyboard.instantiateViewControllerWithIdentifier("InitVC") as? InitVC
//        self.modalVC.delegate = self
//        var _ = self.modalVC.view.frame //hack to preload the vc
//        self.modalVC.transitioningDelegate = self;
////        self.modalVC.modalTransitionStyle = .CoverVertical;
//        self.modalVC.modalPresentationStyle = .Custom;
//        self.modalVC.rootViewController = self
//        
//        interactivePresentTransition.attachToViewController(self, viewToAttach:self.playerView , presentViewController: self.modalVC)
//        interactiveDismissTransition.attachToViewController(self.modalVC, viewToAttach:self.modalVC.targetImageView , presentViewController: nil)
        self.playerView.userInteractionEnabled = true
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MainVC.expandPlayerView))
        self.playerView.addGestureRecognizer(gestureRecognizer)
        
    }
//
    @IBAction func expandPlayerView() {
        
//            self.presentViewController(modalVC, animated: true) { () -> Void in
////                self.menuAnimator.setContentScrollView(self.modalVC.playlistVC?.tableView)
//            }
        
        //Drag to present
//        guard let _ = modalVC else {
////            debugPrint("playbackview not ready yet..can't expand")
//            return
//        }
//        if modalVC.isViewLoaded() {
////            debugPrint("playbackview is already loaded..")
//        }
//        
//        guard !modalVC.isBeingPresented() else {
////            debugPrint("playbackview is already being presented..")
//            return
//        }
//        
//        guard !modalVC.isMovingFromParentViewController() else {
////            debugPrint("playbackview is being removed from parentview controller..can't process the expand request")
//            return
//        }
//        
//        guard !modalVC.isMovingToParentViewController() else {
////            debugPrint("playbackview is being add to parentview controller..can't process the expand request")
//            return
//        }
//        
//        self.isInteractiveTransitionDisabled = true
//        self.presentViewController(modalVC, animated: true) { () -> Void in
//             self.isInteractiveTransitionDisabled = false
////            LobeStateHelper.sharedInstance.updateYoutubeViewState()
//        }
        self.togglePlaylistView(isHidden: false)
//        self.playlistContainerView.hidden = false
    }
    
}

extension MainVC:UIViewControllerTransitioningDelegate {
    
    //Mark - Custom modal Transitions animation
    func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//        LobeStateHelper.sharedInstance.updateYoutubeViewState()
        presentAnimationTransition.initialY = Double(self.playerView.frame.size.height)
        return presentAnimationTransition
    }
    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {

        dismissAnimationTransition.initialY = Double(self.playerView.frame.size.height)
        return dismissAnimationTransition
    }

    func interactionControllerForPresentation(animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        guard !self.isInteractiveTransitionDisabled else {
//            debugPrint("interactive unavailable")
            return nil
        }
        LobeStateHelper.sharedInstance.updateYoutubeViewState()
        return interactivePresentTransition
    }
    
    func interactionControllerForDismissal(animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        guard !self.isInteractiveTransitionDisabled else {
//            debugPrint("interactive dismiss unavailable")
            return nil
        }
        let value = UIInterfaceOrientation.Portrait.rawValue
        UIDevice.currentDevice().setValue(value, forKey: "orientation")
        return interactiveDismissTransition
    }
}

extension UINavigationController {
    
    public override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
        return UIInterfaceOrientation.Portrait
    }
    
    public override func shouldAutorotate() -> Bool {
        return false
    }
}

extension MainVC {
    
    func displayShareSheet(group_id:String) {
        
        dispatch_async(dispatch_get_main_queue()) {
            
    //        let shareContent = "Hey Folks, Im listen to \(AUDIO_OUT_MANAGER.currentSong.trackInfo.mName) on Lobe! Come Join me!"
    //        let image: UIImage = UIImage(named: "Icon-72")!
            let url = NSURL(string: "https://appymood-lobe.rhcloud.com/invite-page.html?group_id=\(group_id)")!
            let activityViewController = UIActivityViewController(activityItems: [(url)], applicationActivities: nil)

            // Anything you want to exclude
            activityViewController.excludedActivityTypes = [
                UIActivityTypePostToWeibo,
                UIActivityTypePrint,
                UIActivityTypeCopyToPasteboard
            ]
            self.presentViewController(activityViewController, animated: true, completion: { Void in
                self.navigationController?.navigationBar.hidden = false
            })
        }
    }
    
    func shareOnFacebook(shareContent:String) {
        if SLComposeViewController.isAvailableForServiceType(SLServiceTypeFacebook){
            let facebookSheet:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
            facebookSheet.setInitialText(shareContent)
            self.presentViewController(facebookSheet, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: "Accounts", message: "Please login to a Facebook account to share.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
}
//
//extension MainVC:LocationRequestDelegate {
//    
//    func locationRequestNotNow(){
//        print("The user canceled the locationRequestScreen")
//    }
//    
//    func locationRequestAuthorized(){
//        print("Location service is allowed by the user. You have now access to the user location")
//        self.getLocation()
//        LocationHelper.sharedInstance.manager.requestWhenInUseAuthorization()
//    }
//    
//    func locationRequestDenied(){
//        print("Location service are denied by the user")
//
//    }
//    
//    func locationRequestControllerPresented() {
//        print("STLocationRequestController presented")
//    }
//}

extension MainVC {
    
    func togglePlaylistView(isHidden isHidden:Bool){

        dispatch_async(dispatch_get_main_queue()) {
            self.view.layoutIfNeeded()
            UIView.animateWithDuration(0.5, delay: 0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
                
                if isHidden {
//                    self.playlistContainerView.alpha = 0
                    constrain(self.playlistContainerView, replace: self.playlistViewGroupContraint) { playlistView in
                        
                        let screenSize = UIScreen.mainScreen().bounds.height
//                        playlistView.right == playlistView.superview!.right
//                        playlistView.left == playlistView.superview!.left
                        playlistView.width == playlistView.superview!.width
                        playlistView.height == playlistView.superview!.height
//                        playlistView.bottom == playlistView.superview!.bottom + screenSize
//                        playlistView.centerY  == playlistView.superview!.centerY + screenSize
                        playlistView.top  == playlistView.superview!.bottom
//                        playlistView.left == playlistView.superview!.left
                    }
//                    UIView.animateWithDuration(0.5, animations: self.view.layoutIfNeeded)
                }else{
//                    self.playlistContainerView.alpha = 1
                    constrain(self.playlistContainerView, replace: self.playlistViewGroupContraint) { playlistView in
                        
//                        playlistView.top  == playlistView.superview!.top
//                        playlistView.right == playlistView.superview!.right
//                        playlistView.left == playlistView.superview!.left
                        playlistView.width == playlistView.superview!.width
                        playlistView.height == playlistView.superview!.height
//                        playlistView.centerY  == playlistView.superview!.centerY
                        playlistView.top  == playlistView.superview!.top
//                        playlistView.left == playlistView.superview!.left
                    }
//                    UIView.animateWithDuration(0.5, animations: self.view.layoutIfNeeded)
                }
            
                self.view.layoutIfNeeded()
            
                }, completion: { finished in
                    self.playlistContainerView.hidden = isHidden
            })
        }
    }
}

extension MainVC:SPTAuthViewDelegate{
    
    func loginSpotify(){
        
        spotifyAuthViewController.delegate = self
        spotifyAuthViewController.modalPresentationStyle = UIModalPresentationStyle.OverCurrentContext
        spotifyAuthViewController.definesPresentationContext = true
        presentViewController(spotifyAuthViewController, animated: false, completion: nil)
    }
    
    func authenticationViewController(authenticationViewController: SPTAuthViewController!, didFailToLogin error: NSError?) {
        if error != nil {
            print("didFailToLogin with error: \(error)")
            self.spotifyAuthViewController.dismissViewControllerAnimated(true, completion: nil)
        }
        
    }
    func authenticationViewControllerDidCancelLogin(authenticationViewController: SPTAuthViewController!) {
        print("authenticationViewControllerDidCancelLogin")
        self.spotifyAuthViewController.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func authenticationViewController(authenticationViewController: SPTAuthViewController!, didLoginWithSession session: SPTSession!) {
        print("didLoginWithSession: \(session)")
        self.spotifyAuthViewController.dismissViewControllerAnimated(true, completion: nil)
        SpotifyHelper.sharedInstance.successLoginCallBack(sessionObject:session)
    }
}


