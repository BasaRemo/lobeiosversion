//
//  YoutubePlayerVC.swift
//  Lobe
//
//  Created by Professional on 2016-03-30.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

import UIKit
import SwiftyJSON
import WebKit
//import WebViewJavascriptBridge
//import UIView_draggable
//import WKWebViewJavascriptBridge
import Cartography

enum YoutubeViewMode :String {
    case Docked = "Docked"
    case Large = "Large"
    case FullScreen = "FullScreen"
    
    private init () {
        self = .Large
    }
}

protocol YoutubeWebViewDelegate {
    func rotateWebView(isClockWise:Bool)
}

class YoutubePlayerVC: UIViewController {

    var playerViewToRestore: UIView? = nil
    var playerToRestore: AVPlayer? = nil
//    var bridge:WebViewJavascriptBridge?
//    var wkbridge:WKWebViewJavascriptBridge?
    var longpress = false
    var webview:WKWebView!
    override func prefersStatusBarHidden() -> Bool {
        return true;
    }
    
    override func loadView() {
        super.loadView()

    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
//        self.setupLayout()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Javascript that disables pinch-to-zoom by inserting the HTML viewport meta tag into <head>
        let source: NSString = "var meta = document.createElement('meta');" +
            "meta.name = 'viewport';" +
            "meta.content = 'width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no';" +
            "var head = document.getElementsByTagName('head')[0];" +
        "head.appendChild(meta);";
        let javascript = "document.body.style.webkitTouchCallout='none';"
        
        let script: WKUserScript = WKUserScript(source: source as String, injectionTime: .AtDocumentEnd, forMainFrameOnly: true)
        let script2: WKUserScript = WKUserScript(source: javascript as String, injectionTime: .AtDocumentEnd, forMainFrameOnly: true)
        // Create the user content controller and add the script to it
        let userContentController: WKUserContentController = WKUserContentController()
        userContentController.addUserScript(script)
        userContentController.addUserScript(script2)
        
        userContentController.addScriptMessageHandler(
            self,
            name: "playerCommApiReceivedMessage"
        )
        
        let config = WKWebViewConfiguration()
        config.userContentController = userContentController
        config.mediaPlaybackRequiresUserAction = false
        config.allowsInlineMediaPlayback = true
        if #available(iOS 9.0, *) {
            config.allowsPictureInPictureMediaPlayback = true
        } else {
            // Fallback on earlier versions
        }
        
        self.webview = WKWebView(
            frame: self.view.bounds,
            configuration: config
        )
        //        self.automaticallyAdjustsScrollViewInsets = true
        self.webview.translatesAutoresizingMaskIntoConstraints = false
        self.webview.scrollView.scrollEnabled = false
        
        
        self.view.addSubview(self.webview!)
        
        self.view.autoresizesSubviews = true
        self.view.backgroundColor = UIColor.blackColor()
//        self.setView(self.webview!)
//
        NativeToWebHelper.sharedInstance.youtube_delegate = self
//        webView.loadRequest(NSURLRequest(URL: NSURL(string: "http://appymood-lobe.rhcloud.com/youtubePlayer.html")!))
        self.webview.loadRequest(NSURLRequest(URL: NSURL(string: "https://appymood-lobe.rhcloud.com/youtubePlayer.html")!))
        self.webview.navigationDelegate = self
//
//        self.wkbridge = WKWebViewJavascriptBridge(forWebView: self.webview)
//        self.bridge = WebViewJavascriptBridge(forWebView: self.webView)
        registerJSHandlers()
        self.setupLayout()
        
//        let  longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.webViewLongPressed(_:)))
//        longPressGesture.delegate = self
//        longPressGesture.cancelsTouchesInView = true
////        longPressGesture.numberOfTapsRequired = 1
//        self.webview.addGestureRecognizer(longPressGesture)
    }
    
    func youtubeWebViewTapped(sender: UITapGestureRecognizer){
//        print("long press")
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.webview.userInteractionEnabled = true
        self.view.userInteractionEnabled = true
        if self.webview.URL == nil {
            
            self.webview.reload()
        }

    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
 
    
    
    func setupLayout(){
        self.view.layoutIfNeeded()
        constrain(self.webview ,self.view) { webView, view in

            webView.bottom  == view.bottom
            webView.top == view.top
            webView.right == view.right
            webView.left == view.left
            webView.width == view.width
            webView.height == view.height
        }       
    }

}
extension YoutubePlayerVC:UIGestureRecognizerDelegate{
    
    func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
        return true
    }
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

extension YoutubePlayerVC:YoutubeWebViewDelegate{
    
    func rotateWebView(isClockWise:Bool){
    }
}
extension YoutubePlayerVC {
    
    func applicationDidEnterBackgroundNotification(notification: NSNotification) {
//        print("applicationDidEnterBackgroundNotification ..")
        
    }
    
    func applicationDidBecomeActiveNotification(notification: NSNotification) {
        
    }
    
}
extension YoutubePlayerVC {
    
    func observeYoutubePlayerRequest(){
        
//        AUDIO_OUT_MANAGER.youtube_video_id.observe { video_id in
//        }
//        
//        AUDIO_OUT_MANAGER.shouldPlayYoutubeVideo.observe{ shouldPlayYoutubeVideo in
//        }
        
//        LobeStateHelper.sharedInstance.youtube_view_mode.
//        if LobeStateHelper.sharedInstance.youtube_view_mode == .FullScreen{
//            return UIInterfaceOrientation.LandscapeLeft
//        }else{
//            return UIInterfaceOrientation.Portrait
//        }
    }
}

extension YoutubePlayerVC:YoutubeDelegate {
    
    func javascriptFunctionCall(functionName:String, params:String){
//        print("calling js: \(functionName)")
        let jsFunction = "\(functionName)(\(params))"
        dispatch_async(dispatch_get_main_queue()) {
//            self.webView?.stringByEvaluatingJavaScriptFromString(jsFunction)
            self.webview!.evaluateJavaScript(jsFunction,completionHandler: { ( result, error) -> Void in
                guard error == nil else {
//                    print("evaluateJavaScript error: \(error)")
                    return
                }
                if let result = result{
//                    print("evaluateJavaScript result: \(result)")
                }
            })
        }
    }
    func nativeCallLoadVideo(trackInfo:TrackInfo){
        
        let letTrackJson = JsonHelper.trackInfoToJSON(trackInfo)
        guard let rawJsonString = letTrackJson.rawString() else{
//            print("FAILURE Calling JS nativeCallLoadVideo ")
            return
        }
        
//        let param = JsonHelper.replaceBadChar(rawJsonString)
//        let functionName:String  = "nativeCallLoadVideo"
//        self.javascriptFunctionCall(functionName, params: "'\(param)'")
    }
    
    func nativeCallSetIsPlayingMediaContentAllowed(isAllowed:Bool){
        
        let functionName = "nativeCallSetIsPlayingMediaContentAllowed"
        self.javascriptFunctionCall(functionName, params: "\(isAllowed)")
    }
    
    func nativeCallPushSingleSuggestionItem(trackInfo:TrackInfo){
        
        let letTrackJson = JsonHelper.trackInfoToJSON(trackInfo)
        guard let rawJsonString = letTrackJson.rawString() else{
//            print("FAILURE Calling JS nativeCallLoadVideo ")
            return
        }
        let param = JsonHelper.replaceBadChar(rawJsonString)
        let functionName = "JavaCallPushSingleSuggestionItem"
        self.javascriptFunctionCall(functionName, params: "'\(param)'")
    }
    
    func playerCommApiReceivedMessage(strObj:String){
        
        let functionName:String = "JavaCallPlayerCommApiReceivedMessage"
//        dispatch_async(dispatch_get_main_queue()) {
            self.javascriptFunctionCall(functionName, params: "\(strObj)")
//        }
        
    }
}

extension YoutubePlayerVC {
//    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
//        if UIDevice.currentDevice().orientation.isLandscape.boolValue {
//            print("Landscape change")
//        } else {
//            print("Portrait change ")
//        }
//    }

    override func shouldAutorotate() -> Bool {
        return true
    }
    
//    override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
//        return UIInterfaceOrientation.Portrait
//    }
}

extension YoutubePlayerVC {
    
    func registerJSHandlers(){
        
//        self.bridge?.registerHandler("playerCommApiReceivedMessage", handler: { (data:AnyObject!, responseCallback:WVJBResponseCallback!) -> Void in
//            print("playerCommApiReceivedMessage called with data : \(data)")
//            NativeToWebHelper.sharedInstance.playerCommApiReceivedMessage("'\(data)'")
//        })
        
//        self.wkbridge?.registerHandler("playerCommApiReceivedMessage", handler: { (data:AnyObject!, responseCallback:WVJBResponseCallback!) -> Void in
//            print("playerCommApiReceivedMessage called with data : \(data)")
//            NativeToWebHelper.sharedInstance.playerCommApiReceivedMessage("'\(data)'")
//        })
    }
}

extension YoutubePlayerVC:UIWebViewDelegate{
    
    func webViewWebContentProcessDidTerminate(webView: WKWebView) {
        self.webview.reload()
    }
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
//        print("YoutubePlayerVC shouldStartLoadWithRequest : \(request)")
        return true
    }
    func webViewDidStartLoad(webView: UIWebView) {
//        print("YoutubePlayerVC webViewDidStartLoad")
    }
    func webView(webView: UIWebView, didFailLoadWithError error: NSError?) {
//        print(" YoutubePlayerVCdidFailLoadWithError : \(error)")
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
//        print("YoutubePlayerVC webViewDidFinishLoad")
    }
}

extension YoutubePlayerVC:WKNavigationDelegate{
    
    func webView(webView: WKWebView, didFinishNavigation navigation: WKNavigation!) {
        
        //        let javascript:NSString = "var meta = document.createElement('meta');meta.setAttribute('name', 'viewport');meta.setAttribute('content', 'width=device-width, minimum-scale=1.0, initial-scale=1.0, maximum-scale=1.0, user-scalable=no');document.getElementsByTagName('head')[0].appendChild(meta);";
        let javascript:NSString = "document.body.style.webkitUserSelect='none';"
        self.webview.evaluateJavaScript("document.body.style.webkitTouchCallout='none';", completionHandler: nil)
        self.webview.evaluateJavaScript(javascript as String, completionHandler: nil)
    }
    func webView(webView: WKWebView, didCommitNavigation navigation: WKNavigation!) {

    }
    
}


//extension YoutubePlayerVC:WKScriptMessageHandler{
//    
//    func userContentController(userContentController: WKUserContentController, didReceiveScriptMessage message: WKScriptMessage) {
//        print("\(message.name)")
//        switch message.name {
//            
//        case "playerCommApiReceivedMessage":
//            //                print("message body: \(message.body)")
//            let json = JSON(message.body)
//        default:
//            print("no message body")
//        }
//    }
//}

extension YoutubePlayerVC:WKScriptMessageHandler{
    
    func userContentController(userContentController: WKUserContentController, didReceiveScriptMessage message: WKScriptMessage) {
//        print("WKScriptMessageHandler: \(message.name)")
        autoreleasepool {
            
            let json = JSON(message.body)
            switch message.name {
                
            case "playerCommApiReceivedMessage":
                let strObj = json[0]
                //                print("playerCommApiReceivedMessage from youtube to web called with data : \(strObj)")
                let cleanString = JsonHelper.replaceBadChar("\(strObj)")
                NativeToWebHelper.sharedInstance.playerCommApiReceivedMessage("'\(cleanString)'")
                
            default: break
                //                print("no message body")
            }
        }
    }
}

//extension WKWebView {
//    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
//        return nil
//    }
//}

//extension WKWebView {
//    
//    func setScrollEnabled(enabled: Bool) {
//        self.scrollView.scrollEnabled = enabled
//        self.scrollView.panGestureRecognizer.enabled = enabled
//        self.scrollView.bounces = enabled
//        
//        for subview in self.subviews {
//            if let subview = subview as? UIScrollView {
//                subview.scrollEnabled = enabled
//                subview.bounces = enabled
//                subview.panGestureRecognizer.enabled = enabled
//            }
//            
//            for subScrollView in subview.subviews {
//                if subScrollView.dynamicType == NSClassFromString("WKContentView")! {
//                    for gesture in subScrollView.gestureRecognizers! {
//                        subScrollView.removeGestureRecognizer(gesture)
//                    }
//                }
//            }
//        }
//    }
//}
