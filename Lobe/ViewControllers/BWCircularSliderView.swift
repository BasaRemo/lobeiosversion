//
//  BWCircularSliderView.swift
//  TB_CustomControlsSwift
//
//  Created by Yari D'areglia on 14/11/14.
//  Copyright (c) 2014 Yari D'areglia. All rights reserved.
//

import UIKit

@IBDesignable class BWCircularSliderView: UIView {
    
    @IBInspectable var startColor:UIColor = UIColor.redColor()
    @IBInspectable var endColor:UIColor = UIColor.blueColor()
     var slider:BWCircularSlider!

    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        // Build the slider
        slider = BWCircularSlider(startColor:self.startColor, endColor:self.endColor, frame: self.bounds)

        // Add the slider as subview of this view
        self.addSubview(slider)

    }

}
