//
//  LoginVC.swift
//  Lobe
//
//  Created by Professional on 2016-03-09.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit

class LoginVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let loginButton:FBSDKLoginButton = FBSDKLoginButton()
        // Optional: Place the button in the center of your view.
//        NSArray *permissionsArray = @[ @"email", @"public_profile"]
        loginButton.readPermissions = ["public_profile", "user_friends"]
        loginButton.delegate = self
        loginButton.center = self.view.center;
        self.view.addSubview(loginButton)
    }

}

extension LoginVC:FBSDKLoginButtonDelegate{
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
        
//        guard error == nil else {
//            return
//        }
//        guard result.isCancelled else {
//            return
//        }
        // If you ask for multiple permissions at once, you
        // should check if specific permissions missing
//        guard result.grantedPermissions.contains("name") else {
//            return
//        }
//        guard result.grantedPermissions.contains("email") else {
//            return
//        }
//        guard result.grantedPermissions.contains("public_profile") else {
//            return
//        }
        returnUserData()
        
    }
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
        
    }
    
    func returnUserData(){
        
        let params = ["fields": "id,name, email, friends"]
        let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: params)
        graphRequest.startWithCompletionHandler({ (connection, result, error) -> Void in
            
            if ((error) != nil)
            {
                // Process error
//                print("Error: \(error)")
            }
            else
            {
//                print("fetched user: \(result)")
                
                let userAccessToken = FBSDKAccessToken.currentAccessToken().tokenString
//                print("User access_token is: \(userAccessToken)")
                
                let userID : String = result.valueForKey("id") as! String
//                print("User userID is: \(userID)")
                
                let accessTokenExpirationDate = FBSDKAccessToken.currentAccessToken().expirationDate
//                print("User accessTokenExpirationDate is: \(accessTokenExpirationDate)")
                
                let accessTokenExpirationInSec = accessTokenExpirationDate.timeIntervalSince1970
//                print("User accessTokenExpirationDate in seconds: \(accessTokenExpirationInSec)")
                
                //Save in local
                LobeStateHelper.currentUser.facebookAuthData = FacebookAuthData(mAccessToken: userAccessToken, mUserID: userID, mExpirationDateInSec: Int64(accessTokenExpirationInSec))
                
                //Notify Web
                NativeToWebHelper.sharedInstance.loginWithFacebookUsingInfo(userAccessToken, mUserID: userID, mExpirationDateInSec: Int64(accessTokenExpirationInSec))
                
//                self.setRootViewController()
                self.dismissViewControllerAnimated(true, completion: nil)
                
            }
        })
    }
    
    func setRootViewController(){

        let startViewController: UIViewController
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        startViewController = storyboard.instantiateViewControllerWithIdentifier("SideMenuController") as! SideMenuController
        self.presentViewController(startViewController, animated: true, completion: nil)
//        let window = UIWindow(frame: UIScreen.mainScreen().bounds)
//        window.rootViewController = startViewController;
//        window.makeKeyAndVisible()
    }
}