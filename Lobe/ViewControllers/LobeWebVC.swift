//
//  LobeWebVC.swift
//  Lobe
//
//  Created by Professional on 2015-12-08.
//  Copyright © 2015 Ntambwa. All rights reserved.
//

import UIKit
import WebKit
import SwiftyJSON
import ZFDragableModalTransition
import FBSDKCoreKit
import FBSDKLoginKit
import Cartography

class LobeWebVC: CDVViewController {

    @IBOutlet var magicButton: UIButton!
    @IBOutlet var menuButton:UIButton!
    var loginButton:FBSDKLoginButton?
    var menuAnimator:ZFModalTransitionAnimator!
    override func prefersStatusBarHidden() -> Bool {
        return true;
    }
    
    func updateState(){
//        print("WebView Update")
    }
    
    func userStateChanged(){
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.autoresizesSubviews = true
//        self.mWebView!.navigationDelegate = self
        
        var tempFrame = self.view.bounds
        tempFrame.origin.y = 22
        tempFrame.size.height = self.view.frame.size.height
        self.webView.frame = tempFrame
        self.webView.allowsInlineMediaPlayback = true
        self.webView.scrollView.delegate = self
        NativeToWebHelper.sharedInstance.delegate = self
        
        HybridBridge.sharedInstance = HybridBridge(webView: self.webView)
        HybridBridge.sharedInstance.genericJavascriptCall("receivedNativeMessage", params: "")
        
//        constrain(self.webView) { webView in
//            
//            webView.top  == webView.superview!.top + 22
//            webView.bottom  == webView.superview!.bottom - 87
//            webView.right == webView.superview!.right
//            webView.left == webView.superview!.left
//        }
        
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
    }

    @IBAction func menuButtonPressed(sender: UIButton) {
//        print("toggle menu")
        self.sideMenuController()?.menuDelegate = self.parentViewController as! MainVC
        self.sideMenuController()?.toggleSidePanel()
    }
    
    @IBAction func closeBtnPressed(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func shouldAutorotate() -> Bool {
        return false
    }
    
    override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
        return UIInterfaceOrientation.Portrait
    }
    
}


extension LobeWebVC:WKNavigationDelegate{
    
    func webView(webView: WKWebView, didFinishNavigation navigation: WKNavigation!) {
    }
    override func webViewDidStartLoad(webView: UIWebView) {
//        print("Start Loading social webview")
    }
    override func webViewDidFinishLoad(webView: UIWebView) {
//        print("webViewDidFinishLoad social webview")
    }
    override func webView(webView: UIWebView, didFailLoadWithError error: NSError?) {
//        print("didFailLoadWithError social webview: \(error)")
    }
}

extension LobeWebVC:UIScrollViewDelegate{
    
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
        return nil
    }
}
extension LobeWebVC:NativeToWebHelperDelegate{
    
    func genericFunctionCall(functionName:String, params:String){
        
        HybridBridge.sharedInstance.genericJavascriptCall(functionName,params: params)
    }
    func genericLoginWithFacebook(){
        loginWithFacebook()
    }
    
    func changeMenuButtonIcon(showBurgerBtn showBurgerBtn:Bool){
        if showBurgerBtn {
            menuButton.hidden = false
            menuButton.setImage(UIImage(named: "menu_filled"), forState: UIControlState.Normal)
        }else{
            menuButton.hidden = true
//            menuButton.setImage(UIImage(named: "back_button"), forState: UIControlState.Normal)
        }
    }
}

extension LobeWebVC:FBSDKLoginButtonDelegate{
    
    func loginWithFacebook(){
        
        let fbLoginManager = FBSDKLoginManager()
//        fbLoginManager.logOut()
        
        fbLoginManager.loginBehavior = FBSDKLoginBehavior.Browser
        let readPermissions = ["public_profile", "user_friends"]
        
        fbLoginManager.logInWithReadPermissions(readPermissions, fromViewController: nil) { (result: FBSDKLoginManagerLoginResult!, error: NSError!) -> Void in
            self.returnUserData()
        }
    }

    @objc func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
        
        //        guard error == nil else {
        //            return
        //        }
        //        guard result.isCancelled else {
        //            return
        //        }
        // If you ask for multiple permissions at once, you
        // should check if specific permissions missing
        //        guard result.grantedPermissions.contains("name") else {
        //            return
        //        }
        //        guard result.grantedPermissions.contains("email") else {
        //            return
        //        }
        //        guard result.grantedPermissions.contains("public_profile") else {
        //            return
        //        }
        returnUserData()
        
    }
    @objc func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
        
    }
    
    func returnUserData(){
        
        let params = ["fields": "id,name, friends"]
        let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: params)
        graphRequest.startWithCompletionHandler({ (connection, result, error) -> Void in
            
            if ((error) != nil)
            {
                // Process error
//                print("Error: \(error)")
                let fbLoginManager = FBSDKLoginManager()
                fbLoginManager.logOut()
            }
            else
            {
                //                print("fetched user: \(result)")
                
                let userAccessToken = FBSDKAccessToken.currentAccessToken().tokenString
//                print("User access_token is: \(userAccessToken)")
                
                let userID : String = result.valueForKey("id") as! String
//                print("User userID is: \(userID)")
                
                let accessTokenExpirationDate = FBSDKAccessToken.currentAccessToken().expirationDate
//                print("User accessTokenExpirationDate is: \(accessTokenExpirationDate)")
                
                let accessTokenExpirationInSec = accessTokenExpirationDate.timeIntervalSince1970
//                print("User accessTokenExpirationDate in seconds: \(accessTokenExpirationInSec)")
                
                //Save in local
                LobeStateHelper.currentUser.facebookAuthData = FacebookAuthData(mAccessToken: userAccessToken, mUserID: userID, mExpirationDateInSec: Int64(accessTokenExpirationInSec))
                //Notify Web
                NativeToWebHelper.sharedInstance.loginWithFacebookUsingInfo(userAccessToken, mUserID: userID, mExpirationDateInSec: Int64(accessTokenExpirationInSec))
                
                //                self.setRootViewController()
//                self.dismissViewControllerAnimated(true, completion: nil)
//                self.loginButton!.hidden = true
                
                FBSDKAccessToken.setCurrentAccessToken(nil)
                FBSDKProfile.setCurrentProfile(nil)
                
//                let deletepermission = FBSDKGraphRequest(graphPath: "me/permissions/", parameters: nil, HTTPMethod: "DELETE")
//                deletepermission.startWithCompletionHandler({(connection,result,error)-> Void in
//                    print("the delete permission is \(result)")
//                })
                
                let fbLoginManager = FBSDKLoginManager()
                fbLoginManager.logOut()
                
//                let storage:NSHTTPCookieStorage = NSHTTPCookieStorage.sharedHTTPCookieStorage()
//                for cookie in storage.cookies!
//                {
//                    let domainName:NSString = cookie.domain;
//                    let domainRange:NSRange = domainName.rangeOfString("facebook")
//                    if(domainRange.length > 0)
//                    {
//                        storage.deleteCookie(cookie)
//                    }
//                }
//                
//                let facebookCookies:[NSHTTPCookie] = storage.cookiesForURL(NSURL(string: "http://login.facebook.com")!)!
//                for  cookie:NSHTTPCookie in facebookCookies {
//                    storage.deleteCookie(cookie)
//                }
                
            }
        })
    }
}

extension LobeWebVC {
    
    func resizeWebView(enlarge enlarge:Bool){
    
        var tempFrame = self.view.bounds
        tempFrame.origin.y = 22
        if (enlarge){
            tempFrame.size.height = self.view.frame.size.height
        }else{
            tempFrame.size.height = self.view.frame.size.height - 87
        }
        
        self.webView.frame = tempFrame
    }
}

