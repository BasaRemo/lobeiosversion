//
//  BWCircularSlider.swift
//  TB_CustomControlsSwift
//
//  Created by Yari D'areglia on 03/11/14.
//  Copyright (c) 2014 Yari D'areglia. All rights reserved.
//

import UIKit

struct Config {
    
    static let TB_SLIDER_SIZE:CGFloat = UIScreen.mainScreen().bounds.size.width
    static let TB_SAFEAREA_PADDING:CGFloat = 15.0
    static let TB_LINE_WIDTH:CGFloat = 3.0
    static let HANDLE_WIDTH:CGFloat = 15.0
    static let TB_FONTSIZE:CGFloat = 10.0
    
}


// MARK: Math Helpers 

func DegreesToRadians (value:Double) -> Double {
    return value * M_PI / 180.0
}

func RadiansToDegrees (value:Double) -> Double {
    return value * 180.0 / M_PI
}

func Square (value:CGFloat) -> CGFloat {
    return value * value
}


// MARK: Circular Slider

class BWCircularSlider: UIControl {

    var radius:CGFloat = 0
    var angle:Int = 360
    var startColor = UIColor.appThemeColorBackgroundThirdOrange()
    var endColor = UIColor.appThemeColorBackgroundThirdOrange()
    var trueCenter:CGPoint = CGPointMake(0, 0)
    var handleCenter:CGPoint = CGPointMake(3, 3)
    // Custom initializer
    convenience init(startColor:UIColor, endColor:UIColor, frame:CGRect){
        self.init(frame: frame)
        
        self.startColor = startColor
        self.endColor = endColor
    }
    
    // Default initializer
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.clearColor()
        self.opaque = true
        
        //Define the circle radius taking into account the safe area
        radius = self.frame.size.width/2 - Config.TB_SAFEAREA_PADDING
        trueCenter = CGPointMake(self.frame.size.width/2, self.frame.size.height/2)
        
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func beginTrackingWithTouch(touch: UITouch, withEvent event: UIEvent?) -> Bool {
        super.beginTrackingWithTouch(touch, withEvent: event)
        
        return true
    }
    
    
    override func continueTrackingWithTouch(touch: UITouch, withEvent event: UIEvent?) -> Bool {
        super.continueTrackingWithTouch(touch, withEvent: event)
        
        let lastPoint = touch.locationInView(self)
        
        self.moveHandle(lastPoint)
        
        self.sendActionsForControlEvents(UIControlEvents.ValueChanged)
        
        return true
    }
    
    override func endTrackingWithTouch(touch: UITouch?, withEvent event: UIEvent?) {
        super.endTrackingWithTouch(touch, withEvent: event)
    }
    
    
    
    
    //Use the draw rect to draw the Background, the Circle and the Handle
    override func drawRect(rect: CGRect){
        super.drawRect(rect)
        
        let ctx = UIGraphicsGetCurrentContext()
        
        CGContextAddArc(ctx, CGFloat(self.frame.size.width/2)  , CGFloat(self.frame.size.height/2), radius, 0, CGFloat(DegreesToRadians(Double(-angle))) , 0);
        //define the path
        CGContextSetLineWidth(ctx, 5)
        CGContextSetStrokeColorWithColor(ctx, UIColor.appThemeColorBackgroundThirdOrange().CGColor)
        CGContextDrawPath(ctx, CGPathDrawingMode.Stroke)
        
        /* Draw the handle */
        drawTheHandle(ctx!)

    }
    
    
    
    /** Draw a white knob over the circle **/
    
    func drawTheHandle(ctx:CGContextRef){
        
        CGContextSaveGState(ctx);

        //Get the handle position
        handleCenter = pointFromAngle(angle)
        //Draw It!
        CGContextSetFillColorWithColor(ctx, UIColor.appThemeColorBackgroundThirdOrange().CGColor)
        CGContextFillEllipseInRect(ctx, CGRectMake(handleCenter.x + 2 , handleCenter.y + 4, 22, 22));
        
        CGContextRestoreGState(ctx);
    }
    
    
    
    /** Move the Handle **/

    func moveHandle(lastPoint:CGPoint){
        
        //Get the center
        let centerPoint:CGPoint  = CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
        //Calculate the direction from a center point and a arbitrary position.
        let currentAngle:Double = AngleFromNorth(centerPoint, p2: lastPoint, flipped: false);
        let angleInt = Int(floor(currentAngle))

        //Store the new angle
        angle = Int(360 - angleInt)
        
        //Redraw
        setNeedsDisplay()
    }
    
    /** Given the angle, get the point position on circumference **/
    func pointFromAngle(angleInt:Int)->CGPoint{
    
        //Circle center
        let centerPoint = CGPointMake(self.frame.size.width/2.0 - Config.TB_SAFEAREA_PADDING, self.frame.size.height/2.0 - Config.TB_SAFEAREA_PADDING);

        //The point position on the circumference
        var result:CGPoint = CGPointZero
        let y = round(Double(radius) * sin(DegreesToRadians(Double(-angleInt)))) + Double(centerPoint.y)
        let x = round(Double(radius) * cos(DegreesToRadians(Double(-angleInt)))) + Double(centerPoint.x)
        result.y = CGFloat(y)
        result.x = CGFloat(x)
            
        return result;
    }
    
    
    //Sourcecode from Apple example clockControl
    //Calculate the direction in degrees from a center point to an arbitrary position.
    func AngleFromNorth(p1:CGPoint , p2:CGPoint , flipped:Bool) -> Double {
        var v:CGPoint  = CGPointMake(p2.x - p1.x, p2.y - p1.y)
        let vmag:CGFloat = Square(Square(v.x) + Square(v.y))
        var result:Double = 0.0
        v.x /= vmag;
        v.y /= vmag;
        let radians = Double(atan2(v.y,v.x))
        result = RadiansToDegrees(radians)
        return (result >= 0  ? result : result + 360.0);
    }

}
