//
//  ArtistVC.swift
//  Lobe
//
//  Created by Professional on 2016-03-01.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

import UIKit

class ArtistVC: UIViewController {

    @IBOutlet var tableView: UITableView!
    
    var albums = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.separatorStyle = .None
        let albumsQuery = MPMediaQuery.artistsQuery()
        albums = albumsQuery.collections!
        
    }
}

extension ArtistVC: UITableViewDataSource {
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("AlbumCell", forIndexPath: indexPath) as! AlbumCell
        
        let rowItem:MPMediaItemCollection = albums[indexPath.row] as! MPMediaItemCollection
        cell.artistCollection = rowItem
        
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return albums.count ?? 0
    }
    
}

extension ArtistVC: UITableViewDelegate {
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.reloadData()
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }
}
