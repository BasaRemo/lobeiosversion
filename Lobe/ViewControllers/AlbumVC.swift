//
//  AlbumVC.swift
//  Lobe
//
//  Created by Professional on 2015-11-17.
//  Copyright © 2015 Ntambwa. All rights reserved.
//

import UIKit
import Foundation
import MediaPlayer
import SwiftyJSON
import Alamofire

class AlbumVC: UIViewController {

    @IBOutlet var tableView: UITableView!

    var albums = []
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.separatorStyle = .None
        let albumsQuery = MPMediaQuery.albumsQuery()
        albums = albumsQuery.collections!
        
    }

}

extension AlbumVC: UITableViewDataSource {
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("AlbumCell", forIndexPath: indexPath) as! AlbumCell
        
        let rowItem:MPMediaItemCollection = albums[indexPath.row] as! MPMediaItemCollection
        cell.albumCollection = rowItem
    
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return albums.count ?? 0
    }
    
}

extension AlbumVC: UITableViewDelegate {
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.reloadData()
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }
}
