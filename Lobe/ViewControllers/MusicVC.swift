//
//  MusicVC.swift
//  Lobe
//
//  Created by Professional on 2015-11-17.
//  Copyright © 2015 Ntambwa. All rights reserved.
//

import UIKit
import Foundation
import MediaPlayer
import SwiftyJSON
import Alamofire
import SABlurImageView
//import BLKFlexibleHeightBar
//import StreamingKit
import MGSwipeTableCell
//import JLToast
import Bond
//import CustonUploadAudioPlayerVC


//Create the delegate protocol to add a mediaItem to a lobeList
protocol LobeListDelegate {
    func addMusicItemToLobeList(musicItem: SongItem)
    func addMusicTrackToLobeList(musicItem: SongItem)
    func removeMusicItemFromLobeList(musicItem: SongItem)
    func lobeListContains(musicItem: SongItem) -> Bool
    func removeMusicItemFromBoth(musicItem: MPMediaItem)
}

class MusicVC: UIViewController {
    
    @IBOutlet var headerView: SABlurImageView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var songArtWorkImageView:RounderCornerImageView!
//    @IBOutlet var blkMenuBar:BLKFlexibleHeightBar!
    @IBOutlet var noSongsLabel:UILabel!
    
    var mediaItems = [MPMediaItem]()
    var filteredMediaItems = [MPMediaItem]()
    
    var filteredSongItems = [SongItem]()
    var collection: MPMediaItemCollection?
    
    var indexOfCurrentPlayingMusic: NSIndexPath = NSIndexPath(forItem: 0, inSection: 0)
    var nowPlayingSongRow = 0
    
    var lobeListDelegate: LobeListDelegate?
//    var delegateSplitter: BLKDelegateSplitter?
    
    var inSearchMode = false
//    var bar:BLKFlexibleHeightBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.tableView.separatorStyle = .None
        self.observeCurrentSongChange()
        dataSourceObservable()
        self.applyBlurEffect()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "trackToRemove:", name:"trackToRemoveNotification", object: nil)
        //MARK: - Placeholder Image
        let width = Int(CGRectGetWidth(self.view.bounds))
        let heigth = Int(CGRectGetHeight(self.view.bounds))
        let croppedImage = DownloadHelper.cropImage( UIImage(named: placeholderImageName)!, withWidth: width , andHeigth:heigth)
//        self.headerView.backgroundColor = UIColor(patternImage: croppedImage)
        self.songArtWorkImageView.image = croppedImage
        
        guard /*!inSearchMode &&*/ AUDIO_OUT_MANAGER.initialLoading  else {
            return
        }
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        if AUDIO_OUT_MANAGER.localObservableTrackList.count > 0 {
            noSongsLabel.hidden = true
        }else{
            noSongsLabel.hidden = false
        }
    }
    
    func selectItem(atRow row:Int, andSection section:Int){
        
        let indexPath = NSIndexPath(forRow: row, inSection: section)
        self.indexOfCurrentPlayingMusic = indexPath
        self.nowPlayingSongRow = indexPath.row
//        self.tableView.selectRowAtIndexPath(indexPath, animated: false, scrollPosition: UITableViewScrollPosition.None)
//        self.tableView(self.tableView, didSelectRowAtIndexPath: indexPath)
        
    }
    
    func updateState(){
//        print("Music Update")
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
    }
    
    
    func applyBlurEffect(){
        
//        let darkBlur = UIBlurEffect(style: UIBlurEffectStyle.Dark)
//        let blurView = UIVisualEffectView(effect: darkBlur)
//        blurView.frame = headerView.bounds
//        headerView.addSubview(blurView)
    }
    
    func userStateChanged(){
    }
    
    override func shouldAutorotate() -> Bool {
        return false
    }
    
    override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
        return UIInterfaceOrientation.Portrait
    }
    
}

//==========================================================================================
// MARK: - Observers
//==========================================================================================
extension MusicVC {
    
    func observeCurrentSongChange(){
        
        //Observe current song
        AUDIO_OUT_MANAGER.currentTrackObservable.observe{ currentTrack in
            
            currentTrack.trackInfo.songAlbumArt.observe{ image in
                if let albumImage = image {
                
                    let croppedImage = DownloadHelper.cropImage(albumImage, withWidth: Int(CGRectGetWidth(self.view.bounds)) , andHeigth:Int(CGRectGetHeight(self.view.bounds)))
                    dispatch_async(dispatch_get_main_queue()) {
                        self.songArtWorkImageView.image = croppedImage
                    }
                }
            }
            dispatch_async(dispatch_get_main_queue()) {
                self.tableView.reloadData()
            }
        }
        
        //Observe LobeList
        AUDIO_OUT_MANAGER.lobeListObservableList.observe{ newList in
            dispatch_async(dispatch_get_main_queue()) {
                self.tableView.reloadData()
            }
        }
    }
}


//==========================================================================================
// MARK: - UITableViewDataSource Observable
//==========================================================================================
extension MusicVC {
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("MusicCell", forIndexPath: indexPath) as! MusicCell
        
        let songItem:SongItem!
        if inSearchMode{
            songItem = filteredSongItems[indexPath.row]
        }else{
            songItem = AUDIO_OUT_MANAGER.localObservableTrackList[indexPath.row]
        }
        
        cell.songItem = songItem
        //Hide reoder button
        switch LobeStateHelper.sharedInstance.state {
        case .Local,.DJ,.Participant:
            self.tableView.setEditing(false, animated: false)
        }
        
        let lockQueue = dispatch_queue_create("lobeList.LockQueue", nil)
        dispatch_sync(lockQueue) {
            // code
            cell.onPushTrackToLobeList = { [unowned self] (MusicCell) -> Void in
                self.handlePushTrack(songItem,index: indexPath.section)
                
            }
        }
        
        return cell
    }
    
    func dataSourceObservable(){
        
        AUDIO_OUT_MANAGER.localObservableTrackList.observe{ newList in
            dispatch_async(dispatch_get_main_queue()) {
                self.tableView.reloadData()
            }
        }
    }
 
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if inSearchMode{
//            print("inSearchMode")
            return filteredSongItems.count ?? 0
        }else{
            return AUDIO_OUT_MANAGER.localObservableTrackList.count ?? 0
        }
        
    }
    
    func currentTimeMillis() -> Int64{
        let nowDouble = NSDate().timeIntervalSince1970
        return Int64(nowDouble*1000)
    }
    
    func handlePushTrack(songItem:SongItem,index:Int){
        
        //Set the trackInfo with the right infos
        songItem.trackInfo.mUser = LobeStateHelper.currentUser
        songItem.trackInfo.mIsYoutubeVideo = false
        songItem.trackInfo.mUserOwnerName = LobeStateHelper.currentUser.userName.value!
        songItem.trackInfo.mUserOwnerUid = LobeStateHelper.currentUser.parseID
        songItem.trackInfo.mUniqueDeviceId = unique_device_id
        songItem.trackInfo.mUnixTimeStampAtUserPush = "\(currentTimeMillis())"
        
        //TODO: - Get the format desc in trackInfo
        guard let url = songItem.music!.valueForProperty(MPMediaItemPropertyAssetURL) as? NSURL else {
//            print("AssetUrl unavailable this track is DRM protected")
            return
        }
        let songAsset = AVURLAsset(URL: url)
        let trackAsset = songAsset.tracks[0] as AVAssetTrack
        let descriptions = trackAsset.formatDescriptions
        let audioFormstDesc = descriptions[0] as! CMAudioFormatDescription

//        let descJSON = JSON(audioFormstDesc)
        let descString = "streamBasicDescription: \(audioFormstDesc)"
        let descStringArray = descString.componentsSeparatedByString("]> ")
        let descJSON = descStringArray[1]
//        print("descJSON: \(descJSON)")
        
        let mFormatIDArrayLong = descJSON.componentsSeparatedByString("mFormatID: ")
        let mFormatIDArray = mFormatIDArrayLong[1].componentsSeparatedByString("mFormatFlags")
        let mFormatID = mFormatIDArray[0]
//        print("mFormatID: \(mFormatID)")
        
        let mSampleRateArrayLong = descJSON.componentsSeparatedByString("mSampleRate: ")
        let mSampleRateArray = mSampleRateArrayLong[1].componentsSeparatedByString("mFormatID")
        let mSampleRate = mSampleRateArray[0]
        let integerSampleRateString = mSampleRate.componentsSeparatedByString(".")[0]
//        print("mSampleRate: \(mSampleRate)")
//        print("integerSampleRateString: \(Int(integerSampleRateString)!)")
        
        let mChannelsPerFrameArrayLong = descJSON.componentsSeparatedByString("mChannelsPerFrame: ")
        let mChannelsPerFrameArray = mChannelsPerFrameArrayLong[1].componentsSeparatedByString("mBitsPerChannel")
        let mChannelsPerFrame = mChannelsPerFrameArray[0]
        let integerMChannelsPerFrame = mChannelsPerFrame.componentsSeparatedByString(" \n")[0]
//        print("mChannelsPerFrame: \(mChannelsPerFrame)")
//        print("integerMChannelsPerFrame: \(Int(integerMChannelsPerFrame)!)")
        
        songItem.trackInfo.mAudioNbChannels = Int(integerMChannelsPerFrame)!
        songItem.trackInfo.mAudioSampleRate = Int(integerSampleRateString)!
        songItem.trackInfo.mAudioBitRate = 16
        
//        let message = "streamBasicDescription: \(streamBasicDescription)"
//        JLToast.makeText(message).show()
        
        switch LobeStateHelper.sharedInstance.state {
            
        case .Local:
            if isValidFileType(songItem.music!){
                if (!AUDIO_OUT_MANAGER.lobeListContains(songItem)){
                     AUDIO_OUT_MANAGER.lobeListSongItems.addObject(songItem)
                    AUDIO_OUT_MANAGER.lobeListObservableList.replaceRange(0...0, with: [SongItem()])
                }else{
                    AUDIO_OUT_MANAGER.removeSongInLobelist(songItem)
                    AUDIO_OUT_MANAGER.lobeListObservableList.replaceRange(0...0, with: [SongItem()])
                }
            }
            tableView.reloadData()
        case .Participant,.DJ:
            
            if isValidFileType(songItem.music!){
            
                if (!AUDIO_OUT_MANAGER.lobeListContains(songItem)){
                    NativeToWebHelper.sharedInstance.pushSingleSuggestionItem(songItem.trackInfo)
                }else{
                    NativeToWebHelper.sharedInstance.deleteTrackFromLobeList(songItem.trackInfo.mID)
                    
                }
            }
        }
    }

}


func isValidFileType(mediaItem:MPMediaItem) -> Bool{
    
    if let localPath = mediaItem.valueForProperty(MPMediaItemPropertyAssetURL) as? NSURL{
        
        let fileType:String = localPath.pathExtension!
//        print("file type: \(fileType)")
        if fileType == "mp3" {
            return true
        }
        let message = "This file type: \(fileType) is not supported at the moment"
        WebToNativeHelper.sharedInstance.showToastMessage(message)
    }

    return false

}

//==========================================================================================
// MARK: - UITableViewDelegate
//==========================================================================================
extension MusicVC: UITableViewDelegate {
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
//        print("did select")
        if AUDIO_OUT_MANAGER.musicPlayerState != .Library{
            AUDIO_OUT_MANAGER.musicPlayerState = .Library
        }
        
        switch LobeStateHelper.sharedInstance.state {
            
            case .Local:
                var songItem =  AUDIO_OUT_MANAGER.localObservableTrackList[indexPath.row]
                if inSearchMode{
                    songItem = filteredSongItems[indexPath.row]
                }
                AUDIO_OUT_MANAGER.currentSong = songItem
//                AUDIO_OUT_MANAGER.currentLocalSongIndex = indexPath.row
            case .Participant,.DJ:
                let message = "Can't play the song from library in group. Queue the song in the playlist to listen to it."
                print("\(message)")
                WebToNativeHelper.sharedInstance.showToastMessage(message)
        }
    
        AUDIO_OUT_MANAGER.directPlayMusic()
        tableView.reloadData()
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }
}

//==========================================================================================
//MARK: - Search Mode
//==========================================================================================

extension MusicVC:SearchVCDelegate{
    
    func searchSongWithText(text:String){
//        print("SEARCHING!!")
        guard AUDIO_OUT_MANAGER.localObservableTrackList.count > 0 else {
//            print("searchSongWithText: localObservableTrackList is empty..")
            return
        }
        inSearchMode = true
        let filteredSongs:[SongItem] = AUDIO_OUT_MANAGER.localObservableTrackList.filter {
            let songName = $0.trackInfo.mName
//            let artistName = $0.trackInfo.mArtist
            let options = NSStringCompareOptions.CaseInsensitiveSearch
            return songName.rangeOfString(text, options: options) != nil
        }
        guard filteredSongs.count > 0 else {
//            print("searchSongWithText: filteredSongs is empty.")
            self.filteredSongItems.removeAll()
            self.tableView.reloadData()
            return
        }
        self.filteredSongItems = filteredSongs
        self.tableView.reloadData()
    }
    func returnToDefault(){
        inSearchMode = false
        self.tableView.reloadData()
    }
    func searchActivated(isActivated: Bool) {
        inSearchMode = isActivated
        self.tableView.reloadData()
    }
    
    
    func searchButtonTapped(){
        searchActivated(!inSearchMode)
    }
}

// MARK: - Searchbar Delegate
//extension MusicVC: UISearchBarDelegate {
//    
//    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
//        searchBar.setShowsCancelButton(true, animated: true)
//        searchActivated(true)
//    }
//    
//    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
//        searchBar.resignFirstResponder()
//        searchBar.text = ""
//        searchBar.setShowsCancelButton(false, animated: true)
//        searchActivated(false)
//    }
//    
//    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
//        searchSongWithText(searchText)
//    }
//}

public struct MyString {
    public static func contains(text: String, substring: String,
        ignoreCase: Bool = false,
        ignoreDiacritic: Bool = false) -> Bool {
            
            if substring == "" { return true }
            var options = NSStringCompareOptions.CaseInsensitiveSearch
            
            
            return text.rangeOfString(substring, options: options) != nil
    }
}
