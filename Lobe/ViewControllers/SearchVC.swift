//
//  SearchVC.swift
//  Lobe
//
//  Created by Professional on 2016-01-24.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

import UIKit

protocol SearchVCDelegate {
    func searchSongWithText(text:String)
    func returnToDefault()
    func searchActivated(isActivated:Bool)
}

class SearchVC: UIViewController {

    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var containerView:UIView!
    var delegate:SearchVCDelegate?
    var musicVC:MusicVC!
    
    // this view can be in two different states
    enum State {
        case DefaultMode
        case SearchMode
    }
    
    // whenever the state changes, perform one of the two queries and update the list
    var state: State = .DefaultMode {
        didSet {
            switch (state) {
            case .DefaultMode:
                delegate?.searchActivated(false)
                delegate?.returnToDefault()
                searchBar.resignFirstResponder() // 3
                searchBar.text = ""
                searchBar.showsCancelButton = false
                
            case .SearchMode:
                delegate?.searchActivated(true)
                searchBar.setShowsCancelButton(true, animated: true) 
                let searchText = searchBar?.text ?? ""
                delegate?.searchSongWithText(searchText)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        self.musicVC = self.childViewControllers.first! as! MusicVC
        self.delegate = musicVC
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        searchBar.becomeFirstResponder()
        state = .SearchMode
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        search_animator.setContentScrollView(self.musicVC?.tableView)
    }
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(true)
        searchBar.resignFirstResponder()
//        state = .DefaultMode
    }
    
    override func shouldAutorotate() -> Bool {
        return false
    }
    
    override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
        return UIInterfaceOrientation.Portrait
    }
    
}

//==========================================================================================
// MARK:- Segue
//==========================================================================================
extension SearchVC {
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if let vc = segue.destinationViewController as? MusicVC
            where segue.identifier == "MusicVC_segue" {
                if self.musicVC == nil {
                    self.musicVC = vc
                    self.delegate = self.musicVC 
                }
        }
    }
}

// MARK: Searchbar Delegate

extension SearchVC: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
        state = .SearchMode
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.text = ""
        searchBar.setShowsCancelButton(false, animated: true)
        state = .DefaultMode
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        delegate?.searchSongWithText(searchText)
    }
}