
import Foundation
import SwiftyJSON
import CoreLocation
//import RNActivityView
import JGProgressHUD

 @objc(HybridBridge) class HybridBridge : CDVPlugin {
    
    static var sharedInstance = HybridBridge()
    let appDelegateInstance = UIApplication.sharedApplication().delegate as? AppDelegate
    let HUD:JGProgressHUD = JGProgressHUD(style: .Dark)
    
    var mGenericJvascriptCallbackId:String?
    
    //** From Native to Javascript **//
    func genericJavascriptCall(functionName:String, params:String){
//        print("Native Calling JS: \(functionName)")
        let jsFunction = "\(functionName)(\(params))"
        
        dispatch_async(dispatch_get_main_queue()) {
            self.webView?.stringByEvaluatingJavaScriptFromString(jsFunction)
        }
    }
    
    //** From Javascript to Native **//
    func getDeviceLocation(command: CDVInvokedUrlCommand) {
        self.commandDelegate?.runInBackground({ () -> Void in
//            let json = JSON(command.arguments)
            
            LocationHelper.sharedInstance.updateLocation()
            let latitude: CLLocationDegrees = LocationHelper.sharedInstance.location_latitude
            let longitude: CLLocationDegrees = LocationHelper.sharedInstance.location_longitude
            
//            let functionName:String = "javaCallDeviceLocationCallback";
//            self.genericJavascriptCall(functionName, params: "'\(latitude)','\(longitude)'")
            let dict = ["longitude":longitude,"latitude":latitude]
            let jsonDict = JSON(dict)
            let pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAsString: jsonDict.rawString()!)
            self.commandDelegate!.sendPluginResult(pluginResult, callbackId:command.callbackId)

            LocationHelper.sharedInstance.stopMonitoring()

        })
    }
    
    func testWebToNativeCall(command: CDVInvokedUrlCommand) {
        self.commandDelegate?.runInBackground({ () -> Void in
//            let message = command.arguments[0] as! Bool
            let json = JSON(command.arguments)
//            print("webcall params: \(json)")
//            WebToNativeHelper.sharedInstance.playNotifSound(json[0].string!)
            
            let pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAsString: "Hello ")
            self.commandDelegate!.sendPluginResult(pluginResult, callbackId:command.callbackId)
        })
    }
    
    func sendSwiftMessage(command: CDVInvokedUrlCommand) {
        self.commandDelegate?.runInBackground({ () -> Void in
            let message = command.arguments[0] as! String
            let json = JSON(command.arguments)
//            print("webcall: \(json)")
            
            let pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAsString: "Hello \(message)")
            self.commandDelegate!.sendPluginResult(pluginResult, callbackId:command.callbackId)
        })

    }
    
    func goToViewFromBackAction(command: CDVInvokedUrlCommand) {
        self.commandDelegate?.runInBackground({ () -> Void in
            let json = JSON(command.arguments)
//            print("webcall goToViewFromBackAction: \(json)")
            WebToNativeHelper.sharedInstance.goToViewFromBackAction(json[0].string!)
            WebToNativeHelper.sharedInstance.playNotifSound("")
        })
    }
    
    func testCall(command: CDVInvokedUrlCommand) {
        self.commandDelegate?.runInBackground({ () -> Void in
            let json = JSON(command.arguments)
//            print("webcall: \(json)")
            WebToNativeHelper.sharedInstance.testCall(json[0].string!)
        })
    }
    
    func showToastMessage(command: CDVInvokedUrlCommand) {
        self.commandDelegate?.runInBackground({ () -> Void in
            let json = JSON(command.arguments)
            WebToNativeHelper.sharedInstance.showToastMessage(json[0].string!)
        })
    }
    
    func toJavaCallSetAudioSourceMode(command: CDVInvokedUrlCommand) {
        self.commandDelegate?.runInBackground({ () -> Void in
            let json = JSON(command.arguments)
            self.mGenericJvascriptCallbackId = command.callbackId
            WebToNativeHelper.sharedInstance.toJavaCallSetAudioSourceMode(json[0].string!)
        })
    }
    
    func playNotifSound(command: CDVInvokedUrlCommand) {
        self.commandDelegate?.runInBackground({ () -> Void in
            let json = JSON(command.arguments)
            WebToNativeHelper.sharedInstance.playNotifSound(json[0].string!)
        })
    }
    
    func setIsSignedInState(command: CDVInvokedUrlCommand) {
        let json = JSON(command.arguments)
//        print("webcall: \(json)")
        WebToNativeHelper.sharedInstance.setIsSignedInState(json[0].bool!)
    }
    
    func setMyPersonalInfo(command: CDVInvokedUrlCommand) {
        let json = JSON(command.arguments)
//        print("webcall setMyPersonalInfo: \(json)")
        WebToNativeHelper.sharedInstance.setMyPersonalInfo(json[0].string!, name: json[1].string!, picUrl: json[2].string!)
    }
    
    func setLobeList(command: CDVInvokedUrlCommand) {
        let json = JSON(command.arguments)
//        print("webcall: \(json)")
        WebToNativeHelper.sharedInstance.setLobeList(json)
    }
    
    func setSuggestionList(command: CDVInvokedUrlCommand) {
        let json = JSON(command.arguments)
//        print("webcall: \(json)")
        WebToNativeHelper.sharedInstance.setSuggestionList(json)
    }
    
    func backToNormal(command: CDVInvokedUrlCommand) {
        self.commandDelegate?.runInBackground({ () -> Void in
            WebToNativeHelper.sharedInstance.backToNormal()
        })
    }
    
    func doSynchoFutureEventTest(command: CDVInvokedUrlCommand) {
        
    }
    
    func resumeMusic(command: CDVInvokedUrlCommand) {
        self.commandDelegate?.runInBackground({ () -> Void in
            WebToNativeHelper.sharedInstance.resumeMusic()
        })
    }
    
    func pauseMusic(command: CDVInvokedUrlCommand) {
        self.commandDelegate?.runInBackground({ () -> Void in
            WebToNativeHelper.sharedInstance.pauseMusic()
        })
    }
    
    func setTimeDiffWithFirebase(command: CDVInvokedUrlCommand) {

        let json = JSON(command.arguments)
        let jsCurrUnixTime:Int64 = json[0].int64Value
        let deltaF_JS:Int64 = json[1].int64Value
        WebToNativeHelper.sharedInstance.setTimeDiffWithFirebase(jsCurrUnixTime, deltaF_JS: deltaF_JS)
//        self.commandDelegate?.runInBackground({ () -> Void in
//            
//        })
    }
    
    func startStreamingFromUrl(command: CDVInvokedUrlCommand) {
        
        self.commandDelegate?.runInBackground({ () -> Void in
            let json = JSON(command.arguments)
            //            print("webcall startStreamingFromUrl: \(json)")
            let time_stamp_String:Int64 = json[1].int64Value
            let music_start_time_delay_String:Int64 = json[2].int64Value
            //            print("time_stamp_String: \(time_stamp_String)")
            //            print("music_start_time_delay_String: \(music_start_time_delay_String)")
            
            WebToNativeHelper.sharedInstance.startStreamingFromUrl(json[0], server_time_stamp: time_stamp_String, music_start_time_delay: music_start_time_delay_String)
        })
    
    }
    
    func toggleDrawerMenuClick(command: CDVInvokedUrlCommand) {
        self.commandDelegate?.runInBackground({ () -> Void in
            WebToNativeHelper.sharedInstance.toggleDrawerMenuClick()
        })
        
    }
    
    func groupUploadRequestReceived(command: CDVInvokedUrlCommand) {
        
        self.commandDelegate?.runInBackground({ () -> Void in
            let json = JSON(command.arguments)
            WebToNativeHelper.sharedInstance.groupUploadRequestReceived(json[0])
        })
        
    }
    
    func addTrackUniqueToLobeList(command: CDVInvokedUrlCommand) {
        
//        self.commandDelegate?.runInBackground({ () -> Void in
            let json = JSON(command.arguments)
            WebToNativeHelper.sharedInstance.addTrackUniqueToLobeList(json[0])
//        })
    }
    
    func createAllLocalFileChunks(command: CDVInvokedUrlCommand) {
        
        let json = JSON(command.arguments)
        WebToNativeHelper.sharedInstance.createAllLocalFileChunks(json[0], nbSecondsPerChunk: json[1].int!)
        
    }
    
    func uploadFileToServer(command: CDVInvokedUrlCommand) {
        
        self.commandDelegate?.runInBackground({ () -> Void in
            autoreleasepool {
                
                let json = JSON(command.arguments)
                let mCorrelationID:String = json[2].string!
                let mChunkStartTimeServer:Int64 = json[3].int64Value
                let mChunkIndex:Int64 = json[0].int64Value
                
                //        print("mCorrelationID: \(mCorrelationID)")
                //        print("mChunkStartTimeServerUInt64: \(mChunkStartTimeServer)")
                
                WebToNativeHelper.sharedInstance.uploadFileToServer(UInt32(mChunkIndex), localFilePath: json[1].string!, correlationID: mCorrelationID, chunkStartTimeServer: mChunkStartTimeServer)
            }
        })
    
    }
    
    //===============================
    // MARK:- Social playback
    //===============================
    
    func updateCurrTrackProgressRatio(command: CDVInvokedUrlCommand) {
        
        dispatch_async(dispatch_get_main_queue()) {
            //TODO: - Update the slider circle
            autoreleasepool {
                
                let json = JSON(command.arguments)
                //           print("updateCurrTrackProgressRatio json: \(json)")
                guard  let ratioFloat = json[0].double else {
                    //                print("updateCurrTrackProgressRatio ratio value is nil.. can't update the slider")
                    return
                }
                social_slider_value.value = ratioFloat
            }
            
        }
    }
    
    //===============================
    // MARK:- Social Voting System
    //===============================
    func setLobeListDynamicMap(command: CDVInvokedUrlCommand){
        autoreleasepool {
            
            let json = JSON(command.arguments)
            //        print("webcall setLobeListDynamicMap:")
            AUDIO_OUT_MANAGER.socialVotingMapList = json
        }

    }
    
    func showLoadingSpinner(command: CDVInvokedUrlCommand){
        dispatch_async(dispatch_get_main_queue()) {
            
            autoreleasepool {
                
                //            self.appDelegateInstance!.window?.showActivityViewWithLabel("")
                guard let window = self.appDelegateInstance!.window else {return}
                self.HUD.interactionType = .BlockNoTouches
                self.HUD.showInView(window)
            }
        }

    }
    
    func hideLoadingSpinner(command: CDVInvokedUrlCommand){
        dispatch_async(dispatch_get_main_queue()) {
            
//            self.appDelegateInstance!.window?.hideActivityView()
            self.HUD.dismissAnimated(true)
        }
    }

    func requestNativeToShowView(command: CDVInvokedUrlCommand){
        
        autoreleasepool {
            
            let json = JSON(command.arguments)
            guard let viewString = json[0].string else {
                //            print("requestNativeToShowView sent a viewString that is not a valid")
                return
            }
            //===============================================================
            //TODO:- Move this in showView call in NativeToWebHelper
            //===============================================================
            //        print("webcall requestNativeToShowView: \(viewString)")
            dispatch_async(dispatch_get_main_queue()) {
                
                //            guard viewString != "localMusicView" else{
                //                LobeStateHelper.sharedInstance.menu_selected_index.value = 4
                //                AUDIO_OUT_MANAGER.showLibraryView()
                //                return
                //            }
                
                //            guard LobeStateHelper.currentUser.isSignedIn else {
                //                NativeToWebHelper.sharedInstance.showView("welcomeView", backToViewString: "")
                ////                self.manageViewChange("welcomeView")
                //                return
                //            }
                
                //If user is in group check
                if LobeStateHelper.sharedInstance.state == .Local {
                    
                    var invalidViewState = false
                    switch (viewString)
                    {
                    case "groupView","chatting","invitePeopleView":
                        invalidViewState = true
                        break;
                    default :
                        break;
                    }
                    guard !invalidViewState else {
                        print("\(viewString) is not a valid view in Local Model..Ignoring it")
                        return
                    }
                }
                //TODO:- add magic button check (eventually..check si pertinent)
                NativeToWebHelper.sharedInstance.showView(viewString, backToViewString: "")
                //            self.manageViewChange(viewString)
            }
        }
    }
    
    func openPlaylistView(command: CDVInvokedUrlCommand){

        dispatch_async(dispatch_get_main_queue()){
            
            AUDIO_OUT_MANAGER.mainVC.expandPlayerView()
        }
    }
    
    func updateSocialProfileInfo(command: CDVInvokedUrlCommand){
        let json = JSON(command.arguments)
//        print("webcall updateSocialProfileInfo: \(json)")
//        print(" followers: \(json[0])")
    }
    
    func triggerLoginWithFacebook(command: CDVInvokedUrlCommand){
        let json = JSON(command.arguments)
//        print("webcall triggerLoginWithFacebook: \(json)")
        dispatch_async(dispatch_get_main_queue()) {
            NativeToWebHelper.sharedInstance.delegate!.genericLoginWithFacebook()
        }
    }
    
    func setIsChattingPanelOpen(command: CDVInvokedUrlCommand){
        let json = JSON(command.arguments)
//        print("webcall triggerLoginWithFacebook: \(json)")
        let isChattingPanelOpen = json[0].bool!
        LobeStateHelper.sharedInstance.setIsChattingPanelOpen(isChattingPanelOpen)
    }
    
    func playerCommApiReceivedMessage(command: CDVInvokedUrlCommand){
        
        autoreleasepool {
            
            let json = JSON(command.arguments)
            //        print("webcall playerCommApiReceivedMessage: \(json)")
            let strObj = json[0]
            let cleanString = JsonHelper.replaceBadChar("\(strObj)")
            NativeToWebHelper.sharedInstance.youtube_delegate?.playerCommApiReceivedMessage("'\(cleanString)'")
        }
    }
    
    func pingNative(command: CDVInvokedUrlCommand){
        
//        self.commandDelegate?.runInBackground({ () -> Void in
            let json = JSON(command.arguments)
            print("webcall pingNative: \(json)")
            let strObj = json[0].stringValue
            
            switch strObj {
                
            case "refreshSpotifySessionToken":
                 let sessionToken = json[1].stringValue
//                 SpotifyHelper.sharedInstance.refreshSessionToken()
//                    NativeToWebHelper.sharedInstance.fromNativePing(pingType: "updateSpotifyAccessToken", params: ["access_token":SpotifyHelper.sharedInstance.auth.session.accessToken])
                SpotifyHelper.sharedInstance.loginPlayerWithSessionToken(sessionToken: sessionToken)
            case "musicStatePlay":
                    AUDIO_OUT_MANAGER.playMusic()
            case "musicStatePaused":
                    AUDIO_OUT_MANAGER.pauseMusic()
            case "loginWithSpotify":
//                SpotifyHelper.sharedInstance.loginWithSpotify()
                AUDIO_OUT_MANAGER.mainVC.loginSpotify()
            case "setupSpotifyAuth":
                
                let sessionToken = json[1].stringValue
//                SpotifyHelper.sharedInstance.refreshSessionToken()
//                NativeToWebHelper.sharedInstance.fromNativePing(pingType: "updateSpotifyAccessToken", params: ["access_token":sessionToken])
                SpotifyHelper.sharedInstance.loginPlayerWithSessionToken(sessionToken: sessionToken)
                
            case "expandPlaybackBar":
                AUDIO_OUT_MANAGER.mainVC.expandPlayerView()
                AUDIO_OUT_MANAGER.mainVC.togglePlaybackView(isHidden:false)
            case "showPlaybackBar":
                AUDIO_OUT_MANAGER.mainVC.togglePlaybackView(isHidden:false)
                AUDIO_OUT_MANAGER.mainVC.togglePlaylistView(isHidden: true)
            case "hidePlaybackBar":
                AUDIO_OUT_MANAGER.mainVC.togglePlaybackView(isHidden:true)
                AUDIO_OUT_MANAGER.mainVC.togglePlaylistView(isHidden: true)
            case "appIsFullyReady":
                AUDIO_OUT_MANAGER.mainVC.toggleLoadingView(isHidden:true)
//                SpotifyHelper.sharedInstance.retrieveSessionFromCache()
                NativeToWebHelper.sharedInstance.fromNativePing(pingType: "iosNativeVersionNumber", params: ["version":7])
            case "shareOnSocialMedia":
                let group_id = json[1].stringValue
                AUDIO_OUT_MANAGER.mainVC.displayShareSheet(group_id)
            case "toggleFullScreen":
                if LobeStateHelper.sharedInstance.youtube_view_mode == .FullScreen {
                    
                    AUDIO_OUT_MANAGER.mainVC.leaveFullScreenLandscape()
                    
                }else{
                    
                    LobeStateHelper.sharedInstance.youtube_view_mode = .FullScreen
                    AUDIO_OUT_MANAGER.mainVC.enterFullScreenLandscape()
                    
                }
                
            default:
                break
            }
            
//        })
    }
    
}