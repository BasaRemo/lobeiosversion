//
//  InitVC.swift
//  Lobe
//
//  Created by Professional on 2016-01-17.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

import UIKit
//import VYPlayIndicator
import ZFDragableModalTransition
import AVFoundation
import Foundation
import MediaPlayer
import SwiftyJSON
import Alamofire
import Haneke
import Bond
//import JLToast
//import RealmSwift
//import CircleSlider
import Social
import Cartography
import LGPlusButtonsView

protocol InitVCDelegate {
    func openWebView()
    func nextButtonTapped()
    func previousButtonTapped()
    func playPauseButtonTapped()
    func setupTargetView()
    func goToSearchView()
    func goToChatView()
    func goToYoutubeView()
    func goToLocalLibraryView()
}

var social_slider_value : Observable<Double> = Observable<Double>(0.0)
class InitVC: UIViewController {

    @IBOutlet var containerView:UIView!
    @IBOutlet var playbackView:UIView!
    @IBOutlet weak var artWorkImageView: UIImageView!
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var songNameLabel: UILabel!
    @IBOutlet weak var artistLabel: UILabel!
    @IBOutlet var filterView:UIView!
    
//    @IBOutlet var currentTrackTimeLabel:UILabel!
//    @IBOutlet var trackLengthLabel:UILabel!
    @IBOutlet var socialButton:UIButton!
    @IBOutlet var playButton:UIButton!
    @IBOutlet var nextButton:UIButton!
    @IBOutlet var previousButton:UIButton!
    @IBOutlet var addButton:UIButton!
    @IBOutlet var settingsButton:UIButton!
    @IBOutlet var chatButton:UIButton!
    @IBOutlet var closeButton:UIButton!
    
    
    @IBOutlet var circularSlider:BWCircularSliderView!
    let youtubeViewGroupContraint = ConstraintGroup()
    @IBOutlet var youtubePlayerContainerView:UIView!
    @IBOutlet var youtubeBackgroundView:UIView!
//    var realm: Realm!
    var trackLength = 0
    var albumArtImage:UIImage?
    var webViewVC: LobeWebVC?
    var mainVC:MainVC?
    var animator:ZFModalTransitionAnimator!
    var animator2:ZFModalTransitionAnimator!
    var currentPlayingSongTimer = NSTimer()
    var useSliderValue = false
    var delegate:InitVCDelegate?
    var playlistVC: PlaylistVC?
    var rootViewController:MainVC?
//    private var circleSlider: CircleSlider! {
//        didSet {
//            self.circleSlider.tag = 0
//        }
//    }
//    
//    private var sliderOptions: [CircleSliderOption] {
//        return [
//            .BarColor(UIColor.clearColor()),
//            .ThumbColor(UIColor.appThemeColorBackgroundThirdOrange()),
//            .TrackingColor(UIColor.appThemeColorBackgroundThirdOrange()),
//            .BarWidth(5),
//            .StartAngle(-90),
//            .MaxValue(1),
//            .MinValue(0),
//            .ThumbWidth(25)
//        ]
//    }
    var plusButtonView: LGPlusButtonsView!
    
    @IBOutlet weak var targetImageView : UIImageView!
    
    //==========================================================================================
    // MARK: - vc Life cycle
    //==========================================================================================
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        realm = try! Realm()
        self.playlistVC = self.childViewControllers.first as? PlaylistVC
        
        LobeStateHelper.sharedInstance.addLobeStateObserver(self)
        self.observeCurrentSongChange()
        
        resetSlider()
        setupUI()
        self.initPlusButtonView()
        self.resizeYoutubeView(viewMode: .Large)
        self.updatePlayBackView()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        //        self.delegate?.setupTargetView()
        //        disableSlider()
//        lobeStateChanged()
    }
    override func  viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
//        self.rootViewController!.isInteractiveTransitionDisabled = false
//        let value = UIInterfaceOrientation.Portrait.rawValue
//        UIDevice.currentDevice().setValue(value, forKey: "orientation")
        self.updateMusicPlayingUI()
        self.scrollToCurrentPlayingItem(animated:false)
        self.updatePlayBackView()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    @IBAction func tapCloseButton() {
        
        let value = UIInterfaceOrientation.Portrait.rawValue
        UIDevice.currentDevice().setValue(value, forKey: "orientation")
//        startViewController.isInteractiveTransitionDisabled = true
//        self.rootViewController?.isInteractiveTransitionDisabled = true
//        self.dismissViewControllerAnimated(true) { () -> Void in
////            startViewController.isInteractiveTransitionDisabled = false
//            self.rootViewController?.isInteractiveTransitionDisabled = false
//            LobeStateHelper.sharedInstance.youtube_view_mode = .Large
//            AUDIO_OUT_MANAGER.mainVC.leaveFullScreenLandscape()
////            LobeStateHelper.sharedInstance.updateYoutubeViewState()
//        }
        
//        let appDelegateInstance = UIApplication.sharedApplication().delegate as? AppDelegate
//        appDelegateInstance?.loginWithSpotify()
        
//        AUDIO_OUT_MANAGER.mainVC.leaveFullScreenLandscape()
        AUDIO_OUT_MANAGER.mainVC.togglePlaylistView(isHidden: true)
    }
    
    override func shouldAutorotate() -> Bool {
        if LobeStateHelper.sharedInstance.youtube_view_mode == .FullScreen { return true }
        return false
    }
    
    override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
//        if LobeStateHelper.sharedInstance.youtube_view_mode == .FullScreen { return UIInterfaceOrientation.LandscapeRight }
        return UIInterfaceOrientation.Portrait
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        
        return UIInterfaceOrientationMask.AllButUpsideDown
    }

   
//==========================================================================================
// MARK: - UI Setup
//==========================================================================================
    func setupUI(){
        
        //Rotate the slider to start from the top
        self.circularSlider.transform = CGAffineTransformMakeRotation((-90.0 * 3.14) / 180.0)
        // Attach an Action and a Target to the slider
        let gestureRecognizer1 = UITapGestureRecognizer(target: self, action: #selector(InitVC.playOrPauseMusic(_:)))
        self.circularSlider.addGestureRecognizer(gestureRecognizer1)
        self.circularSlider.slider.addTarget(self, action: #selector(InitVC.valueChanged(_:)), forControlEvents: UIControlEvents.ValueChanged)
        self.circularSlider.slider.addTarget(self, action: #selector(InitVC.circSliderTouchDown(_:)), forControlEvents: UIControlEvents.TouchDown)
        self.circularSlider.slider.addTarget(self, action: #selector(InitVC.circSliderTouchCanceled(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        blurImage()
        //Playback view separator
        //        let px = 2 / UIScreen.mainScreen().scale
        //        let frame = CGRectMake(0, CGRectGetHeight(self.playbackView.frame), self.playbackView.frame.size.width, px)
        //        let line: UIView = UIView(frame: frame)
        //        self.playbackView.addSubview(line)
        //        line.backgroundColor = UIColor.blackColor()
        //        self.playbackView.backgroundColor = UIColor.playbackBackgroundColor()
        
        
        let width = Int(CGRectGetWidth(self.view.bounds))
        let heigth = Int(CGRectGetHeight(self.view.bounds))
        let croppedImage = DownloadHelper.cropImage( UIImage(named: placeholderImageName)!, withWidth: width , andHeigth:heigth)
        
        self.view.backgroundColor = UIColor(patternImage:croppedImage)
//        self.artWorkImageView.image = croppedImage
//        self.artWorkImageView.layer.cornerRadius = CGRectGetWidth(self.artWorkImageView.frame)/2
        
        //Long press actions
//        let gestureRecognizer = UILongPressGestureRecognizer(target: self, action: "openAddSongContextMenu:")
//        gestureRecognizer.minimumPressDuration = 0.0
//        self.addButton.addGestureRecognizer(gestureRecognizer)
//        
//        let gestureRecognizer2 = UILongPressGestureRecognizer(target: self, action: "openOptionsContextMenu:")
//        gestureRecognizer2.minimumPressDuration = 0.0
//        self.settingsButton.addGestureRecognizer(gestureRecognizer2)

        //Tap actions
//        let gestureRecognizer3 = UITapGestureRecognizer(target: self, action: #selector(InitVC.optionButtonTapped))
//        self.settingsButton.addGestureRecognizer(gestureRecognizer3)
        
//        let gestureRecognizer4 = UITapGestureRecognizer(target: self, action: #selector(InitVC.addTrackButtonTapped))
//        self.addButton.addGestureRecognizer(gestureRecognizer4)
        
        constrain(self.playbackView) { playerView in
            
            playerView.top  == playerView.superview!.top
            playerView.right == playerView.superview!.right
            playerView.left == playerView.superview!.left
            playerView.height == playerView.superview!.height / 2.40
        }
        
    }
    
    func blurImage() {
        
        let darkBlur = UIBlurEffect(style: UIBlurEffectStyle.Dark)
        let blurView = UIVisualEffectView(effect: darkBlur)
        blurView.frame = self.view.bounds
        self.view.addSubview(blurView)
        self.view.sendSubviewToBack(blurView)
        
        constrain(blurView) { blurView in
            
            blurView.top  == blurView.superview!.top
            blurView.bottom  == blurView.superview!.bottom
            blurView.right == blurView.superview!.right
            blurView.left == blurView.superview!.left
        }
    }
    
//==========================================================================================
// MARK: - playbackview functions
//==========================================================================================

    @IBAction func playOrPauseMusic(sender: UIButton) {
//        self.delegate!.playPauseButtonTapped()
        if AUDIO_OUT_MANAGER.isMusicPlaying() {
            AUDIO_OUT_MANAGER.pauseMusic()
        }else{
            AUDIO_OUT_MANAGER.playMusic()
        }
        useSliderValue = false
    }
    
    @IBAction func previousButtonPressed(sender: UIButton) {
//        if LobeStateHelper.sharedInstance.state == .Participant {
//            sender.selected = !sender.selected
//        }
//        self.delegate!.previousButtonTapped()
//        previousButton.enabled = false
//        previousButton.userInteractionEnabled = false
        AUDIO_OUT_MANAGER.skipToPreviousMusic()
    }
    @IBAction func nextButtonPressed(sender: UIButton) {
//        if LobeStateHelper.sharedInstance.state != .Local {
//            guard AUDIO_OUT_MANAGER.lobeListSongItems.count > 1 else {return}
//        }
//        self.delegate!.nextButtonTapped()
//        nextButton.userInteractionEnabled = false
        
        AUDIO_OUT_MANAGER.skipToNextMusic()
    }
    
    //Scrolls to the current playing song
    @IBAction func seekBtnPressed(sender: UIButton) {

        scrollToCurrentPlayingItem(animated:true)
    }
    
    func enableItems(){

        previousButton.enabled = true
        nextButton.enabled = true
        nextButton.userInteractionEnabled = true
        previousButton.userInteractionEnabled = true
        AUDIO_OUT_MANAGER.mainVC.enableItems()
        
    }
    func scrollToCurrentPlayingItem(animated animated:Bool) {
        
        guard AUDIO_OUT_MANAGER.lobeListSongItems.count > 0 else {
            print("No song in lobelist")
//            JLToast.makeText("No song in lobelist").show()
//            WebToNativeHelper.sharedInstance.showToastMessage("No song in lobelist")
            return
        }
        //TODO: - Crashes here because cant find the current song
        let indexOfCurrentPlayingMusic = AUDIO_OUT_MANAGER.getIndexOfCurrentPlayingLobeListTrack()
        guard indexOfCurrentPlayingMusic >= 0 else {return}
        guard indexOfCurrentPlayingMusic < AUDIO_OUT_MANAGER.lobeListSongItems.count  else {return }
        let indexPath = NSIndexPath(forRow: 0, inSection: indexOfCurrentPlayingMusic)
        guard let playlistVC = playlistVC else {return }
        playlistVC.tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Top, animated: animated)
    }
}

//==========================================================================================
// MARK: - BWCircularSlider
//==========================================================================================

extension InitVC {
    
    func valueChanged(slider:BWCircularSlider){
    }
    func circSliderTouchDown(slider:BWCircularSlider){
        
        useSliderValue = true
    }
    func circSliderTouchCanceled(slider:BWCircularSlider){
        useSliderValue = false
        
        let revereSeekTime:Int = Int(trackLength*slider.angle/360)
        let seekTime = Float(trackLength - revereSeekTime)
        
        seekTo(seekTime)
    }
    
    func resetSlider(){
        setupSlider()
        startCurrentPlayingSongTimer()
        updateTrackLength()
    }
    func setupSlider(){
//        trackLengthLabel.text = "00:00"
//        currentTrackTimeLabel.text = "00:00"
        dispatch_async(dispatch_get_main_queue()) {
            self.circularSlider.slider.angle = 360
        }
        
//        circularSlider.slider.setNeedsDisplay()
    
    }
    func startCurrentPlayingSongTimer(){
        
        dispatch_async(dispatch_get_main_queue()) {
            
            self.currentPlayingSongTimer.invalidate()
            // start the timer
            self.currentPlayingSongTimer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: "timerAction", userInfo: nil, repeats: true)
        }
    }
    
    func stopLocalSongTimer(){
        currentPlayingSongTimer.invalidate()
    }
    func timerAction() {
        if  AUDIO_OUT_MANAGER.isMusicPlaying() && !useSliderValue && (LobeStateHelper.sharedInstance.state == .Local) {
                updateTime()
        }
    }

    func updateTime() {
        
        guard (AUDIO_OUT_MANAGER.player.nowPlayingItem != nil)  else {
//            print("no nowPlayingItem at the moment")
            return
        }
        let currentTime = Int(AUDIO_OUT_MANAGER.player.currentPlaybackTime)
//        guard currentTime > 0 else {
//            print("updateTime():AUDIO_OUT_MANAGER.player.currentPlaybackTime < 0 .. Skipping time update")
//            return
//        }
//        let minutes = currentTime/60
//        let seconds = currentTime - minutes * 60
//        circleSlider.value = Float(currentTimeD)/Float(trackLength)
        social_slider_value.value = Double(Float(currentTime)/Float(trackLength))
        
        //Only for the BWCircularSlider
        guard trackLength > 0 else {
//            print("updateTime: tracklength = \(trackLength)")
            return
        }
        let sliderAngle = (trackLength - currentTime)*360/trackLength
        dispatch_async(dispatch_get_main_queue()) {
            self.circularSlider.slider.angle = sliderAngle
            self.circularSlider.slider.setNeedsDisplay()
        }
//        currentTrackTimeLabel.text = NSString(format: "%02d:%02d", minutes,seconds) as String
        
    }
    //FIXME:- IMPORTANT - This function crashed when called BEFORE THE nowPlayingItem in the local player is set
    func updateTrackLength() {
        guard let nowPlayingItem = AUDIO_OUT_MANAGER.player.nowPlayingItem else {
            return
        }
         trackLength = Int(nowPlayingItem.playbackDuration)
//        let minutes = trackLength/60
//        let seconds = trackLength - minutes * 60
//        trackLengthLabel.text = NSString(format: "%02d:%02d", minutes,seconds) as String
//        print("trackLength:\(trackLength)")
//        self.changeSliderMaxValue(value: Float(self.trackLength))
    }
    
    func seekTo(currentTime:Float){
        AUDIO_OUT_MANAGER.player.currentPlaybackTime = Double(currentTime)
    }
}


//==========================================================================================
// MARK: - Observers
//==========================================================================================

extension InitVC {
    
    func observeCurrentSongChange(){
        
        AUDIO_OUT_MANAGER.currentTrackObservable.observe{ currentTrack in
            
            dispatch_async(dispatch_get_main_queue()) {
//                self.enableItems()
                self.songNameLabel.text = currentTrack.trackInfo.mName
                self.artistLabel.text = currentTrack.trackInfo.mArtist
                self.useSliderValue = false
//                self.resetSlider()
                self.updateTrackLength()
                self.circularSlider.slider.angle = 360
                self.updateMusicPlayingUI()
                let image = UIImage(named: "lobe_background_2")
                let croppedImage = DownloadHelper.cropImage(image!, withWidth: Int(CGRectGetWidth(self.view.bounds)) , andHeigth:Int(CGRectGetHeight(self.view.bounds)))
                dispatch_async(dispatch_get_main_queue()) {
                    self.artWorkImageView.image = croppedImage
                    self.view.backgroundColor = UIColor(patternImage: croppedImage)
                }
            }
            
            currentTrack.trackInfo.songAlbumArt.observe{ image in
//                self.enableItems()
                if let albumImage = image {
                    
                    let croppedImage = DownloadHelper.cropImage(albumImage, withWidth: Int(CGRectGetWidth(self.view.bounds)) , andHeigth:Int(CGRectGetHeight(self.view.bounds)))
                    dispatch_async(dispatch_get_main_queue()) {
                        self.artWorkImageView.image = croppedImage
                        self.view.backgroundColor = UIColor(patternImage: croppedImage)
                    }
                }
            }
        }
        
        //Observe audio playing state
        AUDIO_OUT_MANAGER.isAudioPlaying.observe{ isAudioPlaying in
            dispatch_async(dispatch_get_main_queue()) {
               self.updateMusicPlayingUI()
            }
        }
        
    }
    
    func updateMusicPlayingUI(){
        self.playButton.selected = AUDIO_OUT_MANAGER.isMusicPlaying()
    }
}

//==========================================================================================
// MARK:- App State Change management
//==========================================================================================
extension InitVC {
    
    func lobeStateChanged(){
        dispatch_async(dispatch_get_main_queue()) {
            self.updateMusicPlayingUI()
//            self.enableItems()
        }
        self.updateState()
    }

    func updateState(){
//        print("playbackview update")
        
        nextButton.setImage(UIImage(named: "next_button_unfill"), forState: UIControlState.Normal)
        nextButton.setImage(UIImage(named: "next_button"), forState: UIControlState.Highlighted)
        nextButton.setImage(UIImage(named: "next_button"), forState: UIControlState.Selected)
        
        previousButton.setImage(UIImage(named: "previous_button_unfill"), forState: UIControlState.Normal)
        previousButton.setImage(UIImage(named: "previous_button"), forState: UIControlState.Highlighted)
        previousButton.setImage(UIImage(named: "previous_button"), forState: UIControlState.Selected)
//        chatButton.hidden = true
        
        switch LobeStateHelper.sharedInstance.state {
            
        case .Local:
            
//            self.circularSlider.slider.userInteractionEnabled = true
            dispatch_async(dispatch_get_main_queue()) {
//                self.settingsButton.setImage(UIImage(named: "option_button_2"), forState: UIControlState.Normal)
//                self.changeSliderMaxValue(value: Float(self.trackLength))
                self.startCurrentPlayingSongTimer()
                self.stateLabel.text = ""
//                self.chatButton.hidden = true
            }
            
            
        case .DJ:
            
            //Make sure im in the main queue
            dispatch_async(dispatch_get_main_queue()) {
//                self.changeSliderMaxValue(value: 1)
//                 self.settingsButton.setImage(UIImage(named: "share_icon"), forState: UIControlState.Normal)
                
//                self.circularSlider.slider.userInteractionEnabled = false
                self.stopLocalSongTimer()
                self.stateLabel.text = "DJ Mode"
                
                social_slider_value.observe{ sliderValue in
                    let sliderAngle = (1 - sliderValue)*360
                    self.circularSlider.slider.angle = Int(sliderAngle)
                    self.circularSlider.slider.setNeedsDisplay()
//                    self.enableItems()

//                    self.circleSlider.value = Float(social_slider_value.value)
                }
            }

            
        case .Participant:
            
            dispatch_async(dispatch_get_main_queue()) {
//                self.changeSliderMaxValue(value: 1)
//                 self.settingsButton.setImage(UIImage(named: "share_icon"), forState: UIControlState.Normal)
                
//                self.circularSlider.slider.userInteractionEnabled = false
                self.stopLocalSongTimer()
                
                social_slider_value.observe{ sliderValue in
                    let sliderAngle = (1 - sliderValue)*360
                    self.circularSlider.slider.angle = Int(sliderAngle)
                    self.circularSlider.slider.setNeedsDisplay()
//                    self.circleSlider.value = Float(social_slider_value.value)

                    
                }
            }
            stateLabel.text = "Streaming"
//            self.view.backgroundColor = UIColor.redColor()
            nextButton.setImage(UIImage(named: "like_btn_unselected"), forState: UIControlState.Normal)
            nextButton.setImage(UIImage(named: "like_btn_selected"), forState: UIControlState.Highlighted)
            nextButton.setImage(UIImage(named: "like_btn_selected"), forState: UIControlState.Selected)
            
            previousButton.setImage(UIImage(named: "dislike_btn_unselected"), forState: UIControlState.Normal)
            previousButton.setImage(UIImage(named: "dislike_btn_selected"), forState: UIControlState.Highlighted)
            previousButton.setImage(UIImage(named: "dislike_btn_selected"), forState: UIControlState.Selected)
            
        }
    }
}

//==========================================================================================
// MARK:- Add song management
//==========================================================================================
extension InitVC {
    
    @IBAction func addTrackButtonTapped() {
        showTrackOptionPopover(0)
    }
    
    
    func showTrackOptionPopover(index:Int){

        var alert: UIAlertController!
        var firstAction: UIAlertAction!
        var secondAction: UIAlertAction!
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: {(alert: UIAlertAction!) in
//            print("cancel")
        })
        
        switch index
        {
        case 0:
            alert = UIAlertController(title: "Add Song", message: "", preferredStyle: .ActionSheet)
            firstAction = UIAlertAction(title: "From Local library", style: UIAlertActionStyle.Default) { (alert: UIAlertAction!) -> Void in
                self.delegate?.goToLocalLibraryView()
                self.tapCloseButton()
//                self.dismissViewControllerAnimated(true, completion: nil)
            }
            
            if (LobeStateHelper.sharedInstance.state == .DJ || LobeStateHelper.sharedInstance.state == .Participant) {
                
                secondAction = UIAlertAction(title: "From youtube", style: .Default) { (alert: UIAlertAction!) -> Void in
                    //                NativeToWebHelper.sharedInstance.showView("youtubeSearch", backToViewString: "")
                    self.delegate?.goToYoutubeView()
                    self.tapCloseButton()
//                    self.dismissViewControllerAnimated(true, completion: nil)
                    
                }
                alert.addAction(secondAction)
            }
            alert.addAction(firstAction)
            alert.addAction(cancelAction)
            presentViewController(alert, animated: true, completion:nil)
            
        case 1:

            alert = UIAlertController(title: "LobeList options", message: "", preferredStyle: .ActionSheet)
            
            if (LobeStateHelper.sharedInstance.state == .Local) {
                
                firstAction = UIAlertAction(title: "Remove all items", style: UIAlertActionStyle.Destructive) { (alert: UIAlertAction!) -> Void in
                    AUDIO_OUT_MANAGER.lobeListSongItems = NSMutableArray()
                    AUDIO_OUT_MANAGER.lobeListObservableList.replaceRange(0...0, with: [SongItem()])
                }
                secondAction = UIAlertAction(title: "Save", style: UIAlertActionStyle.Default) { (alert: UIAlertAction!) -> Void in
//                    self.savePlaylist()
                    
                }
                alert.addAction(secondAction)
                alert.addAction(firstAction)
            }else {
                firstAction = UIAlertAction(title: "Share on Facebook", style: UIAlertActionStyle.Default) { (alert: UIAlertAction!) -> Void in
//                     JLToast.makeText("This functionality is not yet implemented").show()
                    let text = "Hey Folks, Im listen to \(AUDIO_OUT_MANAGER.currentSong.trackInfo.mName) on Lobe! Come Join me!"
                    self.shareOnFacebook(text)
                }
                alert.addAction(firstAction)
            }
            
            alert.addAction(cancelAction)
            presentViewController(alert, animated: true, completion:nil)
            
        case 2:
            break

        default:
            break
        }
        
    }
    
    func displayShareSheet(shareContent:String) {
        
        let image: UIImage = UIImage(named: "Icon-72")!
        let activityViewController = UIActivityViewController(activityItems: [(shareContent),(image)], applicationActivities: nil)
        
        // Anything you want to exclude
        activityViewController.excludedActivityTypes = [
            UIActivityTypePostToWeibo,
            UIActivityTypePrint,
            UIActivityTypeCopyToPasteboard
        ]
        
        presentViewController(activityViewController, animated: true, completion: {})
    }
    
    func shareOnFacebook(shareContent:String) {
        if SLComposeViewController.isAvailableForServiceType(SLServiceTypeFacebook){
            let facebookSheet:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
            facebookSheet.setInitialText(shareContent)
            self.presentViewController(facebookSheet, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: "Accounts", message: "Please login to a Facebook account to share.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
}



//==========================================================================================
// MARK:- Chat management
//==========================================================================================
extension InitVC {
    
    @IBAction func chatButtonTapped(sender: UIButton) {
        
        self.delegate?.goToChatView()
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
        
//==========================================================================================
// MARK:- Playlist management
//==========================================================================================
//extension InitVC {
//    
//    @IBAction func optionButtonTapped() {
//        
////        showTrackOptionPopover(1)
//        if (LobeStateHelper.sharedInstance.state == .Local) {
//            showTrackOptionPopover(1)
//        }else {
//            let text = "Hey Folks, Im listen to \(AUDIO_OUT_MANAGER.currentSong.trackInfo.mName) on Lobe! Come Join me!"
//            self.displayShareSheet(text)
//        }
//        
//    }
//    
//    func savePlaylist(){
//        
//        let alertController = UIAlertController(title: "Save Playlist", message:
//            "", preferredStyle: UIAlertControllerStyle.Alert)
//        
//        alertController.addTextFieldWithConfigurationHandler { (textField) in
//            textField.placeholder = "New Playlist Name"
//        }
//        alertController.addAction(UIAlertAction(title: "Save", style: UIAlertActionStyle.Default,handler: { (action) -> Void in
//            let textField = alertController.textFields![0] as UITextField
//            if (textField.text != "")  && (AUDIO_OUT_MANAGER.lobeListSongItems.count > 0) {
//                
//                //Override playlist if has same name
//                let savedPlaylists = self.realm.objects(UserPlaylist).filter("playlistName_='\(textField.text!)'")
//                if savedPlaylists.count > 0 {
//                    for elem in savedPlaylists{
//                        try! self.realm.write {
//                            self.realm.delete(elem)
//                        }
//                        
//                    }
//                }
//                let newPlaylist = UserPlaylist()
//                newPlaylist.idPlaylist_ = NSUUID().UUIDString
//                newPlaylist.playlistName_ = textField.text!
//                
//                for elem in AUDIO_OUT_MANAGER.lobeListSongItems{
//                    let newTrack = UserTrack()
//                    let songItem  = elem as! SongItem
//                    newTrack.idTrack_ = songItem.trackInfo.mID
//                    newTrack.trackName_ = songItem.trackInfo.mName
//                    
//                    newPlaylist.tracks_.append(newTrack)
//                    try! self.realm.write({ () -> Void in
//                        self.realm.add(newTrack)
//                    })
//                }
//                
//                try! self.realm.write({ () -> Void in
//                    self.realm.add(newPlaylist)
//                })
//                
//                self.playlistVC!.tableView.reloadData()
//                WebToNativeHelper.sharedInstance.showToastMessage("\(textField.text!) is saved")
//                
//                let pageDict: Dictionary<String,String>! = [
//                    "playlistName": "\(textField.text!)",
//                ]
//                NSNotificationCenter.defaultCenter().postNotificationName("NewNameNotification", object: nil, userInfo: pageDict)
//            }
//        }))
//        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default,handler: { (action) -> Void in
//            self.dismissViewControllerAnimated(true, completion: nil)
//        }))
//        self.presentViewController(alertController, animated: true){
//            
//        }
//    }
//}
//==========================================================================================
// MARK:- Segue
//==========================================================================================
extension InitVC {
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if let vc = segue.destinationViewController as? PlaylistVC
            where segue.identifier == "PlaylistVC_segue" {
                if self.playlistVC == nil {
                    self.playlistVC = vc
                }
        }

    }
}

extension InitVC:UIPopoverPresentationControllerDelegate{
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.None
    }
}

//==========================================================================================
// MARK:- Dragging alert
//==========================================================================================
extension InitVC {
    
//    func openAddSongContextMenu(gesture:UILongPressGestureRecognizer){
//        
//        switch LobeStateHelper.sharedInstance.state {
//        case .Local:
//            PCRapidSelectionView.viewForParentView(self.view ,currentGuestureRecognizer:gesture,interactive:true, options:["From Library"], title:"Quick Song Select", completionHandler: { (index:Int) -> Void in
//                print("Index selected: \(index)")
//                
//                switch index {
//                    case 0:
//                        self.delegate?.goToLocalLibraryView()
////                        self.dismissViewControllerAnimated(true, completion: nil)
//                    self.tapCloseButton()
//                    default:break
//                }
//                
//            })
//        case .DJ,.Participant:
//            PCRapidSelectionView.viewForParentView(self.view ,currentGuestureRecognizer:gesture,interactive:true, options:["From Youtube","From Library"], title:"Quick Song Select", completionHandler: { (index:Int) -> Void in
//                print("Index selected: \(index)")
//                
//                switch index {
//                case 0:
//                    self.delegate?.goToYoutubeView()
////                    self.dismissViewControllerAnimated(true, completion: nil)
//                    self.tapCloseButton()
//                case 1:
//                    self.delegate?.goToLocalLibraryView()
////                    self.dismissViewControllerAnimated(true, completion: nil)
//                    self.tapCloseButton()
//                default:break
//                }
//                
//            })
//        }
//        
//    }
    
//    func openOptionsContextMenu(gesture:UILongPressGestureRecognizer){
//        
//        switch LobeStateHelper.sharedInstance.state {
//            case .Local:
//                guard AUDIO_OUT_MANAGER.lobeListSongItems.count > 0 else {
////                    let message = "No songs in the lobelist so no options"
////                    JLToast.makeText(message).show()
////                    print("\(message)")
//                    return
//                }
//                PCRapidSelectionView.viewForParentView(self.view ,currentGuestureRecognizer:gesture,interactive:true, options:["Save","Remove All"], title:"Lobe list Options", completionHandler: { (index:Int) -> Void in
//                    print("Index selected: \(index)")
//                    
//                    switch index {
//                    case 0:
//                        self.savePlaylist()
//                    case 1:
//                        AUDIO_OUT_MANAGER.lobeListSongItems.removeAllObjects()
//                        AUDIO_OUT_MANAGER.lobeListObservableList.replaceRange(0...0, with: [SongItem()])
//                    default:break
//                    }
//                })
//            case .DJ,.Participant:
//                PCRapidSelectionView.viewForParentView(self.view ,currentGuestureRecognizer:gesture,interactive:true, options:["Share Lobelist on Facebook"], title:"Lobelist Options", completionHandler: { (index:Int) -> Void in
//                    print("Index selected: \(index)")
//                    
//                    switch index {
//                    case 0:break
////                         JLToast.makeText("This functionality is not yet implemented").show()
//                    default:break
//                    }
//                })
//        }
//
//    }
}

extension InitVC {
    
    func leaveFullScreenLandscape(){
        
        dispatch_async(dispatch_get_main_queue()) {
        
            LobeStateHelper.sharedInstance.youtube_view_mode = .Large
            let value = UIInterfaceOrientation.Portrait.rawValue
            UIDevice.currentDevice().setValue(value, forKey: "orientation")
            self.closeButton.hidden = false
//            self.plusButtonView.hidden = false
        }

        resizeYoutubeView(viewMode:.Large)
        
    }
    
    func enterFullScreenLandscape(){
        
        dispatch_async(dispatch_get_main_queue()) {
            
            self.resizeYoutubeView(viewMode: .FullScreen)
            let value = UIInterfaceOrientation.LandscapeRight.rawValue
            UIDevice.currentDevice().setValue(value, forKey: "orientation")
            self.closeButton.hidden = true
//            self.plusButtonView.hidden = true
        }
        
    }
    
    func resizeYoutubeView(viewMode viewMode:YoutubeViewMode){
        
        dispatch_async(dispatch_get_main_queue()) {
            
            self.view.layoutIfNeeded()
            
            UIView.animateWithDuration(0.3, delay: 0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
                
                switch viewMode {
                    
                case .Docked:break
                case .Large:
                    constrain(self.youtubePlayerContainerView, self.playbackView, replace: self.youtubeViewGroupContraint) { youtubeView, playerView in
                        
                        youtubeView.top  == youtubeView.superview!.top + 50
                        youtubeView.right == youtubeView.superview!.right
                        youtubeView.left == youtubeView.superview!.left
//                        youtubeView.height == youtubeView.superview!.height / 3
                        youtubeView.bottom == playerView.bottom - 25
                    }
                    
                case .FullScreen:
                    constrain(self.youtubePlayerContainerView, self.playbackView,self.view, replace: self.youtubeViewGroupContraint) { youtubeView, playerView, mainView in
                        
                        youtubeView.top  == youtubeView.superview!.top
                        youtubeView.bottom  == youtubeView.superview!.bottom
                        youtubeView.right == youtubeView.superview!.right
                        youtubeView.left == youtubeView.superview!.left
                    }
                }
                
                self.view.layoutIfNeeded()
                
                }, completion: { finished in
                    
            })
        }
    }
    
    func updatePlayBackView(){
        
    }
}
//
//// MARK: LGPlusButtonSetup
extension InitVC: LGPlusButtonsViewDelegate
{
    func plusButtonsViewPlusButtonPressed(plusButtonsView: LGPlusButtonsView!)
    {
        showOrHideCategories()
    }
    
    func plusButtonsView(plusButtonsView: LGPlusButtonsView!, buttonPressedWithTitle title: String!, description: String!, index: UInt)
    {
        switch index
        {
            case 0:break
    //            NativeToWebHelper.sharedInstance.showView("youtubeSearch", backToViewString: "")
    //            self.plusButtonView.hideButtonsAnimated(true, completionHandler: nil)
    //            self.tapCloseButton()
//            case 1:
//                NativeToWebHelper.sharedInstance.fromNativePing(pingType: "openChat", params: [:])
//                 self.plusButtonView.hideButtonsAnimated(true, completionHandler: nil)
//                 self.tapCloseButton()
            
            case 1:
                NativeToWebHelper.sharedInstance.fromNativePing(pingType: "syncTrack", params: [:])
                self.plusButtonView.hideButtonsAnimated(true, completionHandler: nil)
                
            case 2:
                scrollToCurrentPlayingItem(animated:true)
                self.plusButtonView.hideButtonsAnimated(true, completionHandler: nil)
            case 3:
                NativeToWebHelper.sharedInstance.fromNativePing(pingType: "addMusicView", params: [:])
                self.plusButtonView.hideButtonsAnimated(true, completionHandler: nil)
                self.tapCloseButton()
               
            default: break
        }

    }
    
    func setupPlusButtons()
    {
        plusButtonView.setButtonsEnabled(true)
        plusButtonView.setButtonsImages([UIImage(imageLiteral:"more_icon_yellow"),UIImage(imageLiteral:"sync_icon"),UIImage(imageLiteral:"now_playing_icon_unfill")], forState: .Normal, forOrientation: .Portrait)
//        plusButtonView.setButtonsImages([UIImage(imageLiteral:"more_icon")], forState: .Normal, forOrientation: .Portrait)
        plusButtonView.plusButtonAnimationType = .Rotate
        plusButtonView.setButtonsTitles(["+","",""], forState: .Normal)
        plusButtonView.setDescriptionsTexts(["","Sync Music","Now Playing"])
        plusButtonView.setButtonsBackgroundColor(UIColor.clearColor(), forState: .Normal)
        plusButtonView.setButtonsSize(CGSizeMake(44,44), forOrientation: .All)
        plusButtonView.setButtonsLayerCornerRadius(44/2, forOrientation: .All)
        plusButtonView.setButtonAtIndex(0, size: CGSizeMake(56,56), forOrientation: .All)
        plusButtonView.setButtonAtIndex(0, layerCornerRadius: 56/2, forOrientation: .All)
        plusButtonView.setButtonAtIndex(0, titleOffset: CGPointMake(0,-2), forOrientation: .All)
        
        plusButtonView.setButtonAtIndex(1, offset: CGPointMake(-8, 0), forOrientation: .All)
        plusButtonView.setButtonAtIndex(2, offset: CGPointMake(-8, 0), forOrientation: .All)
//        plusButtonView.setButtonAtIndex(3, offset: CGPointMake(-8, 0), forOrientation: .All)
//        plusButtonView.setButtonAtIndex(4, offset: CGPointMake(-8, 0), forOrientation: .All)
        
        plusButtonView.setButtonsTitleFont(UIFont.boldSystemFontOfSize(32), forOrientation: .All)
        
        plusButtonView.setDescriptionsBackgroundColor(UIColor.whiteColor())
        plusButtonView.setDescriptionsTextColor(UIColor.blackColor())
        plusButtonView.setDescriptionsFont(UIFont(name: "Helvetica Neue", size: 12), forOrientation: .All)
        plusButtonView.setDescriptionsLayerShadowColor(UIColor(red: 0.1, green: 0.1, blue: 0.1, alpha: 1))
        plusButtonView.setDescriptionsLayerShadowOpacity(0.25)
        plusButtonView.setDescriptionsLayerShadowRadius(1)
        plusButtonView.setDescriptionsLayerShadowOffset(CGSizeMake(3, 1))
        plusButtonView.setDescriptionsLayerCornerRadius(6, forOrientation: .All)
        plusButtonView.setDescriptionsContentEdgeInsets(UIEdgeInsetsMake(4, 8, 4, 8), forOrientation: .All)
        plusButtonView.setDescriptionsOffset(CGPointMake(-6, 0), forOrientation: .All)
        
        plusButtonView.setButtonsLayerShadowColor(UIColor(red: 0.1, green: 0.1, blue: 0.1, alpha: 1))
        plusButtonView.setButtonsLayerShadowOpacity(0.5)
        plusButtonView.setButtonsLayerShadowRadius(3)
        plusButtonView.setButtonsLayerShadowOffset(CGSizeMake(0, 2))
        plusButtonView.coverColor = UIColor.playbackBackgroundColor()
        plusButtonView.showHideOnScroll = true
        
        plusButtonView.offset = CGPointMake(-10, -10)
    }
    
    func initPlusButtonView()
    {
        
        plusButtonView = LGPlusButtonsView(numberOfButtons: 3, firstButtonIsPlusButton: true, showAfterInit: true, delegate: self)
        plusButtonView.delegate = self
        
        plusButtonView.position = LGPlusButtonsViewPosition.BottomRight
        plusButtonView.appearingAnimationType = LGPlusButtonsAppearingAnimationType.CrossDissolveAndSlideHorizontal
        plusButtonView.setButtonsTitleColor(UIColor.whiteColor(), forState:UIControlState.Normal)
        plusButtonView.setButtonsAdjustsImageWhenHighlighted(false)
        self.setupPlusButtons()
        self.view.addSubview(plusButtonView)
//        self.view.bringSubviewToFront(plusButtonView)
        
        
    }
    
    func showOrHideCategories() // efficient function to show or hide the plusbuttonview
    {
        if (plusButtonView.showing) { plusButtonView.hideAnimated(true, completionHandler:nil) }
        else { plusButtonView.showAnimated(true, completionHandler:nil) }
    }
}


extension InitVC {
    
    func toggleYoutubePlayerView(isHidden isHidden:Bool){
        dispatch_async(dispatch_get_main_queue()) {
            self.youtubePlayerContainerView.hidden = isHidden
            self.youtubeBackgroundView.hidden = isHidden
        }
    }
}