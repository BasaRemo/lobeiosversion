//
//  MenuVC.swift
//  Lobe
//
//  Created by Professional on 2016-01-20.
//  Copyright © 2016 Ntambwa. All rights reserved.
//

import UIKit
import Bond
//import LGSideMenuController

//protocol MenuDelegate {
//    func menuSearch()
//    func menuGoToCurrentSong()
//    func menuLeaveGroup()
//    func menuRecalculateSync()
//    func menuLogout()
//    func menuViewProfile()
//}
class MenuVC: UITableViewController {

    
    @IBOutlet var homeCell: UITableViewCell!
    @IBOutlet var exploreSessionsCell: UITableViewCell!
    @IBOutlet var currentSessionCell: UITableViewCell!
    @IBOutlet var myMusicCell: UITableViewCell!
    
    @IBOutlet var leaveGroupCell: UITableViewCell!
    @IBOutlet var resyncCell: UITableViewCell!
    
    @IBOutlet var userNameLabel: UILabel!
    @IBOutlet var userCityLabel: UILabel!
    @IBOutlet var userImageButton: UIButton!
    var delegate:MenuDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        LobeStateHelper.currentUser.userName.observe{ username in
//            self.userNameLabel.text = username
//        }
//        
//        LobeStateHelper.currentUser.userPic.observe{ image in
//            self.userImageButton.setImage(image, forState: UIControlState.Normal)
//        }
        
        LobeStateHelper.sharedInstance.addUserStateObserver(self)
        LobeStateHelper.sharedInstance.addLobeStateObserver(self)
        observeviewStateChanged()
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "menu_background")!)
        self.tableView.backgroundColor = UIColor(patternImage: UIImage(named: "menu_background")!)
        
        //Listen to these values
//        LobeStateHelper.currentUser.userPic.observe{ image in
//            self.userImageButton.setImage(image, forState: UIControlState.Normal)
//        }
//        
//        LobeStateHelper.currentUser.userName.observe{ username in
//            self.userNameLabel.text = username
//        }
    }
    
    func observeviewStateChanged(){
        
        LobeStateHelper.sharedInstance.menu_selected_index.observe{ currentTrack in
            dispatch_async(dispatch_get_main_queue()) {
//                print("menu_selected_index changed: \( LobeStateHelper.sharedInstance.menu_selected_index.value)")
                self.tableView.reloadData()
            }
        }
    }
    
    func userStateChanged(){
        if let image = LobeStateHelper.currentUser.userImage {
            self.userImageButton.setImage(image, forState: UIControlState.Normal)
        }
        if let username = LobeStateHelper.currentUser.userName.value {
            self.userNameLabel.text = username
        }
        
    }
    
    override func shouldAutorotate() -> Bool {
        return false
    }
    
    override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
        return UIInterfaceOrientation.Portrait
    }
    
    func lobeStateChanged(){
        self.tableView.reloadData()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
//        self.tableView.frame = CGRectMake(0,0,200,CGRectGetHeight(self.view.frame))
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        blurImage()
    }
    
    func blurImage() {
        
        let darkBlur = UIBlurEffect(style: UIBlurEffectStyle.Dark)
        let blurView = UIVisualEffectView(effect: darkBlur)
        blurView.frame = self.view.bounds
        self.view.addSubview(blurView)
        self.view.sendSubviewToBack(blurView)
        
    }
    // MARK: - Table view data source
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        let selectionView = UIView()
        selectionView.backgroundColor = SELECT_FOCUS_COLOR
        cell.selectedBackgroundView = selectionView
        
        guard let menuLabel:UILabel =  cell.contentView.viewWithTag(10) as? UILabel else {
            return
        }
        
        let playlistMenuIndex = 5
        let lobeState = LobeStateHelper.sharedInstance.state
        let currentSessionLabel = currentSessionCell.contentView.viewWithTag(10) as? UILabel
        
        if LobeStateHelper.sharedInstance.menu_selected_index.value == indexPath.row && indexPath.row != playlistMenuIndex {
            menuLabel.textColor = UIColor.orangePositiveColor()
        }else{
            menuLabel.textColor = UIColor.whiteColor()
        }
        
        guard lobeState == .Participant ||  lobeState == .DJ else{
            
            currentSessionCell.userInteractionEnabled = false
            currentSessionLabel!.textColor = UIColor.grayColor()
            resyncCell.hidden = true
            leaveGroupCell.hidden = true
            
            return
        }
        
        currentSessionCell.userInteractionEnabled = true
        resyncCell.hidden = false
        leaveGroupCell.hidden = false
        
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
         let cell =  self.tableView.cellForRowAtIndexPath(indexPath)
        cell?.selected = false
        
        if indexPath.section == 0 {
            
            if indexPath.row != 5 {
                LobeStateHelper.sharedInstance.menu_selected_index.value = indexPath.row
            }
            
            switch indexPath.row {
                case 0: print("User Image")
                
                    self.sideMenuController()?.menuDelegate?.menuViewProfile()
                case 1:
                    self.sideMenuController()?.menuDelegate?.menuViewHome()
                case 2:
                    print("Search")
                    self.sideMenuController()?.menuDelegate?.menuViewLobeLive()
                
                case 3:
                    self.sideMenuController()?.menuDelegate?.menuViewGroup()
                
                case 4:
                    self.sideMenuController()?.menuDelegate?.menuViewLocalLibrary()
                case 5: print("Notifications")
                    self.sideMenuController()?.menuDelegate?.menuViewPlaybackView()
                case 6: print("Settings")
//                    self.sideMenuController()?.menuDelegate?.menuViewChatView()
                
                default: break
            }
        }else{
            
            switch indexPath.row {
                case 0:
                    print("")
                case 1:
                    print("Leave Group")
//                    self.sideMenuController()?.toggleSidePanel()
//                    self.dismissViewControllerAnimated(true, completion: nil)
                    self.sideMenuController()?.menuDelegate?.menuLeaveGroup()
                
                case 2:
                    print("Recalculate Sync")
                    self.sideMenuController()?.menuDelegate?.menuRecalculateSync()
    
                case 3:
                    break
                
                default: break
            }
        }
        tableView.reloadData()
    }
}
