//
//  PlaylistVC.swift
//  Lobe
//
//  Created by Professional on 2015-11-17.
//  Copyright © 2015 Ntambwa. All rights reserved.
//

import UIKit
import Foundation
import MediaPlayer
import SwiftyJSON
import Alamofire
//import RealmSwift
//import BLKFlexibleHeightBar
//import JLToast
import Bond
//import PureLayout
import ZFDragableModalTransition
import MGSwipeTableCell

class PlaylistVC: UIViewController {
    
    @IBOutlet var musicImgLeadingConstraint: NSLayoutConstraint!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var popoverView: UIViewController!
    
    var collection: MPMediaItemCollection?
    
    var sectionTitle: String = ""
    var cellEditable = false
    var isPlayingExist: Bool = false
//    var realm: Realm!
    //    var bar:BLKFlexibleHeightBar?
//    var delegateSplitter: BLKDelegateSplitter?
    var delegateLobeList: LobeListDelegate?
    
    var userHasSelectPlaylist: Bool = false
    var persistendIDOfSelectedUserPlaylist: String = ""
    
    var nowPlayingSongRow = 0
    var indexOfCurrentPlayingMusic: NSIndexPath = NSIndexPath(forItem: 0, inSection: 0)
    
    let resumeButton = UIButton()
    override func viewDidLoad() {
        super.viewDidLoad()
        
//            self.tableView.separatorStyle = .None
        self.tableView.setEditing(false, animated: false)
//        self.tableView.allowsSelectionDuringEditing = true
//        realm = try! Realm()
        LobeStateHelper.sharedInstance.addLobeStateObserver(self)
        LobeStateHelper.sharedInstance.addUserStateObserver(self)
        createResumeButton()
         self.observeCurrentSongChange()
    }
    
    override func shouldAutorotate() -> Bool {
        return false
    }
    
    override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
        return UIInterfaceOrientation.Portrait
    }
    
    func createResumeButton(){
        
        resumeButton.backgroundColor = UIColor.appThemeColorBackgroundThirdOrange()
        resumeButton.setTitle("Resume", forState: UIControlState.Normal)
        resumeButton.titleLabel!.frame = CGRectMake(0, 0, 200, 40)
        resumeButton.addTarget(self, action: "resumeButtonTapped:", forControlEvents: UIControlEvents.TouchUpInside)
        resumeButton.tag = 22;
        resumeButton.titleLabel!.font = UIFont.systemFontOfSize(14)
        resumeButton.reversesTitleShadowWhenHighlighted = true
        resumeButton.hidden = true
        resumeButton.layer.cornerRadius = 20
        self.view.addSubview(resumeButton)
        
//        resumeButton.autoPinEdgesToSuperviewEdgesWithInsets(UIEdgeInsets(top: 0, left: 90.0, bottom: 20.0, right: 90.0), excludingEdge: .Top)
//        resumeButton.autoSetDimension(.Height, toSize: 40)
    }
    
    func resumeButtonTapped(sender: UIButton!){

        guard AUDIO_OUT_MANAGER.lobeListSongItems.count > 0 else {
//            print("resumeButtonTapped: No songs in lobelist..resume button stays hidden")
            return
        }
        if LobeStateHelper.sharedInstance.state == .Local {
            AUDIO_OUT_MANAGER.musicPlayerState = .LobeList
            AUDIO_OUT_MANAGER.currentSong = AUDIO_OUT_MANAGER.lobeListSongItems[AUDIO_OUT_MANAGER.currentLobeListSongIndex] as! SongItem
            AUDIO_OUT_MANAGER.playMusic()
            scrollToCurrentPlayingItem()
        }

    }
    
    func scrollToCurrentPlayingItem() {
        
        guard AUDIO_OUT_MANAGER.lobeListSongItems.count > 0 else {
//            print("scrollToCurrentPlayingItem(): No songs in lobelist..cannot scroll")
            return
        }
        let indexOfCurrentPlayingMusic = AUDIO_OUT_MANAGER.getIndexOfCurrentPlayingLobeListTrack()
        guard indexOfCurrentPlayingMusic < AUDIO_OUT_MANAGER.lobeListSongItems.count else {
//            print("index: \(indexOfCurrentPlayingMusic) bigger than lobeListSongItems count \(AUDIO_OUT_MANAGER.lobeListSongItems.count)")
            return
        }
//        guard tableView.numberOfSections == 1 else {
//            print("number of section in playlist is more than 1..Should not be the case")
//            return
//        }
//        guard indexOfCurrentPlayingMusic < tableView.numberOfRowsInSection(0) else {
//            print("index: \(indexOfCurrentPlayingMusic) bigger than the number of rows in section \(0)")
//            return
//        }
        let indexPath = NSIndexPath(forRow: 0, inSection: indexOfCurrentPlayingMusic)
        tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Top, animated: true)
    }
    
    func selectItem(atRow row:Int, andSection section:Int){
        
        guard AUDIO_OUT_MANAGER.lobeListSongItems.count > 0 else {
//            print("selectItem(..): No songs in lobelist..")
            return
        }
        //TODO: - make sure the row & section exists before scrolling (in case we end up using this)
        let indexPath = NSIndexPath(forRow: row, inSection: section);
        self.tableView.selectRowAtIndexPath(indexPath, animated: false, scrollPosition: UITableViewScrollPosition.None)
        self.tableView(self.tableView, didSelectRowAtIndexPath: indexPath)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        updateResumeButton()

    }
    func lobeStateChanged(){
        
        switch LobeStateHelper.sharedInstance.state {
            case .Local:
                updateResumeButton()
            case .DJ,.Participant:
                resumeButton.hidden = true
        }
        
    }
    
    func updateResumeButton(){
        
        guard LobeStateHelper.sharedInstance.state == .Local else {
            self.resumeButton.hidden = true
            return
        }
        
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            
            if (AUDIO_OUT_MANAGER.musicPlayerState == .Library || !AUDIO_OUT_MANAGER.isPlaying) && AUDIO_OUT_MANAGER.lobeListSongItems.count > 0{
                UIView.animateWithDuration(0.2, delay: 0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
                    self.resumeButton.alpha = 1
                    }, completion: { finished in
                        self.resumeButton.hidden = false
                })
                
            }else{
                UIView.animateWithDuration(0.4, delay: 0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
                    self.resumeButton.alpha = 0
                    }, completion: { finished in
                        self.resumeButton.hidden = true
                })
            }

        })

    }
    func updateState(){
       
    }
    
    func userStateChanged(){
        for songItem in ( AUDIO_OUT_MANAGER.lobeListSongItems as NSArray as! [SongItem]) {
            
            if songItem.trackInfo.mUniqueDeviceId == unique_device_id {
                
                songItem.trackInfo.mUser = LobeStateHelper.currentUser
                songItem.trackInfo.mIsYoutubeVideo = false
                songItem.trackInfo.mUserOwnerName = LobeStateHelper.currentUser.userName.value!
                songItem.trackInfo.mUserOwnerUid = LobeStateHelper.currentUser.parseID
            }
        }
        tableView.reloadData()
        
    }
    
}

//==========================================================================================
// MARK: - Observers
//==========================================================================================
extension PlaylistVC {
    
    func observeCurrentSongChange(){
        
        AUDIO_OUT_MANAGER.currentTrackObservable.observe{ currentTrack in
            dispatch_async(dispatch_get_main_queue()) {
                self.tableView.reloadData()
            }
        }
        
        //Observe LobeList
        AUDIO_OUT_MANAGER.lobeListObservableList.observe{ newList in
            dispatch_async(dispatch_get_main_queue()) {
                self.tableView.reloadData()
            }
        }
        
        //Observe audio playing state
        AUDIO_OUT_MANAGER.isAudioPlaying.observe{ currentTrack in
            dispatch_async(dispatch_get_main_queue()) {
                self.updateResumeButton()
            }
        }
        
        //Observe voting map list 
        AUDIO_OUT_MANAGER.socialVotingMapListChangeObserver.observe{ currentTrack in
            dispatch_async(dispatch_get_main_queue()) {
                self.tableView.reloadData()
            }
        }
        
    }
}


//==========================================================================================
// MARK: - UITableViewDataSource
//==========================================================================================
extension PlaylistVC: UITableViewDataSource {
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCellWithIdentifier("PlaylistCell", forIndexPath: indexPath) as! PlaylistCell
        let cell = tableView.dequeueReusableCellWithIdentifier("PlaylistCell") as! PlaylistCell
        let songItem = AUDIO_OUT_MANAGER.lobeListSongItems[indexPath.section] as! SongItem
        
        cell.songItem = songItem
        //Hide reoder button
//        self.tableView.setEditing(true, animated: false)
        //Used for change the view
        var isCurrentPlayingSong = false
        switch LobeStateHelper.sharedInstance.state {
        case .Local:
            isCurrentPlayingSong = AUDIO_OUT_MANAGER.currentLobeListSongIndex == indexPath.section ? true : false
        case .DJ,.Participant:
            isCurrentPlayingSong = AUDIO_OUT_MANAGER.currentSong.trackInfo.mID == songItem.trackInfo.mID ? true : false
        }
        
        if isCurrentPlayingSong {
            cell.cellImgLeadingConstraint.constant = 100
            cell.titleLabel.textColor = UIColor.appThemeColorBackgroundThirdOrange()
        }else{
            cell.cellImgLeadingConstraint.constant = 0
            cell.titleLabel.textColor = UIColor.whiteColor()
        }
        
        
        cell.onRemoveTrackToLobeList = { [unowned self] (PlaylistCell) -> Void in
            self.handleRemoveTrack(songItem,index: indexPath.section)
        }
        
        cell.onOptionBtnTapped = { [unowned self] (PlaylistCell) -> Void in
            self.showTrackOptionPopover(songIndex: indexPath.section)
        }
        
        return cell
    }
    
    func handleRemoveTrack(songItem:SongItem,index:Int){
        
        let lockQueue = dispatch_queue_create("lobeList.LockQueue", nil)
        dispatch_sync(lockQueue) {
            switch LobeStateHelper.sharedInstance.state {
                
            case .Local:
                if (AUDIO_OUT_MANAGER.lobeListContains(songItem)){
                    
                    AUDIO_OUT_MANAGER.lobeListSongItems.removeObjectAtIndex(index)
                    AUDIO_OUT_MANAGER.lobeListObservableList.replaceRange(0...0, with: [SongItem()])
                }
            case .DJ:
                NativeToWebHelper.sharedInstance.deleteTrackFromLobeList(songItem.trackInfo.mID)
            case .Participant:
                NativeToWebHelper.sharedInstance.dislikeTrackInLobeList(-1, trackUid: songItem.trackInfo.mID)
                
            }
        }
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  1
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return  AUDIO_OUT_MANAGER.lobeListSongItems.count ?? 0
    }
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 8
    }
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = UIColor.clearColor()
        return view
    }
}

//==========================================================================================
// MARK: - UITableViewDelegate
//==========================================================================================
extension PlaylistVC: UITableViewDelegate {
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        guard AUDIO_OUT_MANAGER.lobeListSongItems.count > 0 else {
//            print("didSelectRowAtIndexPath: lobeListSongItems is empty..make sure you don't select an item while lobeListSongItems is refreshing it's data")
            return  
        }
        
        guard let songItem =  AUDIO_OUT_MANAGER.lobeListSongItems[indexPath.section] as? SongItem else {
//            print("didSelectRowAtIndexPath: couldn't find songItem in lobeListSongItems at indexPath.section:\(indexPath.section)")
            return
        }
        
        NativeToWebHelper.sharedInstance.fromNativePing(pingType: "moveTrackToPlayNow", params: ["trackIndex":indexPath.section,"song_uid":songItem.trackInfo.mID])
        
//        
//        if AUDIO_OUT_MANAGER.musicPlayerState != .LobeList{
//            AUDIO_OUT_MANAGER.musicPlayerState = .LobeList
//        }
//
//        
////        let cell =  self.tableView.cellForRowAtIndexPath(indexPath) as! PlaylistCell
//        switch LobeStateHelper.sharedInstance.state {
//            
//            case .Local:
//                AUDIO_OUT_MANAGER.currentSong = songItem
//            case .DJ:
//                AUDIO_OUT_MANAGER.currentSong = songItem
//            case .Participant:
//                let message = "A Participant can't directly change a song. Swipe right to promote the song on top of the list."
//                print("\(message)")
//                WebToNativeHelper.sharedInstance.showToastMessage(message)
//        }
//
//        AUDIO_OUT_MANAGER.directPlayMusic()
        tableView.reloadData()
        
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    //MARK: - Drag & drop native method
    func tableView(tableView: UITableView, editingStyleForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCellEditingStyle {
        return UITableViewCellEditingStyle.None
    }
    func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    func tableView(tableView: UITableView, moveRowAtIndexPath sourceIndexPath: NSIndexPath, toIndexPath destinationIndexPath: NSIndexPath) {
        print("source: \(sourceIndexPath.section)")
        print("destination: \(destinationIndexPath.section)")
        AUDIO_OUT_MANAGER.lobeListSongItems.exchangeObjectAtIndex(sourceIndexPath.section, withObjectAtIndex: destinationIndexPath.section)
        NativeToWebHelper.sharedInstance.swapLobeListItems(fromIndex: sourceIndexPath.section, toIndex: destinationIndexPath.section)
        NativeToWebHelper.sharedInstance.fromNativePing(pingType: "moveTrackToPlayNext", params: ["fromIndex":sourceIndexPath.section,"fromIndex":destinationIndexPath.section])
        
        self.tableView.reloadData()
    }
}


extension PlaylistVC {
    
    func showTrackOptionPopover(songIndex songIndex:Int){
        
        guard AUDIO_OUT_MANAGER.lobeListSongItems.count > 0 else {
            //            print("didSelectRowAtIndexPath: lobeListSongItems is empty..make sure you don't select an item while lobeListSongItems is refreshing it's data")
            return
        }
        
        guard let songItem =  AUDIO_OUT_MANAGER.lobeListSongItems[songIndex] as? SongItem else {
            return
        }
        
        var alert: UIAlertController!
        var firstAction: UIAlertAction!
        var secondAction: UIAlertAction!
        var fourthAction: UIAlertAction!
        var thirdAction: UIAlertAction!
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: {(alert: UIAlertAction!) in
        })
        
        alert = UIAlertController(title: "Options", message: songItem.trackInfo.mName, preferredStyle: .ActionSheet)
        
        firstAction = UIAlertAction(title: "Play Now", style: .Default) { (alert: UIAlertAction!) -> Void in
            
            NativeToWebHelper.sharedInstance.fromNativePing(pingType: "moveTrackToPlayNow", params: ["trackIndex":songIndex,"song_uid":songItem.trackInfo.mID])
        }
        secondAction = UIAlertAction(title: "Play Next", style: UIAlertActionStyle.Default) { (alert: UIAlertAction!) -> Void in
            NativeToWebHelper.sharedInstance.fromNativePing(pingType: "moveTrackToPlayNext", params: ["trackIndex":songIndex,"song_uid":songItem.trackInfo.mID])
        }
        fourthAction = UIAlertAction(title: "Add To Favorite", style: .Default) { (alert: UIAlertAction!) -> Void in
            
            NativeToWebHelper.sharedInstance.fromNativePing(pingType: "AddCurrTrackToFavorite", params: ["trackIndex":songIndex,"song_uid":songItem.trackInfo.mID])
        }
    
//        alert.addAction(firstAction)
        alert.addAction(secondAction)
        alert.addAction(fourthAction)
        switch LobeStateHelper.sharedInstance.state {
            
        case .DJ,.Local:
            thirdAction = UIAlertAction(title: "Remove", style: UIAlertActionStyle.Destructive) { (alert: UIAlertAction!) -> Void in
                self.handleRemoveTrack(songItem:songItem)
            }
            alert.addAction(thirdAction)
        case .Participant:break
        }
        
        alert.addAction(cancelAction)
        presentViewController(alert, animated: true, completion:nil)
        
    }
    
    func handleRemoveTrack(songItem songItem:SongItem){
        
        let lockQueue = dispatch_queue_create("lobeList.LockQueue", nil)
        dispatch_sync(lockQueue) {
            switch LobeStateHelper.sharedInstance.state {
                
            case .Local:
                if (AUDIO_OUT_MANAGER.lobeListContains(songItem)){
                    let trackIndex = AUDIO_OUT_MANAGER.getIndexOfLobeListSongItem(songItem)
                    AUDIO_OUT_MANAGER.lobeListSongItems.removeObjectAtIndex(trackIndex)
                    AUDIO_OUT_MANAGER.lobeListObservableList.replaceRange(0...0, with: [SongItem()])
                }
                WebToNativeHelper.sharedInstance.showToastMessage("Removed song")
            case .DJ:
                NativeToWebHelper.sharedInstance.deleteTrackFromLobeList(songItem.trackInfo.mID)
                WebToNativeHelper.sharedInstance.showToastMessage("Removed song")
            case .Participant:
                NativeToWebHelper.sharedInstance.dislikeTrackInLobeList(-1, trackUid: songItem.trackInfo.mID)
                WebToNativeHelper.sharedInstance.showToastMessage("Song disliked")
                
            }
        }
    }
}

