cordova.define('cordova/plugin_list', function(require, exports, module) {
               module.exports = [
                                 {
                                     "file": "plugins/org.apache.cordova.console/www/console-via-logger.js",
                                     "id": "org.apache.cordova.console.console",
                                     "clobbers": [
                                                  "console"
                                                  ]
                                 },
                                 {
                                     "file": "plugins/org.apache.cordova.console/www/logger.js",
                                     "id": "org.apache.cordova.console.logger",
                                     "clobbers": [
                                                  "cordova.logger"
                                                  ]
                                 },
                                 {
                                     "file": "plugins/cordova-plugin-network-information/www/network.js",
                                     "id": "cordova-plugin-network-information.network",
                                     "pluginId": "cordova-plugin-network-information",
                                     "clobbers": [
                                                  "navigator.connection",
                                                  "navigator.network.connection"
                                                  ]
                                 },
                                 {
                                     "file": "plugins/cordova-plugin-network-information/www/Connection.js",
                                     "id": "cordova-plugin-network-information.Connection",
                                     "pluginId": "cordova-plugin-network-information",
                                     "clobbers": [
                                                  "Connection"
                                                  ]
                                 },
                                 {
                                 "file": "plugins/cordova-plugin-inappbrowser/www/inappbrowser.js",
                                 "id": "cordova-plugin-inappbrowser.inappbrowser",
                                 "pluginId": "cordova-plugin-inappbrowser",
                                 "clobbers": [
                                              "cordova.InAppBrowser.open",
                                              "window.open"
                                              ]
                                 },
                                 {
                                 "file": "plugins/cordova-plugin-wkwebview-engine/src/www/ios/ios-wkwebview-exec.js",
                                 "id": "cordova-plugin-wkwebview-engine.ios-wkwebview-exec",
                                 "pluginId": "cordova-plugin-wkwebview-engine",
                                 "clobbers": [
                                              "cordova.exec"
                                              ]
                                 }
                        ];
               module.exports.metadata = 
               // TOP OF METADATA
               {

                "cordova-plugin-inappbrowser": "1.1.0",
               "org.apache.cordova.console": "0.2.13",
               "cordova-plugin-network-information": "1.1.0",
               "cordova-plugin-wkwebview-engine": "1.0.2"
               }
               // BOTTOM OF METADATA
               });