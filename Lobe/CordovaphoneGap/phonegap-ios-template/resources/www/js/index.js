/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
        // Bind Event Listeners
        //
        // Bind any events that are required on startup. Common events are:
        // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
        // deviceready Event Handler
        //
        // The scope of 'this' is the event. In order to call the 'receivedEvent'
        // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        //app.receivedEvent('deviceready');
        document.getElementById('bookmarkBtn').addEventListener("click", app.testGeolocation);
    },
        // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');
        
        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');
        
        console.log('Received Event: ' + id);
    },
    addBookmark: function() {
        console.log("Called add bookmark");
        var win = function(d) {
            console.log("Bookmark added!");
        };
        var fail = function(e) {
            console.log(e)
        }
        var bookmark = document.getElementById("bookmark").value
        cordova.exec(win, fail, "HybridBridge", "testWebToNativeCall", [true]);
    },
    testConnection: function () {
        var networkState = navigator.connection.type;
        
        var states = {};
        states[Connection.UNKNOWN] = 'Unknown connection';
        states[Connection.ETHERNET] = 'Ethernet connection';
        states[Connection.WIFI] = 'WiFi connection';
        states[Connection.CELL_2G] = 'Cell 2G connection';
        states[Connection.CELL_3G] = 'Cell 3G connection';
        states[Connection.CELL_4G] = 'Cell 4G connection';
        states[Connection.CELL] = 'Cell generic connection';
        states[Connection.NONE] = 'No network connection';
        
        cordova.exec(null, null, "HybridBridge", "testWebToNativeCall", ["Network Info - connection type: " + states[networkState]]);
    },
    testGeolocation: function () {
        /* Need to set the following keys in the Info.plist to pop up the access to geolocation dialog
         <key>NSLocationAlwaysUsageDescription</key>
         <string>This app requires constant access to your location, even when the screen is off.</string>
         <key>NSLocationWhenInUseUsage</key>
         <string>This app would like to access your location.</string>
         */
        alert("Start Geolocation, get current position");
        
        navigator.geolocation.getCurrentPosition(
         function (position) {
//             cordova.exec(null, null, "HybridBridge", "testWebToNativeCall", ["Geo localisation: latitude" + position.coords.latitude + " longitude:" + position.coords.longitude]);
         },
         function (error) {
             console.log('\tGeolocation error code: ' + error.code + '\n' + 'message: ' + error.message + '\n')
         })
    }
    
};

app.initialize();

var receivedNativeMessage = function(){
    console.log("function callType1 passed")
}

function callType2 (){
    console.log("function callType2 passed")
}